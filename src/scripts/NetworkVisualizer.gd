extends Control

onready var neuron = preload("res://assets/Neuron.tscn")

onready var mouse_capture = get_node("Interface/MouseCapture")
onready var output_display = get_node("Canvas/Output")
onready var input_display = get_node("Interface/InputSet")
onready var train_button = get_node("Interface/Train")
onready var networkWindow = get_node("Network_Display/Container")
onready var network = get_node("CPP_Network");

var network_topology : PoolIntArray 
var input_data : Array
var num_epochs : int
var l_rate : float
var class_index : int
var displayWeightChange : bool = false
var training : bool = false

func _ready():
	get_node("Setup").popup_centered(Vector2(0,0))

"""
Visualizer set up
"""
func set_up(_topology : Array, imported : bool):
	network_topology = _topology
	
	# Set up mouse capture
	mouse_capture.set_img_size(int(sqrt(1024)))
	mouse_capture.set_output_size(_topology[0])
	
	if !imported:
		print("creating dense network")
		network.createDenseNetwork(network_topology)
	
	# Create Node Visualization Layers
	for layer_size in network_topology:
		create_neuron_layer(layer_size)
	
	# Wait a frame to let layouts update positions
	yield(get_tree(),"idle_frame")

	# Connect Neurons
	displayWeights()

"""
Creates the neuron layer based on passed
layer object
"""
func create_neuron_layer(layer_size : int):
	var container : VBoxContainer = VBoxContainer.new()
	container.alignment = BoxContainer.ALIGN_CENTER
	networkWindow.add_child(container)
	
	for x in range(layer_size):
		var new_neuron = neuron.instance()
		new_neuron.set_bias(network.getNeuronBias(networkWindow.get_child_count() - 1, x))
		new_neuron.name = str(networkWindow.get_child_count()-1) + "_" + str(container.get_child_count())
		container.add_child(new_neuron)

"""
Visualizes the weights
"""
func displayWeights():
	var container_children : Array = networkWindow.get_children()
	for index in range(1, len(network_topology)):
		connect_neurons(container_children, index)

"""
Connects the respective neuron layers together
accordingly
"""
func connect_neurons(_container_childern : Array, _index : int):
	var container_layer : Array = _container_childern[_index].get_children()
	var container_layer_size : int = len(container_layer);
	var prev_container_layer : Array = _container_childern[_index - 1].get_children()
	for i in range(container_layer_size):
		var node = container_layer[i]
		if(_index != len(_container_childern) - 1):
			node.updateBias(network.getNeuronBias(_index, i))
		for j in range(len(prev_container_layer)):
			var weight = network.getDendriteWeight(_index, i, j)
			node.connectToNode(prev_container_layer[j], weight)

"""
Sets the input data value
"""
func set_input(x):
	input_data = x
	input_display.text = "Input Data Set"
	var container_children : Array = networkWindow.get_children()
	var input_layer = container_children[0].get_children()
	for i in range(len(input_layer)):
		input_layer[i].updateBias(input_data[i])

"""
Clears the input data
"""
func clear_input():
	input_data = []
	input_display.text = "Input Data Not Set"

"""
Begins running the network with the passed
input array and desired output array for the
inputted number of epochs.
"""
func runNetwork(input, output, n_epoch):
	for n in range(n_epoch):
		#var prediction = neural_network.train_network(input, output)
		if (typeof(output) == TYPE_INT):
			output = y_hot(output)
		var prediction = network.trainNetwork(input, output)
		output_display.text = "Current Prediction %: \n" + str(prediction) + "\nRun : " + str(n+1)
		
		var last_layer : Array = networkWindow.get_children()[-1].get_children()
		for index in range(len(last_layer)):
			last_layer[index].updateBias(prediction[index])
		
		if !training:
			break
		yield(get_tree().create_timer(0.01), "timeout")
		
		if (argMax(prediction) == class_index):
			output_display.modulate = Color.green
		else:
			output_display.modulate = Color.red

		if (displayWeightChange):
			displayWeights()
	
	displayWeights()
	clear_input()
	training = false
	train_button.text = "Train"

"""
Creates encoded array for passed class index
"""
func y_hot(class_index : int) -> PoolRealArray:
	var output : PoolRealArray = []
	for i in range(network_topology[-1]):
		output.append(0)
	output[class_index] = 1
	return output

"""
Gets the index of the max value of the passed array
"""
func argMax(arr : Array):
	var m = arr.max()
	for i in range(arr.size()):
		if (arr[i] == m):
			return i
	return -1

"""
Updates the num_epochs value on text change event
"""
func _on_Epochs_text_changed(new_text):
	var epochs = int(new_text)
	if (typeof(epochs) == TYPE_INT):
		num_epochs = epochs

"""
Updates the learning rate value on text change event
"""
func _on_LearnRate_text_changed(new_text):
	l_rate = float(new_text)
	if (typeof(l_rate) == TYPE_REAL):
		network.setLearningRate(l_rate)

"""
Updates the class index value on text change event
"""
func _on_Classify_text_changed(new_text):
	var index = int(new_text)
	if (typeof(index) == TYPE_INT):
		class_index = index

"""
Begins training on press
"""
func _on_Train_pressed():
	if training:
		training = false
	elif input_data.size() > 0 and class_index >= 0 and l_rate > 0 and num_epochs > 0:
		training = true
		train_button.text = "Stop Training"
		runNetwork(input_data, class_index, num_epochs)
	else:
		$Popup.show()
		yield(get_tree().create_timer(1), "timeout")
		$Popup.hide()

"""
Toggles value for weight change display
"""
func _on_WeightOption_pressed():
	displayWeightChange = !displayWeightChange

"""
Opens the save window on click
"""
func _on_Export_pressed():
	get_node("SaveDialog").popup_centered(Vector2.ZERO)

"""
Saves the current neural network weights to the passed
file path
"""
func save_weights(filepath : String):
	var data : Dictionary = network.getExportDictionary()
	
	# Open a file
	var file = File.new()
	if file.open(filepath, File.WRITE) != 0:
		print("Error opening file")
		return
	
	# Save the dictionary as JSON
	file.store_line(JSON.print(data))
	file.close()

"""
Loads passed weights stored within the passed
file path.
"""
func load_weights(file_path : String):
	var file : File = File.new()
	if not file.file_exists(file_path):
		print("Invalid file path!")
		return

	# Open existing file
	if file.open(file_path, File.READ) != 0:
		print("Error opening file")
		return
	
	# Get the data
	var data : Dictionary = {}
	data = JSON.parse(file.get_line()).get_result()
	var settings : Array = network.importWeightDictionary(data)
	var topology : PoolIntArray = settings[0]

	l_rate = settings[1]
	get_node("Interface/LearnRate").text = str(l_rate)

	if topology != null and len(topology) > 2:
		set_up(topology, true)
		get_node("LoadDialog").visible = false
		get_node("Setup").visible = false

"""
Loads the weights from a selected file path
"""
func _on_LoadDialog_file_selected(filepath : String):
	load_weights(filepath)

"""
Saves the current network weights to the passed file path
"""
func _on_SaveDialog_file_selected(filepath : String):
	save_weights(filepath)

"""
Creates a new neural network on click
"""
func _on_New_pressed():
	network.clearNetwork()
	for child in networkWindow.get_children():
		networkWindow.remove_child(child)
	get_node("Setup").popup_centered(Vector2.ZERO)
