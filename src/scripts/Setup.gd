extends Control

onready var _dial = preload("res://assets/Dial.tscn")
onready var hidden_layers_container = get_node("Hidden_Layers/ScrollContainer/HBoxContainer")
onready var minus = get_node("Hidden_Layers/Minus")

func _ready():
	add_dial()

"""
Removes dial from hidden layer.
Disables minus when appropriate.
"""
func remove_dial():
	var to_remove = hidden_layers_container.get_children().pop_front()
	hidden_layers_container.remove_child(to_remove)
	to_remove.queue_free()
	
	if (len(hidden_layers_container.get_children()) == 0):
		minus.disabled = true

"""
Adds dials into the hidden layer container.
"""
func add_dial():
	var dial = _dial.instance()
	hidden_layers_container.add_child(dial)
	
	if (minus.disabled):
		minus.disabled = false

"""
Creates a neural network based on current
dial values.
"""
func _on_Create_pressed():
	var topology : Array = get_topology()
	
	# Not Enough Layers
	if (len(topology) < 2):
		return
	
	for layer_num in topology:
		if layer_num <= 0:
			return
	
	
	self.visible = false
	get_parent().set_up(topology, false)

"""
Gets the topology based on the current
dials.
"""
func get_topology() -> Array:
	var _topology : Array = []
	_topology.append(get_node("Input_Dial").value)
	for child in hidden_layers_container.get_children():
		_topology.append(child.value)
	_topology.append(get_node("Output_Dial").value) 
	return _topology

"""
Opens load window for importing a neural
network file.
"""
func _on_Import_pressed():
	var load_dialog = get_parent().get_node("LoadDialog")
	load_dialog.popup_centered(Vector2.ZERO)

"""
Decrease number of hidden layers.
"""
func _on_Minus_pressed():
	remove_dial()

"""
Increase number of hidden layers.
"""
func _on_Plus_pressed():
	add_dial()