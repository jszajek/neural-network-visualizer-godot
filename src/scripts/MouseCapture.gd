extends Control

var canDraw : bool = false
var draw : bool = false
var output : Array
var img_size
var lines : Array
var output_size

"""
Catches input events
"""
func _input(event):
	if event.is_action_pressed("draw") and !draw and canDraw:
		$TextureRect.texture = null
		draw = true
		make_line()
		add_vec()
	if (!canDraw and draw) or (event.is_action_released("draw") and draw):
		close_line()
		draw = false

func set_output_size(_size : int):
	output_size = _size

func make_line():
	$ViewportContainer/Drawport/Line2D.show()

func set_img_size(x):
	img_size = x

"""
Ends drawing line.
"""
func close_line():
	var arr = $ViewportContainer/Drawport/Line2D.get_points()
	var analysis = PointAnaylsis.new()
	var lines = analysis.run(arr)
	for i in range(lines.size()):
		var points = lines[i]
		var temp : Line2D = Line2D.new()
		$ViewportContainer/Drawport.add_child(temp)
		temp.modulate = Color(rand_range(0.2,1),rand_range(0.2,1),rand_range(0.2,1))
		temp.add_point(points[0])
		temp.add_point(points[1])
		self.lines.append(temp)
	
	lines.sort_custom(self, "lineComparison")
	lines.resize(output_size)
	
	output = []
	for i in range(output_size):
		if lines[i] == null:
			output.append(0)
			output.append(0)
		else:
			# Append both the magnitude and direction of line
			output.append(calcMagnitude(lines[i][0], lines[i][1]))
			output.append(calcDirection(lines[i][0], lines[i][1]))
	
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	var img : Image = $ViewportContainer/Drawport.get_texture().get_data()
	
	img.flip_y()
	
	var wVals = getImgWidth(arr)
	var hVals = getImgHeight(arr)
	var rect = Rect2(wVals[0], hVals[0], wVals[1], hVals[1])
	img = img.get_rect(rect)
	
	img.resize(img_size,img_size)
	
	for i in range(self.lines.size()):
		self.lines[i].queue_free()
	self.lines.clear()
	
	$ViewportContainer/Drawport/Line2D.clear_points()
	$ViewportContainer/Drawport/Line2D.hide()
	
	img.resize($ViewportContainer/Drawport.size.x,$ViewportContainer/Drawport.size.y,1)
	var tex = ImageTexture.new()
	tex.create_from_image(img)
	$TextureRect.texture = tex

"""
Compares two line's magnitudes
"""
func lineComparison(a, b):
	var line1 = calcMagnitude(a[0], a[1])
	var line2 = calcMagnitude(b[0], b[1])
	return line1 > line2

"""
Calculates the line's magnitude
"""
func calcMagnitude(point1 : Vector2, point2 : Vector2):
	return sqrt(pow(point1.x-point2.x, 2) + pow(point1.y - point2.y,2))

"""
Calculats the line's direction
"""
func calcDirection(point1 : Vector2, point2 : Vector2):
	return atan2(point2.y-point1.y, point2.x-point1.x)

"""
Add vector to line on delay
"""
func add_vec():
	if draw:
		$ViewportContainer/Drawport/Line2D.add_point(get_local_mouse_position())
		yield(get_tree().create_timer(0.005), "timeout")
		add_vec()

"""
Determines drawn image width based on 
farthest and closest vector point.
"""
func getImgWidth(arr : PoolVector2Array):
	var Max = -1
	var Min = 1000000
	for i in range(arr.size()):
		if (arr[i].x < Min):
			Min = arr[i].x
		if (arr[i].x > Max):
			Max = arr[i].x
	return [Min, Max - Min, Max]

"""
Determines drawn image height based on
farthest and closest vector point.
"""
func getImgHeight(arr : PoolVector2Array):
	var Max = -1
	var Min = 1000000
	for i in range(arr.size()):
		if (arr[i].y < Min):
			Min = arr[i].y
		if (arr[i].y > Max):
			Max = arr[i].y
	return [Min, Max - Min, Max]

func _on_MouseCapture_mouse_entered():
	canDraw = true

func _on_MouseCapture_mouse_exited():
	canDraw = false

"""
Submits the data to the visualizer as 
input data.
"""
func _on_Submit_pressed():
	if output.size() > 0 and get_parent().get_parent().has_method("set_input"):
		output.resize(output_size)
		get_parent().get_parent().set_input(output)

"""
Clears the current drawn image and output
data.
"""
func _on_Clear_pressed():
	$TextureRect.texture = null
	if get_parent().has_method("clear_input"):
		get_parent().clear_input()

"""
Point Analysis class that determines lines based
on vector points.
"""
class PointAnaylsis:
	var tolerance = 0.5 # 50% tolerance
	
	"""
	Runs comparison based on comparison tolerance, creating 
	new line based on relative difference of slope.
	"""
	func run(points : PoolVector2Array):
		var finLines = []
		var startPoint = points[0]
		var curPoint = points[0]
		var curSlope = 0.0
		
		for i in range(points.size()-1):
			if i == 0:
				startPoint = points[i]
				curPoint = points[i]
				curSlope = pointSlope(curPoint, points[i+1])
				continue
			
			curPoint = points[i]
			var testSlope = pointSlope(curPoint, points[i+1])
			
			if relDifference(curSlope, testSlope) > tolerance:
				# Difference in slope make new line
				finLines.append([startPoint, points[i+1]])
				startPoint = points[i]
				curSlope = testSlope
		
		return finLines
	
	"""
	Calculates point slope.
	"""
	func pointSlope(start, end):
		var denom = (end.x-start.x)
		return float(end.y-start.y) / denom if denom != 0 else 0
	
	"""
	Relative difference between slopes.
	"""
	func relDifference(slopeA, slopeB):
		var dif = abs(slopeA - slopeB)
		var denom = max(abs(slopeA), abs(slopeB))
		return 0 if denom == 0.0 else dif/denom
