extends Panel

var lines : Dictionary

func _ready():
	lines = {}

"""
Sets the label bias
"""
func set_bias(_bias : float):
	$Node2D/Bias.text = str("%0.2f" % _bias)

"""
Creates the line for visual neuron connection
"""
func create_line(target : Control) -> Line2D:
	var _line = Line2D.new()
	_line.z_index = -2
	add_child(_line)
	lines[target] = _line
	return _line

"""
Updates the display bias
"""
func updateBias(new_bias : float):
	set_bias(new_bias)

"""
Updates the neuron's connection display based on
new weight.
"""
func updateNode(target : Control, new_weight : float):
	lines[target].width = sigmoid(new_weight) * 2
	lines[target].default_color = Color.red if new_weight < 0 else Color.blue

"""
Connects this neuron to passed target
"""
func connectToNode(target : Control, weight : float):
	if lines.has(target):
		updateNode(target, weight)
		return
	
	var line = create_line(target)
	var image : Sprite = get_child(1)
	line.add_point(rect_size/2)
	line.add_point(image.to_local(target.get_global_transform().get_origin() + target.rect_size))
	updateNode(target, weight)

func sigmoid(x):
	return 1.0/(1.0+exp(-x))