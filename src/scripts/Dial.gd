extends Control

var value : int = 0

func _ready():
	update_text()

func update_text():
	$Value.text = str(value)

func _on_Down_pressed():
	value -= 1
	update_text()

func _on_Up_pressed():
	value += 1
	update_text()
