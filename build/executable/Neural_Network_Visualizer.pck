GDPC                                                                                D   res://.import/NeuralNet.png-76e773d6d61e25f6c03eaa2664efe950.stex    I      !�      Q�z��ӯ��C89D   res://.import/NeuralNet.svg-564a8818049a9c4ceea904a6bc94ae13.stex   ��      P1      i6�:�3 �:[�B<   res://.import/node.png-7b6b2e65f5cc68860b0123a50d66c29e.stex B      �      [�U�^�Α�蹻#n   res://NetworkVisualizer.tscn�      +      �S�R,#�=��ҋ�   res://assets/Dial.tscn   3      �      F!X��E	%�̡n�    res://assets/MouseCapture.tscn  �7      Z      ���Z���a���\���@   res://assets/Network.tscn   @>      �       c�m[�KN�-��=��4   res://assets/Neuron.tscn�>      *      .��6T��+�w8'��   res://assets/node.png.import�C      �      ��xd�,�l䣭   res://bin/gdnetwork.gdnlib  @F            ̢pU�څ���bM&�   res://bin/gdnetwork.gdns`G            ��|��� ��X.%�h�   res://default_env.tres  pH      �       um�`�N��<*ỳ�8   res://icons/NeuralNet.png   �     x�      ��Cx-X0���V(_�    res://icons/NeuralNet.png.importP�      �      ����gnċNÀ��    res://icons/NeuralNet.svg.import@&     �      M'��Zth|y�XH[�:   res://project.binary�     �      ��N�g?n����   res://scripts/Dial.gd.remap �     '       �GM��G���S���   res://scripts/Dial.gdc  �(     �      ����T����<3��$   res://scripts/MouseCapture.gd.remap  �     /       ��{F�
�FӼP#�    res://scripts/MouseCapture.gdc  �*     �      ��BjX&�<yp.�(   res://scripts/NetworkVisualizer.gd.remap0�     4       �
�Y�H���-�C�F+$   res://scripts/NetworkVisualizer.gdc �F     !$      ��շp\�Z*�Y�UP�a    res://scripts/Neuron.gd.remap   p�     )       �3�U�Ү{\��D��M   res://scripts/Neuron.gdc�j     .      �']�Ω��c�͡�(   res://scripts/ScrollContainer.gd.remap  ��     2       R�K���&�c6)#�]$   res://scripts/ScrollContainer.gdc   �q     #      O� �&$���ă��$   res://scripts/Setup.gd.remap��     (       U�G�5W�R�A�h   res://scripts/Setup.gdc  v     �	      #��`��&�W��x3�        [gd_scene load_steps=7 format=2]

[ext_resource path="res://scripts/NetworkVisualizer.gd" type="Script" id=1]
[ext_resource path="res://assets/Network.tscn" type="PackedScene" id=2]
[ext_resource path="res://scripts/ScrollContainer.gd" type="Script" id=3]
[ext_resource path="res://assets/MouseCapture.tscn" type="PackedScene" id=4]
[ext_resource path="res://scripts/Setup.gd" type="Script" id=5]
[ext_resource path="res://assets/Dial.tscn" type="PackedScene" id=6]

[node name="Visualizer" type="Control"]
anchor_right = 1.0
anchor_bottom = 1.0
mouse_filter = 1
script = ExtResource( 1 )

[node name="CPP_Network" parent="." instance=ExtResource( 2 )]

[node name="Canvas" type="CanvasLayer" parent="."]
editor/display_folded = true
layer = -4

[node name="Background" type="ColorRect" parent="Canvas"]
editor/display_folded = true
show_behind_parent = true
anchor_right = 1.0
anchor_bottom = 1.0
mouse_filter = 1
color = Color( 0.4, 0.4, 0.4, 1 )

[node name="Output" type="Label" parent="Canvas"]
modulate = Color( 0, 0, 0, 1 )
anchor_left = 0.5
anchor_right = 0.5
margin_left = -162.0
margin_right = 138.0
margin_bottom = 100.0
rect_min_size = Vector2( 300, 100 )
text = "Current Output:"
align = 1
valign = 1
autowrap = true

[node name="Network_Display" type="ScrollContainer" parent="."]
editor/display_folded = true
anchor_right = 1.0
anchor_bottom = 1.0
margin_right = -350.0
rect_min_size = Vector2( 650, 600 )
script = ExtResource( 3 )

[node name="Container" type="HBoxContainer" parent="Network_Display"]
margin_right = 650.0
margin_bottom = 600.0
rect_min_size = Vector2( 650, 600 )
custom_constants/separation = 30
alignment = 1

[node name="Interface" type="Control" parent="."]
editor/display_folded = true
anchor_left = 1.0
anchor_right = 1.0
anchor_bottom = 1.0
margin_left = -350.0
mouse_filter = 1

[node name="Background" type="ColorRect" parent="Interface"]
anchor_left = 1.0
anchor_right = 1.0
anchor_bottom = 1.0
margin_left = -350.0
rect_min_size = Vector2( 350, 0 )
mouse_filter = 1
color = Color( 0.905882, 0.905882, 0.905882, 1 )

[node name="MouseCapture" parent="Interface" instance=ExtResource( 4 )]
margin_left = 6.0
margin_top = 52.0
margin_right = 346.0
margin_bottom = 422.0

[node name="InputSet" type="Label" parent="Interface"]
modulate = Color( 0, 0, 0, 1 )
margin_left = 83.5653
margin_top = 422.734
margin_right = 253.565
margin_bottom = 452.734
text = "Input Data Not Set"
align = 1
valign = 1

[node name="Epochs" type="LineEdit" parent="Interface"]
editor/display_folded = true
margin_left = 123.565
margin_top = 452.734
margin_right = 233.565
margin_bottom = 477.734
max_length = 4
caret_blink = true
caret_blink_speed = 0.5

[node name="Label3" type="Label" parent="Interface/Epochs"]
modulate = Color( 0, 0, 0, 1 )
margin_left = -120.0
margin_right = 10.0
margin_bottom = 25.0
text = "# of Epochs :"
align = 1
valign = 1

[node name="LearnRate" type="LineEdit" parent="Interface"]
editor/display_folded = true
margin_left = 123.565
margin_top = 482.734
margin_right = 233.565
margin_bottom = 507.734
max_length = 6
caret_blink = true
caret_blink_speed = 0.5

[node name="Label3" type="Label" parent="Interface/LearnRate"]
modulate = Color( 0, 0, 0, 1 )
margin_left = -120.0
margin_right = 10.0
margin_bottom = 25.0
text = "Learning Rate :"
align = 1
valign = 1

[node name="Classify" type="LineEdit" parent="Interface"]
editor/display_folded = true
margin_left = 123.565
margin_top = 512.734
margin_right = 233.565
margin_bottom = 537.734
max_length = 2
caret_blink = true
caret_blink_speed = 0.5

[node name="Label3" type="Label" parent="Interface/Classify"]
modulate = Color( 0, 0, 0, 1 )
margin_left = -120.0
margin_right = 10.0
margin_bottom = 25.0
text = "Class Index :"
align = 1
valign = 1

[node name="Train" type="Button" parent="Interface"]
margin_left = 73.5653
margin_top = 552.734
margin_right = 283.565
margin_bottom = 582.734
text = "Train"

[node name="Export" type="Button" parent="Interface"]
margin_left = 15.0
margin_top = 11.0
margin_right = 165.0
margin_bottom = 41.0
text = "Export Weights"

[node name="New" type="Button" parent="Interface"]
margin_left = 185.0
margin_top = 11.0
margin_right = 335.0
margin_bottom = 41.0
text = "New Network"

[node name="WeightOption" type="CheckBox" parent="Interface"]
editor/display_folded = true
margin_left = 283.565
margin_top = 502.734
margin_right = 307.565
margin_bottom = 526.734

[node name="Label4" type="Label" parent="Interface/WeightOption"]
modulate = Color( 0, 0, 0, 1 )
margin_left = -40.0
margin_top = -40.0
margin_right = 60.0
margin_bottom = 10.0
text = "Display Weight Change"
align = 1
valign = 1
autowrap = true

[node name="Popup" type="Popup" parent="Interface"]
editor/display_folded = true
modulate = Color( 1, 0, 0, 1 )
self_modulate = Color( 0, 0, 0, 1 )
margin_left = 700.0
margin_top = 140.0
margin_right = 970.0
margin_bottom = 170.0
mouse_filter = 2

[node name="Label" type="Label" parent="Interface/Popup"]
modulate = Color( 1, 0, 0, 1 )
margin_top = 60.0
margin_right = 270.0
margin_bottom = 90.0
text = "Something Is Wrong!"
align = 1
valign = 1
autowrap = true

[node name="Setup" type="WindowDialog" parent="."]
editor/display_folded = true
anchor_left = 0.5
anchor_top = 0.5
anchor_right = 0.5
anchor_bottom = 0.5
margin_left = -350.0
margin_top = -200.0
margin_right = 350.0
margin_bottom = 200.0
window_title = "Set Up Neural Network"
script = ExtResource( 5 )

[node name="Title" type="Label" parent="Setup"]
modulate = Color( 0, 0, 0, 1 )
anchor_left = 0.5
anchor_right = 0.5
margin_left = -75.0
margin_top = 10.0
margin_right = 75.0
margin_bottom = 25.0
rect_scale = Vector2( 3, 3 )
rect_pivot_offset = Vector2( 75, 0 )
text = "Create Neural Network"
align = 1
valign = 1

[node name="Create" type="Button" parent="Setup"]
anchor_left = 0.5
anchor_top = 1.0
anchor_right = 0.5
anchor_bottom = 1.0
margin_left = -62.5
margin_top = -70.0
margin_right = 62.5
margin_bottom = -46.0
rect_scale = Vector2( 2, 2 )
rect_pivot_offset = Vector2( 62, 12 )
text = "Create"

[node name="Hidden_Layers" type="ColorRect" parent="Setup"]
editor/display_folded = true
anchor_left = 0.5
anchor_top = 0.5
anchor_right = 0.5
anchor_bottom = 0.5
margin_left = -200.0
margin_top = -78.0
margin_right = 200.0
margin_bottom = 78.0
color = Color( 0.486275, 0.486275, 0.486275, 1 )

[node name="Label" type="Label" parent="Setup/Hidden_Layers"]
modulate = Color( 0, 0, 0, 1 )
anchor_left = 0.5
anchor_right = 0.5
margin_left = -45.5
margin_top = -40.0
margin_right = 54.5
margin_bottom = -26.0
rect_scale = Vector2( 2, 2 )
rect_pivot_offset = Vector2( 50, 0 )
text = "Hidden Layers"
align = 1
valign = 1

[node name="ScrollContainer" type="ScrollContainer" parent="Setup/Hidden_Layers"]
editor/display_folded = true
anchor_right = 1.0
anchor_bottom = 1.0
scroll_vertical_enabled = false

[node name="HBoxContainer" type="HBoxContainer" parent="Setup/Hidden_Layers/ScrollContainer"]
margin_right = 400.0
margin_bottom = 150.0
rect_min_size = Vector2( 400, 150 )
custom_constants/separation = 10
alignment = 1

[node name="Minus" type="Button" parent="Setup/Hidden_Layers"]
margin_top = -40.0
margin_right = 20.0
margin_bottom = -20.0
rect_min_size = Vector2( 10, 10 )
rect_scale = Vector2( 1.5, 1.5 )
text = "-"

[node name="Plus" type="Button" parent="Setup/Hidden_Layers"]
anchor_left = 1.0
anchor_right = 1.0
margin_left = -20.0
margin_top = -40.0
margin_bottom = -20.0
rect_min_size = Vector2( 10, 10 )
rect_scale = Vector2( 1.5, 1.5 )
rect_pivot_offset = Vector2( 20, 0 )
text = "+"

[node name="Input_Dial" parent="Setup" instance=ExtResource( 6 )]
editor/display_folded = true
anchor_top = 0.5
anchor_right = 0.0
anchor_bottom = 0.5
margin_left = 40.0
margin_top = -75.0
margin_right = 115.0
margin_bottom = 75.0

[node name="Label" type="Label" parent="Setup/Input_Dial"]
modulate = Color( 0, 0, 0, 1 )
anchor_left = 0.5
anchor_right = 0.5
margin_left = -22.5
margin_top = -40.0
margin_right = 22.5
margin_bottom = -26.0
rect_scale = Vector2( 2, 2 )
rect_pivot_offset = Vector2( 22, 0 )
text = "Input"
align = 1
valign = 1

[node name="Output_Dial" parent="Setup" instance=ExtResource( 6 )]
editor/display_folded = true
anchor_left = 1.0
anchor_top = 0.5
anchor_bottom = 0.5
margin_left = -115.0
margin_top = -75.0
margin_right = -40.0
margin_bottom = 75.0

[node name="Label" type="Label" parent="Setup/Output_Dial"]
modulate = Color( 0, 0, 0, 1 )
anchor_left = 0.5
anchor_right = 0.5
margin_left = -22.5
margin_top = -40.0
margin_right = 22.5
margin_bottom = -26.0
rect_scale = Vector2( 2, 2 )
rect_pivot_offset = Vector2( 22, 0 )
text = "Output"
align = 1
valign = 1

[node name="Import" type="Button" parent="Setup"]
anchor_left = 1.0
anchor_right = 1.0
margin_left = -100.0
margin_top = 15.0
margin_right = -30.0
margin_bottom = 55.0
text = "Import"

[node name="LoadDialog" type="FileDialog" parent="."]
anchor_left = 0.5
anchor_top = 0.5
anchor_right = 0.5
anchor_bottom = 0.5
margin_left = -275.0
margin_top = -187.5
margin_right = 275.0
margin_bottom = 187.5
window_title = "Open a File"
mode = 0
access = 2
filters = PoolStringArray( "*.net", "*.network" )
current_dir = "C:/Users/Justin/Documents/Godot Projects/Neural_Network_Visualizer/src"
current_path = "C:/Users/Justin/Documents/Godot Projects/Neural_Network_Visualizer/src/"

[node name="SaveDialog" type="FileDialog" parent="."]
anchor_left = 0.5
anchor_top = 0.5
anchor_right = 0.5
anchor_bottom = 0.5
margin_left = -275.0
margin_top = -187.5
margin_right = 275.0
margin_bottom = 187.5
resizable = true
dialog_hide_on_ok = true
access = 2
filters = PoolStringArray( "*.net", "*.network" )
current_dir = "C:/Users/Justin/Documents/Godot Projects/Neural_Network_Visualizer/src"
current_path = "C:/Users/Justin/Documents/Godot Projects/Neural_Network_Visualizer/src/"
[connection signal="text_changed" from="Interface/Epochs" to="." method="_on_Epochs_text_changed"]
[connection signal="text_changed" from="Interface/LearnRate" to="." method="_on_LearnRate_text_changed"]
[connection signal="text_changed" from="Interface/Classify" to="." method="_on_Classify_text_changed"]
[connection signal="pressed" from="Interface/Train" to="." method="_on_Train_pressed"]
[connection signal="pressed" from="Interface/Export" to="." method="_on_Export_pressed"]
[connection signal="pressed" from="Interface/New" to="." method="_on_New_pressed"]
[connection signal="pressed" from="Interface/WeightOption" to="." method="_on_WeightOption_pressed"]
[connection signal="pressed" from="Setup/Create" to="Setup" method="_on_Create_pressed"]
[connection signal="pressed" from="Setup/Hidden_Layers/Minus" to="Setup" method="_on_Minus_pressed"]
[connection signal="pressed" from="Setup/Hidden_Layers/Plus" to="Setup" method="_on_Plus_pressed"]
[connection signal="pressed" from="Setup/Import" to="Setup" method="_on_Import_pressed"]
[connection signal="file_selected" from="LoadDialog" to="." method="_on_LoadDialog_file_selected"]
[connection signal="file_selected" from="SaveDialog" to="." method="_on_SaveDialog_file_selected"]
            [gd_scene load_steps=2 format=2]

[ext_resource path="res://scripts/Dial.gd" type="Script" id=1]


[node name="Dial" type="Control"]
anchor_right = 1.0
anchor_bottom = 1.0
margin_right = -949.0
margin_bottom = -450.0
rect_min_size = Vector2( 75, 150 )
mouse_filter = 1
script = ExtResource( 1 )

[node name="Background" type="ColorRect" parent="."]
anchor_right = 1.0
anchor_bottom = 1.0
color = Color( 0.513726, 0.513726, 0.513726, 1 )

[node name="Up" type="Button" parent="."]
anchor_right = 1.0
margin_bottom = 50.0
rect_rotation = 180.0
rect_pivot_offset = Vector2( 37.5, 25 )
text = "v"

[node name="Down" type="Button" parent="."]
anchor_top = 1.0
anchor_right = 1.0
anchor_bottom = 1.0
margin_top = -50.0
rect_pivot_offset = Vector2( 37.5, 25 )
text = "v"

[node name="Value" type="Label" parent="."]
modulate = Color( 0, 0, 0, 1 )
anchor_left = 0.5
anchor_top = 0.5
anchor_right = 0.5
anchor_bottom = 0.5
margin_left = -18.5
margin_top = -12.5
margin_right = 18.5
margin_bottom = 12.5
rect_scale = Vector2( 2, 2 )
rect_pivot_offset = Vector2( 18.75, 12 )
text = "1"
align = 1
valign = 1
[connection signal="pressed" from="Up" to="." method="_on_Up_pressed"]
[connection signal="pressed" from="Down" to="." method="_on_Down_pressed"]
      [gd_scene load_steps=2 format=2]

[ext_resource path="res://scripts/MouseCapture.gd" type="Script" id=1]

[node name="MouseCapture" type="Control"]
margin_right = 340.0
margin_bottom = 370.0
script = ExtResource( 1 )

[node name="Outline" type="ColorRect" parent="."]
margin_right = 340.0
margin_bottom = 340.0
mouse_filter = 2
color = Color( 0, 0, 0, 1 )

[node name="ViewportContainer" type="ViewportContainer" parent="."]
margin_left = 10.0
margin_top = 10.0
margin_right = 330.0
margin_bottom = 330.0
rect_min_size = Vector2( 320, 320 )
mouse_filter = 2

[node name="Drawport" type="Viewport" parent="ViewportContainer"]
size = Vector2( 320, 320 )
handle_input_locally = false
render_target_update_mode = 3

[node name="Line2D" type="Line2D" parent="ViewportContainer/Drawport"]
light_mask = 2
width = 15.0
default_color = Color( 0, 0, 0, 1 )

[node name="TextureRect" type="TextureRect" parent="."]
margin_left = 10.0
margin_top = 10.0
margin_right = 330.0
margin_bottom = 330.0
mouse_filter = 2

[node name="Submit" type="Button" parent="."]
margin_left = 20.0
margin_top = 340.0
margin_right = 150.0
margin_bottom = 370.0
text = "Submit"

[node name="Clear" type="Button" parent="."]
margin_left = 180.0
margin_top = 340.0
margin_right = 320.0
margin_bottom = 370.0
text = "Clear"
[connection signal="mouse_entered" from="." to="." method="_on_MouseCapture_mouse_entered"]
[connection signal="mouse_exited" from="." to="." method="_on_MouseCapture_mouse_exited"]
[connection signal="pressed" from="Submit" to="." method="_on_Submit_pressed"]
[connection signal="pressed" from="Clear" to="." method="_on_Clear_pressed"]
      [gd_scene load_steps=2 format=2]

[ext_resource path="res://bin/gdnetwork.gdns" type="Script" id=1]

[node name="Network" type="Node"]
script = ExtResource( 1 )
               [gd_scene load_steps=3 format=2]

[ext_resource path="res://scripts/Neuron.gd" type="Script" id=1]
[ext_resource path="res://assets/node.png" type="Texture" id=2]



[node name="Neuron" type="Panel"]
self_modulate = Color( 1, 1, 1, 0 )
anchor_right = 1.0
anchor_bottom = 1.0
margin_right = -974.0
margin_bottom = -550.0
rect_min_size = Vector2( 50, 50 )
rect_pivot_offset = Vector2( 25, 25 )
script = ExtResource( 1 )

[node name="Node2D" type="Node2D" parent="."]

[node name="Bias" type="Label" parent="Node2D"]
modulate = Color( 0, 0, 0, 1 )
anchor_right = 1.0
anchor_bottom = 1.0
margin_right = 50.0
margin_bottom = 50.0
rect_min_size = Vector2( 50, 50 )
text = "0.1231"
align = 1
valign = 1

[node name="node" type="Sprite" parent="."]
position = Vector2( 25, 25 )
z_index = -1
texture = ExtResource( 2 )
      GDST2   2           f  PNG �PNG

   IHDR   2   2   ?��  )IDATh���� D���~9=�c5!0Ne��]E�gB{�֊}�1@Y|�Ի����m[�ڧ�[�R/��lAr�B��f&�LVE���[i<�=[�TFQ%~�z�!u�9��t��we�օ�=F�Lk�SH^�T�IB02U����`ek��?����7J�T���-�R��!�F��"l�!�F��"l�!�F������`�����)��N���^V6�����)ί����(�nz�F�O�sE�dz$ }Ξ���|` �<0 �I��vS�����(�L�� �q�����i��gof�Uk�W�    IEND�B`�              [remap]

importer="texture"
type="StreamTexture"
path="res://.import/node.png-7b6b2e65f5cc68860b0123a50d66c29e.stex"
metadata={
"vram_texture": false
}

[deps]

source_file="res://assets/node.png"
dest_files=[ "res://.import/node.png-7b6b2e65f5cc68860b0123a50d66c29e.stex" ]

[params]

compress/mode=0
compress/lossy_quality=0.7
compress/hdr_mode=0
compress/bptc_ldr=0
compress/normal_map=0
flags/repeat=0
flags/filter=true
flags/mipmaps=false
flags/anisotropic=false
flags/srgb=2
process/fix_alpha_border=true
process/premult_alpha=false
process/HDR_as_SRGB=false
process/invert_color=false
stream=false
size_limit=0
detect_3d=true
svg/scale=1.0
         [general]

singleton=false
load_once=true
symbol_prefix="godot_"
reloadable=true

[entry]

OSX.64="res://bin/osx/libgdnetwork.dylib"
Windows.64="res://bin/win64/libgdnetwork.dll"
X11.64="res://bin/x11/libgdnetwork.so"

[dependencies]

OSX.64=[  ]
Windows.64=[  ]
X11.64=[  ]
             [gd_resource type="NativeScript" load_steps=2 format=2]

[ext_resource path="res://bin/gdnetwork.gdnlib" type="GDNativeLibrary" id=1]

[resource]

resource_name = "gdnetwork"
class_name = "GDNetwork"
library = ExtResource( 1 )
_sections_unfolded = [ "Resource" ] [gd_resource type="Environment" load_steps=2 format=2]

[sub_resource type="ProceduralSky" id=1]

[resource]
background_mode = 2
background_sky = SubResource( 1 )
             GDSTO  O          �  PNG �PNG

   IHDR  O  O   e�0n    IDATx��w�$E���5�w� ���9�I���%� 
��>"A$(�OQQ�
�`Ā�#+
"�wJ�
�xw�S�?jz���jvv��w���z�k��{�jzz�?��o !�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B!�B��1= !D?p��m��� ���&ˤ�W�_�,0���i��B��ē�`��%¨�j+Z<US�Y�lg�B�OB�qr��-k�(�d�!����	!D{H<	!��	��%��C�)��O�L{��i�4)���B��"�$�Y���m�4��3��`��"T���
fY���v2fT_ ���o���``� TS
)�'����SZl%hjO�OB�6�:|����!���ӆP�������M@�a1� �7�z�s�{��)�eZ<��A�(!�HH<	!Z��4%���x���% �����NP�	�*q���<�Z`�x�!�,�㯤�Ӝy�B�nE�I���̲����ذ�Y
x+p�=����U�`. .�0��6P�!D�'!D��hJ0��-��`? �L�8��x�)��7��N!K����'!D��pJh�>�
�S�7FR\n�y��`kJ�g�J/k*O!�$��%�]L��g��㎩T��fC��?!+��3˟�?*!D�"�$D_s��}�ea7�\`V�Mv.|�6��=FhO��%�)���S�2�duY��h}%=-� �FP������-$��u�+!D/#˓}Ǳ�e_�n3|8X��Qu/gg@uq�99V�����x��8�4]b}�l �K�nQ��:s;��Gs�Ϭx	(!�M�	�7$Y�}�t�����ON	v+����h�	'9ZQ�+H<	�$�9���:KA�\��˕6��`:؋��e��$�SJ�~@�vB�<�Ѹ�'?�j�����&e���0w��~�Zz5|Ϡ�%D/#�$DO�NE�P�V+w\݌y
����-pf�cB�����Y�Su����p��		�QbW��P�-���7�|T�+H<	ѓ��Y�	��A�wȿi�L.��ܜ�L������ț!����A��\�f�,ۃ�������S�w��`��v��%KY���I��f╷��zp*��{`�knN�S6����ø=�	������*��^L-{T}��`wn'���W�x�g8gY�i��	� ,_����%�<�s��a��\��@�I���������Zo�����]`��|`>΃����i8������`7 6 3�`7����z�7M��T��J��h��Nђ�Mz���$�����Q�`�s�o�fYG����ǀ�3� f��f_`��G=:V�#`g0C�)-����,OBt=�␱@�	��҇�`!�3\i�9@�!�L핈�A����ܑZ�}oB0`0��'з˜��N�����M'�����L����	Ŀ��kא3�dC��M�
8gi;��u+6�X~scXB�^B�'!���i!��������=�t0��N��Y�������ɗ�
�����l;�8��0���[��^���	ѽH<	ѵ����Zp���%��aɧa��8�fM�<m#Z�V��;����7�[�����i%GQOBt-I�į�. ���������a)JbQZ]b���?��+;C���Zt�&f?����z]��_�'!��'!��ch���(�����������e�����SU#	�z}��� o�8�4�R��L���s%CQ*� DW�-�?�
5�ƙ��%`�?������H���4�į�vc���g]Z��E�e���.��Y�e]��� 7D�^Q4�<	�u�H�#u���A�gr�= �e��ɔ�Pf�/F����nuʴ�}�\,y@���9p�[�:�k�N�nB�
��zSK�������\v������'pӃg�.q�˜��=�"h2TϨ壊ܕ"6��UO��[�6��;���^P��}{ss�~Mc%k��:n���|�􈃰0���1����8BL$�<	��L¹��@\��� n�[��˝(��y\9�p�6`Q�A:�Mk� TQ"OBtU�}w*3���vh�\�h�	�NN	gNI�\v�)��;a�5�����P��]�I4����� [���|�������[��nvȴ5	���R�C���v*�_�5�N(�N��@�'!��2؈Y��u���K�/Sv���tQ^���g��W�@�e��B�ē]�@��v3�!^��30�>X2��}�󑺎������P��>��IPyO#0�NBtOBt'e��?݃"v�ԋڂP���{�SB�G+[h�ǁO���Шo���x]
!
C�I�����X�N7���
�7U"��n~X����ka`�H�BDF�I��#��>8RC�Ǩ�ke���Ye�M�Z�������F�E���$���:��1Er7��jn���
� �`.��W2u�\�Bt��
���9g�˳�x��,��n1N�鹜���4��%h}��#Jj�ʆ���%���dB�6�<	�g_;���^B]8Asm�^N��|�)���K��Yݹ1u�M~*��T$���
r�`;��Ǟ�,Cqv�u$Ǣzv�R"X�I�nA�I���>�>����h,�ET�[�>W������[�w*MBtOBtf�v���i:�H���a�+@u��BDDWI!:�}m��a���bZD���N&���l��DTٶq�%^���+��9��83BG�OF�o/�8�;#�wF����!�B�I���s�7s���2���u�ߵ��eg䭋B�NF�I��'iW�x�wrb&�q23߱ē݀ē]���Z������W������TE�	�=H<	�U�_�`a��_�����{x���T7(x�B��H<	�]�a��"쳗���]�Ո�K^B�NE�I��&��`z�>��ayT�/~�v��!��'!:�}M���C�~�}�.�{,��'!������9'�e�wy�
�a��2���`QB�B�I��&w��`y�x��E��w�b�}T$���"$���hrV���[=��@O�x��eB�B�I��&'�"�f�P���%rS�K"��N*z�B�xH<	������❉c8�����#8t��'D!�$DG��IG�O�xj�o�p$���"$���h|�ʅ[=�o3:"/#�$D�<OBt4��|n�7ٕ�M�G�9���9^��b�{.�!DaH<	����V��V`F�}�23#�S�'!��'!���M-��3����hdyKP=�w�"�Ϫ��݃ē��Z���4\����[QI������yM��^<��Bt*OBt6ks�C�)�q��q������6��U7�*�����S�'!:�5p�i=`*�&8��#��S�}� ��k���X�T��ą���@��d5�`�T��7ug�,����sݿ�}j�Le֟3w.~z��ɍ�g�o!Ddy��X'���ɻ�?&��; ��©_Sd?w�S�nW|_����B�I��A�I��������7��W����`������M��ON�,~�B�XH<	�9��N�m���n�?}(Nd�= �O��~�>%��d_Nx�p4_*D� �$Dg�<�;�u�)8k`�;�V �,~�O�Of;�!D$���x���n�-��d���U��� �9?�KQ0�/~�B�H<	1�,� �vto�"� >,�_ub�>;_^'��u:2N�M�㾸(K!D�#�$�İ�i���HSu�c8>�L��)���T�|�����X�a���O�I��Q�N`i�-�A��M�̳v��f�ȻG��`��ӗ�*pF�.'��iv�O�+�T`�8��<��(�.0\|B����I�� � O?�	�������eM05���Upz�K�a��5�!�p��+��&�w���E!�$D�|x
�
8�Vl��x��}&أ��j,Wk�^K_�8�[��u��~2R����?����ēų*�ࣸ�M�X|�K"�|2Tσj�a�I�S���ק�j`*`�L�ӧ���?a���k��+�(�'!��I`[����E�c'���O8u��)=����\ɚH�pNO��W�o!�x�x"���i�`p+�;"�pj�����ԭ��|¯.�v N�׷�������FĥHBt OB�c���ϑ���	Tc����O�~"����o�@eu0��#Q=#%D<��h%`�xcB��'!�1���e̶ٽ��g`�F�\�ٕ�+Mi+��Y�:�
ub�U!鳀]쯉���^���%���q\�E�I�8 ?����e�T���pŊS����u��4#�ki�r`��9��}�۾�?��6���#�OBO�.���,�PKZy1�@����R`�Fh�$*��T2M��'���T��v���Ǟ�GpE�}o8B��"�$D�\R_������ٽ�^��ܞNg`�ck����+m!�xV���?{*�a��.�8~ �����资-Bt��뀝q��o"^��}���u� 7����������	 ��>��b���U=[8�� ����csc-�/���L�7�|�՞�AX\	!& �'!�3l�+Ӳ/������ ��	�N��M� �P�2����$�%k��|����B�.���}r�(�����n19&gf�;���(�؄c@����V�f%����e5�G&F`5�V`������5�}	�'
�#��%�8��]SZ�$B!�z�Ch'ʯn}��3�}�����Q��/dV��Y)�n.z`B��!�ԡX�DB��C�M�'���͘�����2�u�I[�s`?��q ���K���^�ՏW��N��:�ykND��/�T��Y\�����i�y�	,��[y?�l���q�OB�@�C��x�Z�	%��,S㢂K���H�_��Z,IYa�����`/�ᣁ'�V��x
�E�V�S<�,"�тuu�g�m	v�ez��+��n��/��.t�B�1!�4�X��o�a}$����v�S֏�M���	��r6���*��z]L��oO���(�e��Q�(��1�
��}�>�3}��?v[�im�����8��9`=�uɏ0hq��=NC���B� O%�Li��)���O�Ӂ�X�1���b���9y	Wg�%�9�,�e5v�A<�����r>���o�W
�����wp����(��:�>'�,8�q�2���`6����ulC<���"���C��P�㼏[�8X���j��B��iXBM!D�H<���8��b�D@5�Sؐ
31l�e&0� X~�CY�K�x0�<,�pO���KւE��F���
���K(��,�4�Oh��?|5�lp�Xk�uMǓ��փ���<)��\C?��|��	��x�ơlO`S0��= X�����砺%�[���rQvY�����ᄚB���x�L���l��?8�UvŰ+��q��b2��0�f�z�b*;F�x�o*N����zpM\��$x�ux�F}���|f6�-� �0r����������?�s�_͛�O+��`6v��V�<�v�`��������(� �>�i��g��BC�\L{�����a��py���X9���b�G�M��j�o� ���Sz+���k�oÉ��-=병U+�ʇ�P��s#!OɌ`�h0c	k+����`�w��d���pֳ�� 1�{��7kM�0�t]�M��j����{hL�B��S����Y�ӆމ�`&~�!�0�9X..��r;�	�F�YL~��,��!�8q�,~�C��IM����Y8��Ԯ�9�Z9����E�h�o"��j�	BoƟ->��&��$�
���4�HN�ax�O�}U�����snz�1B�XY��|(��E����p��خ�1|<$w��$�{��Ƿo���sM�Y5j�t p���)`m\�bPa��9����e.��>���OQ�!*\�oj*��e�1�q�V�',��q����!����NྙÐ�q�ث���x���1���^�U����� �4쌚�i�ges"奀O��~H�����H��T�>IA���7@[Ӛ��~��Z���d��+�����t<�)a	������#h8p׬@�����},�]/|>֝
!Ə��� F���;�/Qn�������˹8�^G ��ݰ���Ȗ��O_��Z~:
s�ҙ�P���w��v1}�+��~`^nnO���C��?�uGT,X�	C�iԧ��~MKH��p7�kp��z�I>��^\r�fґz����[�%05�����e�v!�l�+fX���m�q��-�������l̾'�q'p���+�!��x%�~r��;�0�O3�|�����A�����%.ɣ?�(i��g�i�Ĕk����|5ܐ��}���-�w��{Z\�'P��v�����G�oS��||#�~0a�.�����(�YJ�j5,Wc9]����Wl-��ś� �V@U�������¼�Y�w���qyH(_�V΅ʮ�q����������1-�
N�|���/�+A#�(��6�3݋!�6���ڙAn��"�
�/j�P�%J2V�.Pgo��/N8��Y�.ɡ����|�A@8Q�\vs0W�C7c���̀��)�\�T]�a��?�C!Dk$�F������V�=�.��|�n&��~]�u���:�Kr�������R�����	'j�v� {�=	M�%�RP�<���	��c�����z7^�c!D���Y��G�a}�\��d�V�����˶޵�{PX�v ��b�ɸ�*!B�Nw��"���4	��*p��qŢ�����8L*��'��X��<x��J[ D���&�,��1�V �ĲkYc�^��c������`�L��d� �8m�}�������)�p�Ι|�g�^�%\}���6.�\$i��&�8�.��HQO�Hn�y��[����1u?S�KH¬~�(�V~��>n�E?&%�5�����i�Q�y8�	�BZ����,\*�����/��S�9����ҸsOQO����j#��V��3 �_�˻���)�I���R4��pY��;�~BV�a�1�l\�IsdX�r�$��M�����M���V�Q��;&�j ��:B�M�@�J�(��^�z�k	[D�,'c9���L2�7����"�B�Jp�u�dYB��x~�C+�3i�Rp�o^�Du�S��Y�^셳��i�_�������8B��S��T�%��`2�S�*\iQg`8��%1z��R��	���f�������}����t�x��l	������-j"*S���y�6�Mq��?��6&��Z�&����q\������݈����T]��pBD1>�1���W80�L��`�x�!�c�ym�k��ko&\�c8>�����&U�.�-�A`��M�vU�`. {	���b/�9����_<�C�:��^�E�Fݿ&/������T���c��A�9�/b��e:�������y'"n�Y�j�I�j+�`���sq�h�r$�eO�b\��o*f�9!����t�a��Z�Yt��W��<��?���lx$�Ô�v�hJ�g`kO�)�^�ёx�6]绎��kV�N`�����^,��=1���Zr�:���8�Z'�o�lIR�"{vNsg)�X�����8q��7ߊ_��H~=�����rH<%��nx�
�7BuW0��݂�� W���U�}ɒ�HZ˵O�m+7    IDAT+�>�?������j
!
���S}����q8�eB�2�p%�q7mw3=@'��*��k�X��r|�'�?�S�\�k)vJOӁ	O�ރ�B�jօ�e���98�����iK�SZG&⩒�� v0Bu���ՙ��4'Q�%8�t�yP��y`�ϹM�90����ui&�|4,<�?��~�|dB�HH<e�����q%6�*yH	�`�
���j�����v�Sr� ,�p�O� v��r6���r��:Ɨ�*���>��Oo�_����X�=N�u�)=p'S������2 �a�T�,�eq*g��`x!LZ,l��������c���SB�[�B|7��*��"}-�ry�jΧ`f�#�A�b�&���J3�qȽg3�S�h���-�}�_��q�S�f.��N�ǰ�c��{����,�>;���M<%�Ƭ�*�(}Rd����xJ�����x0��`&0��	�'��x
&`tG�",�+o4 <��x,��Ԯ����vsw�-�Y��)�b��y�[��_�I�Pڰa�8�|'�� �q�W��]h�bN���$��3��'h�v�SB�Z�Zq��N� G�<!���<��nvea���(��D�-,Hc��xO��Sc��r�M�>Q��ȵZ�
���y3�a'2�;�$��TϺ��[�z����O	=)�����>�s�d��)w8B�})�Z������K�3�A��{F6��#����
_ �a���nō`D�$��P���8߶������ȲlKا���}���O{��O	R"��ĕ�E�\P�h��$���,�Ra����PmZ*�U�ɬ훎+��1���6����c�"���+�cku���fʸ��V��0�:��f`�����t/����'b�9(�)�]����ʳ�N��.i��nʏ�'�ҷ�Էb�/��Y��\F��a���T�W�-6�ck^�Ŗk���	�q!��u�ճ�	'Q�s���%�E�����S�L9i	N>V� ��y�T�MY�=5g�tI�,�5Uv�PK�u��Sj/�n:�=:���GS�K�n`>�s�B$<������2"D��W⩅��u�ޱ��p8���H?+�D�0���k"1si%���a��&&K1ȱRM��$��~�pN�EH�m	�(,z�o����U1�J<��T,�O��2���h����L�O�~��H���~RN�����x;.�w�#)�Hoh��_#΄H����}(=�=��l�%j�3�sRņ������m���MH8%܏aw�uĢ0�!�b��h�^�5�C_�B����:��%�|;���48H�X�xjq�=����08��̗kol�A֦��Ԃ?��]��1��c�W��k��=�������2�*�N�N"߃ZTj3���J�=M߈� ��q���6���#��A8%�{�P�UX>q�P�Tq�p	�֎��
X9�~..�S��o{©��$b�/�	3/s B�:�%���n�C�W����{���4͔PM7^Z����Q��P�!XG!s��@(.�)`/\ޥ"	�;=��G&DB���~xB���O�(;w���"Qb�)��Ŕvp�D��V^�T�0TqYӟ�������b����ֽ�<2�Q�	�;]�/!� ���>�`�c�g����θ�O1�8?��-NY�>C��S�����;{he;<���:��<����+ ����I��[�����.D_�?⩒{�����*�Q�T� ����?�R�n�����r#��Nx
\z�8�4;�mI<���Ɵ�-`���"DO���)e7�P<�o�G�^sӏ�d��
|���m�R�o�~p	�������Y������q"&����8.D��x
�0-�~�`9#���"��L�T���E���u?��>W�E�-x�M�aQ�ԠU~'!��<�.{�<!z��O�(���m|X.$�|����[�%9���RlG>�R�/�_�+3�4e'���fO{��B�<�!�*�x����t�Fݗ+/T�H�0W�0��a�M���0�4���|]侅H��;���D
ѧ��x��*a��x��__J��^�:�Đ�@#�IZ�L8��`\2�؄�����x�,.����D,�O!���Ov37��+Δ��B	�(�u����"NݻD���L����п�;�Na1�d�9�1zZ<>������חR"�o�N	���pو��ƙ���Se��:eA���X''1|�#�v�&%�E�����S��y�^.ϕa�S���֧K"t��rΪ*��[�����5�^n(qB$<�>���eD�^���S�Pؠ�}[���̧��}����܌+�R(�Mf�����\���U�N���!D���� \�l!�(��0�jĸXT�SM�h��#P�opK�}͘�m~��,�LB�I�Nb"�-��Ӂ��;!z��OC�׌�<����KeX�L�߬p�������E�5�P�����$&�*.���#���Bo���ͻx�d�s�^������lOW9>VM�V��솀����?9�f�K�]Oo��m�U���a^]����5�\|�^��l�<mwq��!4e�g�EZ�(�g�_�)m������J����G�xx��N�����E�{`��:MىN!�8�N��eD�n���S%�*��a~vzPQvA,���w�� >R�~G���o>O�S��ycN.y,Bt5�-��a�����^�W.b\�j���X:j�ߑ�;-&B����[������!
��~,1�S����΋E�pز�/�g��!�[���9!F����N�*�� �4~$�J�.���`p7����w�J]�Д��4^$T!�q!ڤ���Ԃ�����S�؜���W�,z�m��|`��c�N�@�[�����J���%�o�pAZ�ǟ|,nD8��@���������J��.�\�4!���x*��Q!\�V�)|����r8_^]t-�;݀���s	�-�0�́э��x*���b7�6�O�p�Oe�+�lX''������}U\]H!D$��� ��3�+�x�|��e��ba��6��E������$�D'�2���:9�1�-��7��#�Y�ahz��ګ�kk���X�?��-��!���D�[���uܯ3�.��n,� z]<�d_c�0#+��ƅ���8����E��%I�Aܓ��E��!��t�Ѣ�pD����Fo��<��G�ն�c7�5�G'>����|���-�>�NSv�[9������S����\C&$�'��y#ǳ}ث�~q~US��It��/����5��x��<͋��끥���n��n#�w(����+��lP�6�b�]����_|U�Nd�N`�ǅ���)�|���a)o(x�]��\r�3��S���j8U$��r���ŷ�'~-�u�c�+�7��2~����T���]��p�G%3m�a�Ͼ�Au;W�Ň��D��$py`�G���B��'�{#��@�hz����$EA����&cy ���>��S@�;��|��It#!����(s Bt�(�n���-� /'<��!�,��G��W���oY���2�;=���۸���4~�Bt;=-��\os�@��խ-)�K�DIQ�5����5/y���\x۶����uH<]=��
1QX���u�#g!��i��O_ � ���f](Vr���p��O�å8�w୧1vG��MCC�>������>7U-��я�i	pS�������J���O-��O>���^�FM�	�ǁ�o��س��D�w#�$���K딶@��(� �M�8��TO|Y�?���+�y�.����j
\�fC��1����xx���9��K�!��_�S~�w�z��e��R���Ǐr�e-��N@-|2R�W 7�m���¢�H`�Q���(;��
��i>X�X��Xz��t�pg�9��8�:½8}睮k�����tl���xچFD��h!_��$��
m��*��Ç��D�p~��0��*DO�MSf�[�0���� ��I�*`��P�xJ��g��X������64w�
�Y�[ M-�M8��g���yp����4�jc}Ή�6�?YNN����w�:m�)��%��wz�^�r���P���O��51�ߒ��7�4G������2��';/Sac����(9�C`b�(.��gP�1�xK���>_�*��[�%�L����E� ���� �x��a����XO[Z���x�S5��OG��r���W����_�觺��PJDR�t,B<�O�4UG?�'���x�9�n$���p`��-PvC��h�q>�A��������_�.ޙi���5�`3Z;}? ��i�*N��1�{�ͳ�%��HL��,�C��>�{�#*_La�L�x�"15��x��oN�}7�ݞ
��Nн�)�g��6˟q)
bp�]�~�B��S8�,G_��F\4��?jm����~p�r��-{G	�'��EF+���m[�'_�SZl��\��i?8�ܡD�W���qZ$UR۔!�|��y-���/�?Y'�J���*�=����	��17�"�����8������ġ���o��R�:�b��B� W��;
8��~n�.?"x���1��o�&{ױ����i���i�t���w�� �K�K`^ ^��< �T���){yK��řuc�Z������,�&��r�S ���t~��tʋ�51l@�0,�e*��X��aX�e!� �*�,��0���b��ш�E���+��B��'pS>1��3�-0�'� �殈#(��h��� ~L{��c����:��4J<����eMβ�W���x=�h̋p�Y�l5��{��Ԝ��O������Ϻ�/+��A�>`عP�xx��)A"*�$��՗��@���M�ʔ�Y<-lO�-1�Ĳ�D���ǁy�|,�b�쒷���Ӕ�v�FE�o�SL��|c&�{Û�G��p�ڹ�xv�LCr�iP�b"����E��p�x�!p��? >�[(r�� J?Y?��re9�tx���)�������V�=�]q�ˬ��$p���ߑ�����'�|��C��}����n����^���:�ik�^x�m�l���,��B�����/����*��H�F����?fڂ��;0�s���	_sO�Q�;��{u����ɘ��#�`؄����Cq�������>���qn
��l蛩O#���=������J0��Y%^wl��O`.�I� ϶/�&!Kk�.}����(w8��J�"c �f��� `/�?��<�a6.�6���թ���*�,�L_�'���4ኾ��p7K�x"'�2ڔUe°��_@������J%Pɉ����O�-���[˳����������Y�r�]G֧)���$�� o{ .Pg�l�����Q/�kiDs$d?s_��_o���O�ּ��4�֧��r �R��H�*�{�X��z�NY�4 |����O�#�Q�k%�a������t�`v���,t>�������t;�ד���������žw����8./�7��{�w��ňd�����iM�`9\��#�WFZ�<|	8�9���SZ8e.��>�dO��Յ����zxhG8T� o�p<1�\���
���N�SX"�z�<K�� ��(:M��Y�an��n�_8��ٍ�O"Q�/{l�����J�/I©
�ax�K�}�@{�5���>V��p�1�������Z)k�J8G�q)��Q8��8_�s�U\�H.-Y�\_p�O�4�A�Gj�*y��$� wc�1�)��}�P�^�\�/p%-��j��j/1.�����	����r+�J���Qؔ�E��ˡ<giJ�ϊ:wAڞ
�w���7�<Kz�rG_���N`������V�B��Vi e}ZW[�#���5�,ĝk����d�� ���1�)�,����N,G>u�[>��D⥧�H,p9U��ɝ�ٙ��P>�R<���H��|���n`�C����'�BSz�B���+K��u�'NIO�
�>�]���[�Vq~IϏ���8�n-�E�T/��}r�;����� ��W�U乣0O�=�R"I�f8���M,+�}��m���N�l��lp����:�7�R��7p;.��O8�;�5�7&�nڮ��ݴ���=�F�S��Gܾf������ڨ��bLYv��E��	'p��9��N��&*���x�݌.��-t�p���zӜ�`���B8�Up�/�P��������)�%I�gb��}���w��s���'�	`��c�_
�D8%)ؒ�N1*�����S�^T�5��0������Sg�ti�d�ii����d{��<�=�b9ط�w�e�;B+��+pB/�����R��@��ƶ��k��O����|�����6��b0g��zvO.��C_D�m�+a�e1.�xy��i:��[�/��S�g�S�e�C�'�,N��&M㵍,Oஅ����21���r!���|x�M��I�́�`�+#�3z�)K߅FvO��r�c��R_��R���ׯ�k�.����	`2���B=�}V8%Ӝ�ў>�jn���2�p2��I�\�)���=�
�b�)�.� ,{1���'N�~���o�ԬO�y�A\ảE������j��W���8K�AL�g^B��q�us��|�����/�}���9��pS7!^V \�	E�5]E��}�gͼ �PಐS��
|��Q�O(�������jGz�0[�YU֦�%����;��Z�R5�䫻-?��{�����Z���q�g�ǽ~�0�7Paa}�O<-&_1�Y<Up9�v�%�܅N�4�dؓ:���� ����}N�;�V!.���� �F�%���w������	������%�9�I�p$�t\z�e=��¥0��ϳ�Y@�÷�M$Xn �<�N��������Ѕ��x�Ob�˰;�¢�N����0���s֪�/f1U���0X�
`���`ۚ��i.�p `}��6#o�o9;����^<E8�q��V�s4����
\v�R���\���FSZ8����"�{�o�w1��'J�G���_�X���i��$��v�W8��\�D'Z���gN�_�|�br-��uQ��L	�Q��98��~%~g�;�mC<��ܵ8�Bc��'��
���o�����`���'p�S�Wl��n�R�Yd*���N^@�,u(E�<�a��#-��]�Uϋ�@����En��E[4.��R�#r�K������e��p���M�h��im\�	���Z07@�V�'�z���g��PtՁ�T]��� �]$���+�\���(�A ������'z=��ނa��w`�E�o��:�ѕaع�m$�5���sjC�7�
*��3n�I���p��r.���C(�8t"���&w�Xg5-/ʯ7���ǵ��y�\�;���"񔢥������=�bx�v�YoI;��1��y85���`�umn{>.����%n��5�\Ay�uz�5�z5��}��L�y)��N� �3�go��j �lF7/�b�>�Z��p�s���^��p��ϖ8�^�����q��o��mq�{��<NPw�%&�Q[\��%�uˬ�W�qb��Y*W�R�j4%�>�x��j�tq��P�[�	�?��`��p�`�O8��Ԅē���vC8%-p
�#�v�q+K�x47�^c���Z��vDN��iI�}7�cm�72�tr�./������jX�kX<�q�d/='���ܑ�M
Z����ڷ1�
�a��_)�D�N�<-w&v��?a��X�Gg�1�t.�͒ZX����ڥx�uiV�����	����⟶����#���{BS�%�ܜsN"��_�}s�j�g�y`�� �g�.�{٩�e�Au%0���t^�jS����)��E����PI��0� �C�^Y�8g\�������-Oay	�KX��ʭ`��    IDATd*L^�a*�uq���\��f�xg}��UI4��x�z
��z3�ٸ��"���U���l8e�*�������N��-��߇qf���{.��d?������R��A0�s`�|�5�;�����r�@��j0�3�R��*�9�Q�FO	���𴿀�{i�J�pG��Ԯ�����<��0�
�2�ݸ��ҧr��:���6Ӏ���H���l�X㌋�Nܵ�թȞ�OO�+ ���s1�����|�	�8�<j��4���spp����2��5�?p�#T��LJ�>	���9[��q�����W��B�7�O�AF/��R�M����磴��/��;Mz�\-ʵ=�FzPI�
8�������Wfw ��o��bp���K,����66��ؾ�*���*aز��1*��^9���#T|�����c�N5Gr�A5s[�"�z��ⱸP�cw�;=�N�f�Cӂ���Jf���H0k�.�%��f?���0p�(C��V�� ��K��&��q��&��x�{��0L8��-�7'���}O�O4m1Lb�I\��	�'�U����(üe�[�P�?�?n+*l��GĮ�#��X:��s"�xj�8�0Α|O\Y�~���.���[d�{���U0!�.���B���M��,κᓁ���V_+m�J�����^����x3�z��`F��FI��e� ?^�+A:b��;�6eK��r�6�`����n���y��z�Ժ��Mr���H{9׊�a,G ��`3��~�jjwJ�x/Ufa�ပxX�E���Z{�&�p��i�QR��K���oY˩>Jg�_��T9�
�ϲ̱2�D���j���?򴯈�]�{�x?����ǟ��R�O�8W��L�I0�5�Z]b|���Zl��m��ϟ�i�ͻ��C����i�I��D��}
Me� 7	�?.�}���-0x9����u�{]��,,gby9��d��])�6B����m)�$���@�M�<�v1���jm�i�4D�N�I<��i�!{]L��㞲����N���-=ן�D��N����y���f�δ-�{:e�~W���q4��=f#���@��~	�g]���2!�# N�%w_�v:�Y��wC^v3�>�������ډf�l��<#�<�+y���I���H��a����w�?��;�Ci��0O[��O�m�M�AJ�#���,NOb,x��)!��t*���aP�X�����'�
�3�`������r7n� ���?�V�cK����ʳ�"�!��q��r?a\�ȵ�
�>�@0W�ׅr|�Y#���d���#T������g��g����X�;� ������O��Y$CN�ʩ��<��!��!�!�����%��ZV��#ʳB-`�Y$~iiaW�/����4���^Q�E���s�}4�Je	p!l|x�~F����}��ݥ	'��l�?GP;�NY�|.�n%ܴEć���8R�pb�́+��_	�O'�X8��J��Ɏj���j֠�GL����ۿ�g|�V&�ht4�;��g�;���<�a'�*)�r�>�x���j�!�1��RWf�ҲT9�j`l}���8���đ.|�\�Q��B�GJ���,.�0gB��t&�Ϊ긳����pSY�L�j�������'go���������`���lO��{�c�8��3�V�/��j�z�.��7K�����5��Çp�hcŉ''�����q��n�������� �h���ګP�DS0m-H��v�|L1���j�3���K�H<���v�Kp��a��/����cb..m�h�?��+�� �{�k���e�ݻ���um������so����'S�T�9�`s�O@%֠� �r%.[.���Swd�*����?n��y��E#̀�"�h� ˎ8���\�s^���f�����Z�5\{�Z�~����Oq��P�r�x*3w����s`_���`W�z�c"�7�b��y�,��B�J�St!��������~����N0O�/�W��p���0�NN-�֥T�$�<�pj�:��wW ��XR�m���D�4>���r���3�o�=qY��kX���n�h������_{u�*�g�"���ԗ�^}V�N�)#�����y��u7�c��pyj�c|#0��ؾM	3��y��ʬ���x�
-�O����|�,�nK��ac$��Jl�O�B�c�Y�˭7�P��aۜ�߉���,P��|�����C��>���ge�1�� �����(n�E��ˎ�T�9@'ӆ��=��oÕˉ;�g9	�����Q�]	��M�Y�
�̀�1l��%m+҂�����b��ј'&�n4L��Sߴ��8��h<�f���+���YWŉ��T��O�r�f��ťQX~�����롻k�%ǭI8�����Ҍ��:`3�x��i���t�,��\�%R��ۓ}(�^�;=���+���)d��Me��q@�ΈH<��7��HAJ	�oj�*�2�`&U��Qa:��qևi8Ѱ�s�B\�΅X��e>�a�,��1�'�	p��.�YNƥ�8�%9�0�J�d��q���A`3����.G������N �E��3�D�\	v/��7e��Eb��	'p6�'�W �8�湴�pJ�n�Ǫ8������a�O�;�wOz�V&�E;��q���T6��v^89,��pv��l����������F�6�,g�2�yH*���ߗ�o��/��P.�Ҝ�^�j�T�l\��,��J|���qӝ>�K�f|��pe^��ؙ��u0wCx��9O��͡���)�!9a�I8%�,P�����;jN��OkAeQs{G�'p��qi��w���Y��7Xx+��x�@w	�����+}�/_dkۑu��#�$��	bT��	1�y����������ވ�c�X?�,�B��X�.���i.%�ƿ��,��S�7��F��g������>�ww�]�����"Ҥ.��t��� �ÊTAA������Gd���(
����R���޶��w������$��srS�$���~��73ɜ��d�3�:���8��ԍD�@aU��W�D���f��<w��aD�}w	��ϋ�{<���zo�Q8�8��RDҧ���������Iช�#�a5;D)95���Nܥ�������w��\�N����g�cHޏ�EZ�N�~Z�<zXs{.p���F�j7�"*e�dH!���^��S� _\�4��&%d+��d[iN*�ɯ�[�SD|�q/���MaTw�X>�eѶ���!*�r�y$����l$�J?z�9�O�x�#����_aī� ��<���^d��lq�>^ �]9#'T�����W�;�z1��{����&˞ǁ����X�M[ކ��KD70>���KH�87"�j!���#�g�}@�h|%#:T`�׸)�d���I6E�NG�]�����`�@ܘx*����c���N��l�TU��'���4�`ca�')�'u�}>�9��s��-,���E$�F�����Nq~N��w��,{��}��'�rM�D����=���[�w�3����	�ө/�)&�"�n0�ݭ��U�-P��S|��pJ�X���-��a��ӷ�E�v4��n�(M���=�9�!�&�"�ϔ>�^�*�f��9�ڵ�xR��ۨ/1��x�$O"]�]�{�H��VDb�P8�Rf9Nԧ�qҌ�`���@l��M�낙>�C,�l�]kyc[��+��"�O���H��Oi��J=b٫r?G�ԇ�'�_x�[��HE�,�pu'�]��l���%�"*/-WB}�x� ��ЀٹR-���8W,6��w��t�QO��8`� ~�������OϋD�q6�������ӨxR�	��.ɝ����^�dO��X��*�NX款�}�H<���l���E��[���V�+���h=!WB�e��H�}!.����T}�?�ΚP7r�Fœ�O�+��tفڼ	�m�R='�\t�ƾ\{N���V���-�r[���D7JIw��� u��@���x�-�q���]��_u�������4�j�^V��n�Q���O��7�*�Y�\J���Հ53�^�;������v��
.��l7$��FR������%*M����\���>�V��S�iAƳ쒊{�|��r��'��x�����9d߲&"��"����x*r��Uβ#T�V���%QbR��W�0�;��r]��H���<�Fb��\�q@/dNӰX��^�#�೥����>���QQ�-��6�U���p�(��-n'I4�j1�*O!�E��=�s�o���+�I��݁� I��"مR�.�p��/��x�S2��p[�F�\w����a��zOJ�1����$�N�����Sk��	_p����[{׵J�s��MV��b��t�� �y��s����2�2��Jc(q����yL'�:�`�n�yDœ�o�&���8~��c@Z%�<�Z��B��'v��H�������~Y�����As��X7^#3n��My=�)֩>n&{��XF�ؽ���_��4�C���E��p�$�*��N�kd���[*��>���E��	!���O�]g��W���Z�G�+1?�-��w�~&Y�A�ϛȍ^��tOɲ�Q���xz��O��Ȋu����rp�ڈ�ʒ7p�����=8?���[4
�'s�>�+�G��X[�쭧����Ż!��`_���`J��AA���I�7�� 3���"r����}M�7�$0kg����$dE��צ|z,�mשd�u��)�X��3���3	b=ut��@�����-kw�^��xR�������yϊ;��-�i��H�D����[�1do��vRW� �)�^{�w  ��X�&{���[{�H�#� ǰa�~��魦�oԪ�4��Sg�z/R������n�R/��81�{��o��H�B?�����#�ּ��:���a�L�S�õH��g>����3��xR���ښ�@��)�I�ļpd��F�����e�M^�8�����]�w��t���#b�}�6��T��xz;�[��ē��~b	��w�6��U�7��������uh��i3;�U�THe���~����wa�ӜԒ�9^�Dd�ӈ�'��ؙ�s@��}W�����U�O�
xM�p��a��@�ƀ�xʔ��2[�:����1lY���OJ?QOLɢ��BO$��"`C�j֚*��l���h�������C���!<
Ղf9�d�bo/רxR����"��s1\���nM�dn�O`U�S-R�� �V�)va�W�]�բr��n*��+��$�o���s�Tg�+b�
ҕ� /� Y�S,�U�g��d����EH9�<�Y����2��D��%ٛ��_&�+0ǨxR����o����f���Q�>�̽d%^�Z\�OM\�)k��J�!\�gV��6�2�� _�`[q҂�H4h܇)�'eI�8��n;�_h���R��&2
� �C�1�\Y��,�vM�`�n䛮�Ke?Nq��$B/a��:|�33ߺai�y��H�t�QT<)J�O��=������J�W ����Y'�F{�B�19�1�r'd���s7r|����@�X�Ѱ�${��n�cf���U��+�޿=��4���q�HL�(�4�7��T�탳^���+s �T|�����7�^DJpl���t�9�͒� �5���o��)Υ�`y����'��ճ��%���\<���n��|�������ڔ�Ĳ��ڋt��������	�B|:��iQ��G��]���Fœ��\v�x֭L�imS�����!qMM�y���-f���t!5�+�kd5��p�d��N`���t��s!�+o`��A��i�c�Y6�F�Fœ���������E�֧�H�����7��[�d�-�ܵ۩Z��E*X��Za��$���t|/�p �ߓd�=��y�0�����!,�x.�d�/�R?f��{��x��:��u7����Yߌx2H��o��J=��M�(a���F�#R1`!�S��캫Λ���������.� nq)G�M꺫��A�?Ȇ�Ѿ��~���>K�p[�������t�"����f�Y��O=�>_���.� ������8C����?����+�6�	2�IX6��n&���+��ㄪ�ST<)���Dq/��k�x��T&���Y��f�}8�vנ�=�����9c��
9+�a�c:r�|x���l��,����a��s�����/I��h��#F{(�6s��'��1�� ��)������o��xϗ^I�i�P�y��5�=���ՙ_MY����xX��
��kI�ǁ��J���OcEx���d8����`�X1��Zn������J%6@\.��wy^ר�np���e"���
��i*�0c�`Ï�=�"�B���b%z5�<Q͋AD��X>P~��|>�hy�(.�07 ܘʭ���8*��^��4ܑX�e�8�/�}ȶ0�|������/m3	ط�<~�/e|mi�G�exS�ު�.��w�y)�Ȇ�`���~�%E�.|4��/"��}��'����;��M,�͠���,��b�f#���[2�fs"Ӄ�0�q6�ˀ���,p�4��%����R�e�.�R�_2�\6o�1�bF���:%Q��*���\V�{q� 
rm�'��la�,;؛s�}w�<������)�uJ�ٽ���"�@����=��r�n��1�E���1qw]\@Y&b�b�17�3�zF����'�W�1S�p\����՚4s�,��h���h�����T 
'��{U@�>W��������.8{!��5�~Ǔ���?�H,0:~8'�z�H@�,d�!��T=� OJ��s��'ޤ���n���ѱHpp+�,OgD�/nq{>v >�_��@�v�H8�}%����a�5uQ]��8iu��ţL$��bQ8��W�^P�g,="�Y˱�F���O��#G��i�&��#T̤�9x�SQ�}��Ճ:�O<݁(>�5�H���æH��V���☗ �ƶ�M�G��`u|z��8����4n�pe��Ѕ>�q�ρ#�z�l`>�H�^kp(����"�hz�"�c� ���#��b�VV!r�s�Z�r��(��H[j��ӏ.e}�Z5�����<آ��,���*�Ig��	���m��� ���eq��+�;Wv]YȜ�h�k��مb)�2HR�-m�i�b2N?�06u��	c_�� ��P2�4!��H
!i��qT<u�q�#W?j�r��V��j������h6�i�0��Y�Z��y�_�r��iMΫ�A
"�8�����Z�7S��K�sٝ�zq����5�fU�v�l�I���.wYl��({0����ei,�8������FT<u��e���Nv*eQt�U�W�i6��Qޛu���w!��d��׬����5`�`�R����X��1�Gt��VT\8�D̊`/#\��W	f1l{"��52��{LF,�a�|��~��B߭�OѼ��B��2��o��:���u�
6b��##[�iHpX|V�1�cJ�:0q	-�,���R�hN����y�����%�gׯ,2��_�!|b�6FڹwD�v�Ŵ	{8xӃ"xO�O �|n"�x� ��G0��]�\�A��'�sN7||'�KJCC�C�C�1#�"���!p��rc�wral�˅xQ5�by�p$�U;N*a�� �w���p���On��n��s�4���OŲ�>�&ٶ�x�0߇��n#j��a�a)ˁ�Zi;��c�ב-���m���O������Y$hgE������ q|��g"�aR�#.� S:\� �N��i� ��3'�b���:�⩵���Y�#�	��@�x� K[��0H�ϒ�ʼ�ߤ�N�gI�����5�-{"�n��}�e���R�j,y=���OX��r�s�Nؓɾ���H���W�?%�    IDAT����ŷ��� 6�������m�$�^�݊H��O8]��4�2b5�_�:��� f��yC������;N �n�SK����	�X8�OJ8���N c���w���ɍy���,NI�]�Gؙ"!]�`�� �רx
@9&��j�K���y�y�XZ2�,� ���r�W�_��^�&�|.����j�J��"��me�����$b��KT'�&lzg�P��ᮐ�!�$�_�uyP���~�_z��� �V�m	��������+�E����'��8�/�T�Z@m��O�9(��0�~�[�x���<����`Db^�e��]�)6�;� =,V�zE�{�(�x���C�[)����\^B҆Թ��ȱ�&���I{oK�ƹ�wF�񣿤-D�HV�K�GZ���ܹY��m�.�B�M�#U�0�F���`��X��rɞ���y�m�<�&���u�]�+g�ݱ�D�}8���%Ľ*}��y���c�q�o�D�N=�@�S�4��x���EO
>�	�����"���[x��n�IϺ/#�u.�#�і*k �Z���d"b�
U�(΅�稫4^'b�|F�x=ѡ��r�4��]�(���n�K��q0��U�6�H�p9�V���x����s�!}DЙ`�/$�'K����c��0���>HT+��,��'�1�g@z�V~���b�:�J������e�M<Q�&ʆ���x�m@��U��q�bI>��*�e���Ư��@(��C��jے�f��+��i�IC�Ym�R��n��0	N�{&p p�{UW���q���a�Gi>&s0��V�2��V����`�v�p������g�����IXN�=^�iO-�tW�bF�xj�����x* ��S���^��8xH�
�
��e^E\���s�� $�_$&)��E�w�}ϼZė9V����Uּ�����5���g���M�<������c���ͽ���S�m����~��9��6S;\� #N��pL��
�!����k..�Va�K�7�uf1�zD7J�[DOJ�����js
\�e����s�Ӏ`b�pE�GZ �/�mT.A,$��9pV�}X7�|��g;��X��OI�T�}(�O��wNF��8E2� }���q+��';��vs����0_�L&J�)gV�%���әHLT��uN,/�HA[Wg�P��G3���uż��]�L�-^��܇���A�㴋/2�{����Ũxj���n\@����hB����`8
��'�"2PK"��\��s4^�v=�n����o\�������7�NĽ��i��V^�\�:�8�ЩՇ�U��R��;œB,�m�6��6�SӋ=&�
H��U�N��Cn6|�|���>��6�{B,^��jS�s41�*������>mJ��r,���Ҙ#)C��'��Mz���ڛ�BV(��X~���]f2$�(R"����Sn� .H���I�����3�� )ށ��]�������h����u񠿴-.tƐ�q*W��Kn'~m����$܅GCs;�*h؝M�S�郈�5�[H����~��wҢ�O�ǀJ� �IІ�$ur�Ui�T�ϲ.P}�F���sDR0U�\������]A��G��P!;��-UP'eW��-2���N ��w!jiC`'ˣE|%
��q�r:�ón��5��_N�v{����`D������W(܃\,�.� �c|̿�\Kg��0|0�H�iw��.�R�c.0	0_����F�]�r(��[��Ű
p>���xť��7=Y�x.ޏX�>E'ک>K��ُ�*�� U�(-�V�r�S�}Z��}��Hy�
��E�5�|ҭ�x}��vE�}����T�I�џ{�kE�x?�߃�Zu|�^�gC$��y0W"w̝��*���]����A�,J.�y��|Lg#��+���Mft���/�e�j�<C��Av����c(p/�A��:uM�)�Na7@_[�@�v�b7���p��{a��䝉���é���m� ����;	q����ÀfS��@����ȅ�ނ��*�����"�����&�	xAē�~�]��`�Jf��w�M0�@��+H�'�N9�?����с ͻ���w�p��{�n�pR��l� m�m�8R�m�z�)y��܋��g+�>̮K��vC��q���aX~Mp�Da���P��WL8R��@����U���f@*B/^��Eҵg59^��T���*�{�N����P|
x
s�9`�L��`90k#�����˵`>
W�����ߵ܀��Lr2x��L�ǔZW��H6�cXf�P`�����a�%�,�aMDTNFޮ�/��?T�F�AO�v	eRt��5T����C�w��ˮ�+�C�.���x��h^8�\����h���HT��S���f!�
�ot&��Kۂ�6}����v���n� ��2��]��.��p��-RK+���#��y^z�D,��T��i��ۑ����Cؒp��u��̿O��t�<�CRu���N�٥;�����V��6ư�N�?��nj|j)|qOqF�L�=��x�[�}w�TYW�����\y�,�3@�Z���J�)��:�.�]z"��L��$����TN1T<%(�[qW�6~�d�(��>p��'g��4����z0�F=��X���g�NH���0�;���dǋ`��V���i6DU�\p�g�/P�>"�]�p��������$�r(p��H'��.���e�Y����X>��)�:�R����ZS��a��Y�,pK�3��8�F�$�f0N��A��]H�p�P��c�1^��n���E� �V����qW֟O���縄S�"ڮki%b�t]�}�c�
���iu�Վ̥�Y��.$�����wz ��
c���{�e��r�wqk�� ����`�kT�<
#��_�!�s�uq|�������/��m�<`_���8��{H�እ�%�~��Iu*�J�� .���ςf䄻H*�q�#tg��,�ί�}��t�ܐ<d�VwC �TvF��)�s7؝����>(�giy�5�]�Z�"��Ylo������\�m�qN^4���pr'��M�?���ą�M�G0<F��x��X�,�0���8��XV�2�$����r��),W bJ?�DcLG��u)7\M�a��]��f���B��r�J��U	�� E	�D���������Tn#
؋RG�+OϺ�@�e�q,)��m#�d?���U)��ύ���0b}ve	�Z�j��S�R�:��j�\�r-}����kEDh�4BS��d�Yj��Z*�;�+l'�*�Ô��-�����=,K� ��qu�v��z�����S�$����9����x")q�P͗7�.C�_'9�PY#�N����-�R�[�`Ɩ��GGrR@�p�Iߟ(�
aV�`I��d]�����a�#U���x��y�%��IAK"oG>s�����	��T�y�h �;��l���+"U�W���_F���d��ːj�s!�L��to G�z��.H-;�=3��x���6�6r�0�נ}���2lG�����8���� �]N
(ͬ���y���e8��	��1|1�	Z��E� ݊�GN���;jE.��ҩ�E���wZ��^�pi��Sz�N ���f�"�+wCqK���iH�@8��d���ÄN �7~�R�cG��TO�<�:���ħ�y=2FG��
�:�k�T&��T�ۑ̺'���X�a�߁9 �8˦�N4�e5�W�k�ǆH�a��w� �ʳ����*�+�9:�G@JLs:�:��&ϥV�U�%�Τ�k9%�e�]ؖ��e"�NT�0">�$F�;Wc���|��˚����ꦋN��R��e�H�L���t�?J��Uk�C�}�����ǯE>��H?��a�TR�n����]6�q�mp��#]�]�GNľ�2S���J��S���w](�أm����h0���.޸z<0<��H�pW���ig��ȅN�Z��1|���#������,���i��/�7�^@-Oi΢���f
l|b��:S���P��aqK�-?��r�"�,�ZH�r��D�q_}�иp������Ԣz��.q��9�Q�H��=�|x�}s�(/�B��G�V�����HL���o�����~62�N��=�����HV�EU�V���@��KT85A_��r�S���%r�l�4��n�S5���<�L�hp[C�i�#��U��,��g��� v�Q�����KQ�SZ�V-](�@��+��$����9��6L��Fv���M�Ez����C<˳���х?q���@~�G�	�v�He���c0��j\@i|S����Ιag �A��C�R`N�]7�1�K���%�>J�AN���T����Pv����\��V�' Y}>����T��nJ�R)��9��w4�`'"I��B�*���B.!� ٭	�)�Ч�	`[�EU�+�ia������#��:h�"�,��p_y�hWw-~�}gy*[��.+�ѴG8=�;�s�S�����#��Qx��co�P` v2���{��Ȫ��S���L�B�4�<��R�9��y/5���	�x���V0�kHP�*���z},��(~%�N �gT<
�Ї"2獱|��:m^��_�
��W��}��T8e@�Y�<�	�!����[*N73'd��Aױ:���-c��S�E%1�W`O�k�����S�S<�f#w�.��R*Ɂ��u��sN�,w���Ba`�� 9�s�+�a���־M ��/�� ٴCʎ��E�p`C����7�7/<\�B.d������yGS���x�!�%|����(��^�RFi���F��Uz'#�!�lh��X	w�����಼��:_k�+�O8���bcwI��
ջ�.�@��!EJ�p~��?�Ns9��+����)b?����/"Yv�lQ�Q��I���c�=)r8�}��oy�Y,S�b�TZ*�s}��4�)s�prk!���&d���J��JK?޼	�8�}���C��vH�7�2�"��0��n��k&z֟D�*_��������Z�"��v9(������-M��S`�D��k��X=�zē��Dn*��I� �I�y�:6T�_�X�C2rw�}�f#��ǽXlYPO�f���պM���d��˧�"��C�lG�'�kK?b3#�2�*K�נp	�v)Yq�����(���Fܮ����BZ���Y�G$����usܽ�,�Ҩ#v�[Izg}�)^:�ĪP�
����ص����(Cȍ��1�C��ϻ�E�:��hr�4�v�z�H7N�+��xO�-���ű�a3`}��L�#V/�3������0�����%����N]t��w�4�e�z�a�Xv�pc��h��P.��� �8�������\|��[J��e>����F� 0��cݾ�;����(Hg?įB���g
k�ȺPXX���;�D����怙���0�8��J,[j��������G?q,�E�|s�c��)魎��zI+1�d�a�H��& �j	,�9J�/r<[��0��ccی�'�u]O~��J��j��&�i���'�p	J������%%�I4E��<�O�����B� d�g�d�i�8W�V�����-$����#HyW���x�{5k	)�-f
�������#񃪛���7�TZŗeי�NY����Gy_U�'���Ͷ��7kodT~ZZ�#���nD�"u�ZO�c��>E˄����SDY@��!@�C�aN�E`���l���͐�|.�����z��	���N�IwW�eR8�-O�u#T[�����_��%�Upl�G�Uo�u��H_+��2��}F!q	�tY!~S)������=��f���zO��w��O��<����*�`C�J�#�~@y��!�W��]���2�����׫�	L�m��	vu�88��)l����B`I��T��HuJ<�Xj��q�W!Y��M2F
�+�"4�;w�xJ�,��p"GE�mXg!ʮ�ƣ�����!�H�g=��!�R���?��dy$.cUϦ�!��e��4��-Ml�O8ɱ�U�qzv�T@v�����u�#����U��U��"�k诘�9h��ǁT��F�ް89IWyyi�|Z�CM[.
�TJ��� W�N�_hr
Q��i�*�j����	�pz���s���y�����,��f�&���s<���RzS8�������Z���n��.��W�C�S�X�`EI����~;�����x*�G���3�ZA�q�Yw7S��ʋ���a87Ę{Tr�^�U��>�y�B���-N�'���_-�J/�R��E�f�)JF�xJ35�Vg`(������?"���7�f�8XE�+ޘX�R���Q��O#܅[O@2�����ZfO"Ǿ��5�(�� جy���,&"�����+�\�!Ɯ*���x�Ր�u�
�]�YHaM�S��Z��i�(=I?���	S��b,��|Z)�d]�0�$�iQ�q�$��� ���4�I�E�vs,���m���䒞O�F�����( {4nu�� �&x3j�ކ�c�H--�x���"ć2�B-��e@����q{w"n;E�{zZ<yQ������ux]�*��4ȠݐՀc��</�����Y�
>�,��VQ���kǢ(ӏ�i�̷h����X'/&��:�0����KH[�ӈ��������N�F6ĝ�0���(
�'�V�|���YfJ5�Od��'�-�-v� .�z����Gx�_�T�W��O���3χ��y��T�W�*�*f�k�eQϪ�S�ρ�'�W(�w��NQb��xJ�������&�PRy<�w'�۲b�8��ǀiO�R�gZ)��(��n�-�׵y.��kz[<�+��O;K$!-^3�=����4ɬk���)��ʷo�%|�� ��E)���)M�� �̔HP|��q��U��@e����G����,Wם�-L@�=�P���$�m�|L0J������̥��rU`|E<�\����D㞔n���?J�lUE�Zz[<��v�W�����⩬78Qr�� ������'�6 �i�D�I|.���:E�z[<�Y<�-0��z(�����`p8����c�!LEEɒ���ˋ���<E�
T<�N�b�W��|���_�*s/_5�-�M��g���)ܷ}7#��EI��_��d��+�e��^'s��ɉ�*�_�g؁���r,W�䝃=�5P\Q<���iL���s��n%�� �����ًvEɊ-��,I�"q���8�7�47��%�p�:O�BH�	��F�n+� �u��(�����(^�M<�8LP��ƺ�g	P.�0+����{<��u��A���u�S���x��BQ���d��
̦ U���kբ�I�#{�;��D�>���K��xJɜU�MP`RN.���r�oq�99)�{چ|�e)J���b��RQ��޾�S��-O���b�v��G�09�v����w�eR6o�\�K�z�]�Ή(J7���)myz#��� ަ[XY2�!��k�9���u�S���8��� m0�(��o�if�1��������q��)�o̼�<�~�>wJ7�s�i����Ao��to����5�^ߓ͑��٧�[K�c��|�F����ѣD�k�:���y.�ҕ���|0��^<�j�fr��(�H�F���Ԭ�xm�_�׆��x*�'�w�ԥ���<Eqq� ��W�<E�Jz[<�y����LM��1v������m��_c��_c��!dqs�^���;���P�:u�)J��x�� �ɲk�m_�^�#u/E��z����Z�|���W���=)ye;�m�dpu��(]Ka���!"b��k�؍K�DAܕ�Wd=�|�, `�'Yo�|�iǶ�BQ��ů@��+J���xJ-�`�	>V~l�ߣ\��?�hv    IDAT���7}�{"�J�7��h�xZX���P�8���X%zZ<���O7��m�aX�%���PL�l�|X2��nN�y�>���i�g��=��u�t
���Y��_Q=-�<����]�Uώ�LP�D����w��R�!�k�;<�U<)��粻�7������x����9��9�A��ʸݰ�O2Ƭ����Y�7������"L�FРq%O,��Y��vNDQz��O��R��� �Xe�����֧�o��q]�DeN1f��M�ׂ��W`R����6��)tv^Jr00Ʊ���Kk(��?�S��Q�	�P�D
8�O�,�(��E˶�
1�8˥�������\C������M�r���+J;�v,��!=/�̣�4���@C~��I�7��QqPQ
�(а�? O��OбP�ӳN]wJ;Y�±|���sQ�����S�t�Pw\?�0���'�g�TW?�*����n��#�}y���4��и'%�Y~R_Q�����@���f� ��z���u+�~T��k��k��+�1���'�6o�D��� �Y�.;Ei��O���P��2�r�t�͍��sOZ�
� �� u�J�
Lw,�%���;�/Ȍ�܋ۥ8l��(��.����o!I��4A_��2���_#�F[������)�RV������e�p���R��4���vՀʼ9q���Sם�|���9E�%�K<�y�<��� �C\b$�F7	�rߺ�p�|�=��Na.�8�q�8`lj�>��@�H�uCMЁ�=)�b<��FU�.;Ei��O�a�
N'�=g��r���"/:b���Hsѐ��/�׫�y�5��]�,���O[#PQB��غǁ��<E�)�F<��QW���\S �n"�<Rv�E�)-���ra��0ȹ���&R_�Wjm�j�S��HJx�Eq��+JV�\v�E۱(JK�x�|�9$3�" On<��C8��:X�ˇ��^,?]D=샿�V�������Y��;%+!��I,pI��(=G_��T�]�!����c� 8����U�|X����j����3�Y�����6�%�O�R��=)��P��aoEn,Ei��O51|��	������trY�:P�nT�~�*�W�������	<[~Q�Q�{���|���ՃO<mOg��+�ϡ��(�(�ÍC�x]cEN��,Cṛ�.����K5�f��-A���0������'����)�9���ιH�LE����`,���nV64�ҟ����d>����+��$*�"�¼��u�4�70\[uY���,L��d�ݮ�X�#~dT�
H%W,E{ 7��P����) ـ���|	��C�˖�Eু��9':��/z�ʗ��]>�s�L���!�e��K���<E�I�R<AB@U[~v�r}��s	�P%��Z<��~S�֨�=��"�b}2H���T��`�����"�O nӁxֿ��|��-��π�v,����Q�������_�x,��V����N��Gnt��VW�LP��\�E��|C�p2�sL�ɭK<�H2Nֳr�A��N^�"��O�/�o�5o0̦�K@�p�5�v��2�]�d��'q��g}������HfT�ઑ	�$�ē/`/|Rm���U`��Ei��OP*N��B�s�x� ��j�`8)9�,��!Etq��
����$�O�a8�'��>ʰ��֘L��B�s�`�ہ��6�`�"�bm��j#.�4Bu8�h�	*�X=�i0��zē���,ǲ��2pXk%��km����,}-� a���	��?�u��糐�/�jujF<ů] E�{S�(`�V&�"� ����و'K�-�e^��Ӳ�>���c�H��.���d�^R<��N�xZ��`��%�8 Cq΂���\,<��i!-��wLg8>_W2xqO'��1EQD�Sm���P'�c��+�7��M<AE@U��A`� D,,��̛�.`'�)&����E2�|Y�g�e8�4�)�Fz��o:�E�.)�
T[�P\�q�N��z`� k�z�cxZ���`f�����#�.ZK<��j!�Y��f�����(JF��x�R urOT,2� �"�����bw� ֌���r,`=`+
�eG`B��Ǜ6'roŭb%��x)_pn��_~��X>1>i"�S��1���ugt����`(�
f�R^��rO��Ln��,veD�I� �2>�V`G��o �o�\��Q�T"徫�(�*�};�� C������ s��X� &P`��)�R��l�ðby���}�$>��x֍ ����8�#���JH�x�H�3E&���iM9
��-:�\�i�3Ս㧿��,N�d쭎TwE2�F����(-��DY<�c��@�c6k�t����+R�Zv]=���<��Cj@���q��
T������&��-���E?����G�����b�vp1�7��Q��߈vt�i����8�_���(Jϣ�YJ��p�a`�w�C��L����Ԩ�X8�\)�_ s1�*Z/�jqYք���K��I�%m��_x�����*� ����y�b �A�Vn@����,�v,����:C����� � w ˷oF=�71��D�nMcYDܬ�Y�(��vc|)6��>��F�HV�N����>m��#e,2�N����?�V��h�]<Զ�E�`��_EQ2D-O	�V9>A�݈
7*�`�����,��>�Ra|/�5����?�Z��$x���\Vg���P��{7c�'(���8����ZU�;�������IQ��'�0�x�St���0�S�zr�!�kF�#I�U'��6�8[7�u9R1��P��[��u{�5��&95�<im��,M+���$���1�����s"��i�B�K�En�q�� ���(Jƨ�i4��	�Y��G�&����r�M��A�p7R��'� Nor�������6�L��0x���`���G`�T*���á�P��N/"�E	��'�1|�)�-����U���]��{_a;.�"��]2�x��&��sݵI<E�MC��c�	��8��p[�z���)H��R�d�8P>Q�[�-�E�O50�N��6�÷��h�C�-��j9�ېY��~\c����4�]�xڊ�퀢}�%$`O{Z�z��A�$F=Wv$ji����vNDQ�O�`�vr�<��+r�,��K�S��:Ѣ|	��/�/�0�\��jp��ġZ�@��7���,N�f��L��x�dq�<1 �R���/��g�-P�N^�iɢ(J T<ՁyT��c���nFR�5@�����ĂC@�T8��k�v�?�n��c�oϺ@����)%�����`w3~�3���Ջ��������N�OPu�O^��W�8����3���G�����YDL@�X8E�><�Y�"�k}�Ƹ�肞�З��aYQ3	��r+����h��Z��s��P�~F�S����	���\�L�?� ��ȇ�V'/��?�#��&8��P�{�o@c5�|�i;2u�%�_V]��S�� =�� p�!G��e�c�>�{� ��\Q�=a6���K�d4�삿l/�i �!𗪬��y�O�]<�4	��Y�#p!��O�w��DRmC�%骋� �<�@�ʔ�1�̅�����1P�3�o����xj��)}9�b������5��_1��8��"P�����(�Հ:w��$o����u�s�0��`?��8}��HٍE+�����c����!T�O-`N����z�. ~����>��, ~�a�s��5]t ���q���Jj_OB*��F������1H���[C)�/"X��c���G���/G~���F۳d�]?���,@�T����Ax�_�\�����#��2��vL�����g��1j[.u,X��)�D�"��-�짚۶��^

&y��� ge1ТHH���v�+��1jy�o97#H}�-1lP*��t����,�V�E2+$c�J���_z�/�� ~l^��>���H6U���i��p
�9���e������(n��$�.JQچZ�a7��E�o)`�J�C��I����wb����^�������//,�l�Y�ri�s��Ok9���5��T'�UY��']lk�:0̻`�o�3)��$Ҍ{��s4��v'������������[ݸ�(���)0^���妀\�
�E�]1�e��?�d��N����_%��^ �����kj��w�:��� .�w�.D��\�?]�ND�d����������)���1$ؙ�����`&Cqs0;�ݹT�)X0��O[���n��^x�J��ؘ��qEQ�@^N�=OJD��|2��Ė˵��a
�>E�`��a=,Sh�0#Hܓ�`&���, ���B�E��Wi�#�|��-���z?����'�1QPr��E��P�7nWO;y�40 k�M�.��O���!���Iށ��S�4i�mZ<}鷘���TZQP��v��ժ�&R��e�2į1f�3�D�,L�2�ۈ ��e��"~K\�:L�g�#���d͎��e^��z�K����/W^�o�xFWY����E}�Ȝ!�b0�{W��j5��Z�)yב:@���~��4����4� ۋ��?k�&�w��:��e3T�9T<u��ca��)jZ\@���u�R}���Ozu\�GO*�jr ��K�6��%�1nPw��d:|��`|ـ!Y ���`�O[�"\_��).�FV����|��j3�<�+�C!�wC!h.��|��q��E	D�w�����7����_�b�ƚ��c=�1�5�#�+�)H���c���]wi�O�h�����[8Y0��q0���@m?p���]WO���X# A�� 5�A�~m�a4ǀ��rFftW����,�
N��v��S��=��<-�-������s�a�w��8ڳn!�������x������f�D�Q��D��	���N���wy�\⩙$@W���`��UT�;��)��u����wK�c�,u��
�\ٱn_�����(Jèx�B���F�S	3#�)1��񬟍�Hݏ�����5E$���2�X����gb��8i�,�|"Jr>�9j:h�A�*�'�]��L0_�m��}�rS�%�� �s,T�E	��'E	�bHoí<�_�^D*��2���-�?�p�Uv�,>��OyD���%�&Rf�4�xlO$���)Kє䛉�xr���{!�t�	E["B�t͏Q-o#.�$?B
�*��f�¸���=�"����+!E��yM���R�����U^r&�rɺj�pz��T��^�C��w	+� �.=p��kpp5��Qe�� ��5�c��L�o���ʤEiO�����)�L�!��=�v@�݅ڌx_������Mγ�D\������J�����#
�Su�{ٿ�hk���+O�s��?"��<��ի(JP������j@Mvu,_�dS-�X>)ȹ�=� G���:]|x�]$,"O�`�/�u�+Z���\	v��yX�T����U�&`�c��d�iXQ��Q�(��@���w7Lc.�w)�Im��G���f/��P	��ʓp�pYw��;���`\6Kb�j�}�O+#ֱ�����糞��(��n;Ei�ߪ��Q�QI8$�<$�A�c`��_�p��6��xs_��� �o��୛P�(Eœ���Ӂ�e����7�y����X���=��1�s���CH�c(��������Y����aT<)J��puv��n��4�0����EU���jm������`�����R�!zEHi�d;q���3���(��IQ��Z������lre`R6�rbO��W�6��]ܥ,��`O	7��F��E�y��)��(JQ�(a�<b)xɶ�mi�z�l����us#��H
���c����2�B
��^)W���$�P���� O�����H�Ȁ`��7+I��+��T��l�8P����5�!� 0���7���%$X\Q���IQ��7� ��Ь,��f����*O�XH����8��pJU&87��ˀ�ӳ�粻�Q�Ei*�%<��d��\v�g��[$�Yu��s?A�#_3�9��[
�����CQ�FQ�(��w�gi����1Hזس�Γ�&T��9�H~���~8'Р��6��
������(J��xR��q)p6�Zw��V1� V�[��S����ң0k>�X�s�i����O���A�6�fs�g��� ��V/s:i�d�����0c�]bn��q��F��+��T<)Jx&���n�kd��
C�oܫ���S�D�3�B^�Wɼ�Sc���o��T)U�ӨxR��l�E�J���\���������5�Ʒ&�9��u�)J�P�(aFb�2��F�c�9,.�n�����T(=��,/
0� ���K`��
0��(-��IQ��O����������m2���m�XVz�k1�1fs���W��AS���xR��P� ��ZH������r^��Y��h���)�����ܖ�X# �{V��NQr��'Ei?#T�Ԋ�n�k�Κ !ē��➊�U�� ��l,�X�,p{��)��*z�T��2LF,RO����L�psE4�x�`�� ��Ʈ�Yq�UU%K��(��U`��K'@s����Ӌ��e�lC� �� �׊�e;��(Y��IQ���������A�f�H��*�7Ei�1����T5fV%O�xR���"�+�Rz�b���U�Rå�R���o2��N�%ϨxR�|�0�TY�1�23c���"���"T�BJ(��ST<)J~�	��[Y4.�8��S���q��i�N���������P%+T<)J���0W�f,0;�F{� ���{U����O���>�#��tJ�R!��4!�S�z��d��'E�n�	\F*�j3�|�O��߾�(Y��IQ���`���߬Ԁ�Z$��D���e�(]�`�'�(J=����TX<�m�4��H����߶�(Y��IQr��eWڜ�-CV�SMRn�ų� ��5��(�Cœ���E:@�����6{��w`Y��췫(J4�IQrM�1���(�.17��q�ӕ�Z�f��f�mɣ(J�P˓����ͬ�=;fJz�z�*��Ŕ��vo�]J�ߔ�E�O��kR�z�`0�����U}OY09�f��yY��ϔ��:�aE�u�)Jw1���B����،��6��߂�;���@�?�m*�O��5��l���7\�>�m�(vG��ٽ �υ�3ެ�(�P�(�'*�X����
�#
W*U��L�~�X%!`R�(J�Q�(�G �d������w�rTǿg�mA�b�
��"jX�D|�	5QD(D����`4�D��Vk+�&-E� ��(PJ!��m{w���;;;{�mw�;s���vwfo眽͝����Q8?_g�--�謾���wS��
�R����v%�n���S`�
�� 3�'������c�K���$M���i�WP�L]���w��u�`Ykۂ�W�3���J��a7f����$����
������~	��/Tp�׀�Up]I2<I�� IX7�k�� ��;�7���7�n�<��S��+(�A�/Վ�I����;.fm5�$�赴(P�WWs�����R���ZH�_�'.�xT�0�lA��>e�/�i	|��2�k��4�O�Ļ����?+(l
�o��������({_����w�����Ӄ�o��(I�dx�j��"po5׏���I��|�W�%�L�{��U�׮:���~7�$�`�Ni�����a�G0z�����i�O��������S*�@=���R=���z
�_*����>-�M4%@]Wr.{��
��
~��.;Iu`x�jễ�n7ZU�O@�>$���^<����cY�O�^�WWv���+��ꊔ42�'������1�!ݨqĒn�,pԵ*[E��ʔ{-N�**|;,X���  �IDATޛշW�KR]��z{%mŨ̙��*�@纵@e-N���b�
7��w�Vx}Is��T+�k-�)��NІp1����?����G�Zz?�,<ekm���{���؂�1����޹�n��HI�f˓T�@�M��O ���aYp��o���m����n�b�N*�Oe\N78���%�NOR�u����j�/ Vt�xcxԤ��^Y}����j�a����%Ս�vR�\ː_�U�y��X=��&a��o��㛦 ΂�o��+��J��p����"/iB���)��nx9��o���R`Eyp�)�Qqu�gf-M���p�e�>��v�'�]�)?���$�MUL%U�a�LJ��+��C�3� ��_l|��'އI�<.�1��{��k �XX}����;Ζ�H��:I;Ö'���c0<% ��$pĘ*rp9�ZzXlu*.UeP(��*�޺�O�>]ae�^�p,�u0XN3� ��Igx�j+�lAB�y	��c������i��-�`4]U�1M��5��ŵ ΁����#(|�������}�N���TO�'��J�|�H����U%�f�����?0�'��8�"ʾg��p�I�`� ��C!�t�Y���:2<I����`��7@xϘ+��[�Cx�wz���>o�g��Bz-[ſ?4<�\\�;�70:�%�~2�Rz���L�Ǣ�������t���j��Đ7���N��O/<%�Ag.�i��8%�=<��� ������{+���;V�4�-NR��H��\k�ss��#m�y�8��7[K�lJ��p.po��o�g�nV���L�<I�0te�i������U��^��!(2���YY�S��)t�9��a1pph�o`n⃐|h��w�^����$i�OR#�����"��0�fo�����	�M�<۷ o��f��2� ������Ѥ!�8�ɺ��8�wzP�'��&�#i��o�K���C���X��5��BQ��5H΄����ₘR8�Ij�a�	�������>��N�鿝��6��V'�9OR�?��_��G��\\�g����{��H�bx��/8��λ�2�k h׵HWW�C�)��*5�c��F�����r����_�g�K!�H�{f3og�$���Ij�l�����yl�*`��թ16�����,8��aJMdx����Aq��p<�'`�تT{�%������|1@����S^j�� ���D:��B������L!�hp��lO�y%i��Lo���D��O�!��~Ns�j$�,��jw���|�{pI��춓���|�sw��g �	0�jM�7�+!,O���a�3uIi>0<I�J6�48e�GB�8ul՚\�A��<�f�̲ e��4_8�I�Wn��׾� �N�p%�eL�4[������;�� }��$�'�<I�V֍��:�����w@���s�X\���s/�M'i��%�Z�y,N�ǅ@<�!p��*��ǡ�U�_+P�6I��vҼv3�;�l�=� k�!�<6���Qx�8XW�5�g7�4���$�#k�������"��7CŪ�����!vS>8��M�O��\]8��S٭��..���f#�X�.���B
ϋ���$)ex�Tbض.e"��>�R������ĵ�E.W[{)?r���$͍�I�,�����������'!,�UU�Y���k�?B|��v�HI�9��I�0�'Is0���R���1���"��w$��Q��Y�	`L������a����S��$���$i'���ͳ�T: {
�#�u4$�@|p ��.@ܷs�oB��l^��č�<��!�fK-D��2v�I�Ó�]TR��TT�S��y^2e�k��̶�'���TƮ9I;��$i7g����T�SoT�)��-<�$�:Ó��v��=�27�R_I�9�'Icp}ɹQ���AI�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I�$I���?fa Q�    IEND�B`�               [remap]

importer="texture"
type="StreamTexture"
path="res://.import/NeuralNet.png-76e773d6d61e25f6c03eaa2664efe950.stex"
metadata={
"vram_texture": false
}

[deps]

source_file="res://icons/NeuralNet.png"
dest_files=[ "res://.import/NeuralNet.png-76e773d6d61e25f6c03eaa2664efe950.stex" ]

[params]

compress/mode=0
compress/lossy_quality=0.7
compress/hdr_mode=0
compress/bptc_ldr=0
compress/normal_map=0
flags/repeat=0
flags/filter=true
flags/mipmaps=false
flags/anisotropic=false
flags/srgb=2
process/fix_alpha_border=true
process/premult_alpha=false
process/HDR_as_SRGB=false
process/invert_color=false
stream=false
size_limit=0
detect_3d=true
svg/scale=1.0
           GDST�   �           41  PNG �PNG

   IHDR   �   �   �Iī    IDATx��w�U��?k�S�C��� )���I\��RQs��**=x���U
^��sDj� BI	9'��Y�?�Y{�Z��09�}�y�޳ה=�;k�������#G�9r�ȑ#G�9r�ȑ#G�9r�ȑ#G�9r�ȑ#G�9r�ȱ6C}�'0p0��6���,:r��NN�N[֥��4x.9��	��hY�˺j	�Y֝[��H"'|�8)����6�+�d.`�	�m�ߟW��刢�q����4�R��f�h���p!������7?L/�Y@���;����5~�B�%�V�6��`��޽Z�rkEN�����	�k��� �>{�j��{A? �y�]Q�����Z��f�	?�XO���k����@�D�i;�r�W�b�&9���� 	�"�s�p9�I�O}
��t����& �gsȵ���j���гA�@�OM�Ǡo�%w��=!�î��	_4�\i�\�#P�9�3DaP��u��m�rX���Ua��l.[s3pg�z'�灅�{�Pm����v����}�9L�"��&u��:4B�ޟCKd/� �
(���4�^��u@�������1y�^;��"N :�/��@�6��Ѡ��:~��3�
�;���V��= �-���WίpZ�_3���h��,���%y|")B�0�3xL��,hj�m�!'|U(����m`cw�΋�o@����������w5�t�9�!7iR1�$�����A��o�}�>h���s�� ���nPS�z� }���M��f���&;����aI,\���b=2�]�+��>�Ԇзcr}5�&9�+��/�d(Ms������5A�W���u���0���c�N��tx��BN���R0�Ex��aCd	������͘��_�=h������%�9�k��>0�������c��?�r�����z��ۨO�Z�y?� '|*T|�n�wO`�d	�{w��n��3PU"'|mH!|����/\�
��~῎�f����Q܆��@ZPR6g�@��gJ��=�ڋ��H�'�)>���z�9ֵ�L	.S�e�8r�׆�^V���u ��1ή� d�tY{��6�ޛ��fP[Ȯ����-?e&�l��J䄯o#�O���� ���m %,&�.��Z�����& ���%Ug�"�Ҥ"<� �������N��������8��_�#$�DQN%*��`�t!zN�J�	_f
-���N͌T�M��۝�~�.�_=�m����� �!r���2|�KY�n�������>N��y�/ ���!7�X]���C ˓à�����+r Sx��R��"Ȣ��,{��ًY�Y��L~<%�q�~P�mއ���d_��R&�]IFP$�=0�{��@�	�|^��>�]�5������S��a�օ�,X�	�"��}){I� �W�(X�/�r�|��J��N���Ȏ��k���'�=�<ЀE�>3�W:N�ğ�(/�����ws�t]ʶ����a��)�y���p*L�l�>��
x�w���_�ݔs�A?�4�O�~nR��i&�Gz|V1SgW�N����O�Wƛ�$��$��J�]]�o� i������,PG��-�������"c0	H����^�d{Z08���
� �O#J��� �S����z�Ǧ���-����~K�7��0�I�P�}�܎8�?O0Bl	|Z��W@I���݇�0�år<����62���d�c��B�|3pp�v$������\<��'{Ǫ���Y���"�����=�������MDn�(����4�}��3����A��D���S��ȴ�/����ў��THo>ť(��z���ȷib�k06n\X�>� 7 d����=�oA�?";��@�B�{G<�'�.�v��!�=� �4p�ᵙ�(���K���P<��jT��EUP9�&^��Y@���~��+�1�� ��"Q\K���S g�w0�=k�����Ժ���������v��Sۇ2�6��A3��Fs�-aE�?��,����~����r��	��U�y�Џ�[�9��o|?�2r�i�f�^�/m���`w#��A�/�x�Ȭj*K����߿��7���+1�] ��Aߗq� ����; /��'I��F�%��}��㯝�dЪ'�|�����mԦ�!���m�rSj�vFz� A-�n�y����ĆI�_������0E�"d�a�@��A�j_`�*~���on���/��҄���(��Af�,���EÄ�S���o���݉��l
�|���)v0*:J����bظ��U��m�����A��H��$b�[`Ďld��=����I��zڠ�I��'�-��E�\P�yW½���N���Cbk���s�Ȟ� EZ�gH�#=4��=Ne��x��l)�r��(fQ�r�'��C��C��������p:p27�RZㇱφ��Z��!:��#=-���+���al�A\�ۤ5hh��z2Q�# ���#�V����&l����v��q	M	���!�	��@�\�� \����쎰0��!ʿ_nO9��c�Ä߽�ޠj��'���?Q��A�^�Pd��wJ�e��N�m.��.�{�>��o�93e�a��OX[>�Y�_��$ه �>B�:�8�_�LG�h��Q$5wQZ�xo�:g���c�>z؇�@������^�~��Z�[x\E���ꥏ�����(���w@�Ǳ;}��P'l���������� �W��}�!��V(���:�h��a$R�q)�L|�9ڦ�e:PLG�#���8�`0����NvH��ׂ���R'p�fup��{:��֤)�9��4�痣M��R�g__�n����p	�n��:ќ�<����bc$��>�P�8*���(\�"�C^ h����▒����x`#���>�z�h�L���	��lȶ�z�����m�s���!��^ࡔvk
�"��O���Ã��
��\�^�[y��E��O�|��ŕX]�����~AB�;�g6���՚�|�Bքw�z�Gz�(�A�R�Ε���Ho�d�͵M��Ȕ��T.����M����kc4F�����j#ۼ�̤�)�����f�7)�kR�l�lŮM��A/�;��5�B۵Y��#�p�$�7G�p��N��1M����З/?������%�[���-x��,	o���-�!Y���K��/���#�Mv��<JrM�d�x#��}Y~�u�َږ����Hf�?��wk��F>x%[�k\꺊I63�n��xѺ^g�%C�>$�){]��0�D��D4l$�iM�&`i�)��!9�ԛE��7�h�KH����;�f>��z���"�l��BB
2�+� ����L���A�B�NJo^%��L�r+q\��Ԅ�bi��>�a���}��I3�&�I���ίp,�4��z����Q|ʶ݄y���9�{0�p��ݽH,M8ٲ.1Y���Ԅ1ѬY���.����q��"�-i벚Ϥ.4F�$Ŷ����м�h�Ȁ0Dv��&P��8=	�
# �!�r���<Sx�M�@���iB�BM����m?�4nY71z&e�Bx����Sk�����遲���>�3�FF�d=�4���?�ɘ���6��ܗ��� -�����NfI��7#&�7�=���@���֣�Qm�RM�j�����_���doY�շI��Dv=|`O����ؤ�>ځG�dO85æ�q(v���r*m���ӓ��e$��T�L�b$��D$u=�]$�pU�����D���I����]
,�X*��� ��$/R�t�!�������o[�f�Ï@���6��,B���S^�j��cڌ@�~1s�H*���>���e3Zi�ƎU��Q&{��� =}'��Ǥ�<�@B�M������{��dE)�]�d?��\�Ddl���Mu ����NB�}"�t��tc��z �u��]���Y[���(��_N���"j��؁�ՠB�����6�_o �2�6��[O�(NA�f$-����h!��Ux8�2�\�P~���.�)�Z�0�/�DXBfTA.�=��D�����~>����Ӡ8��:�^��-,<Xl'�I�Z��ԄHr�F�V&ZĽ/a��w@q.*�՛��x����7d_�Iߐo�j-ݟ�
��O+���ċ��3��wb�@vX��2"	�#�>>��9C��\��~D�sb�kL�	�^�ⷩ�	�C���
�J�Fk����=4����M�\O�� ;��^@R-�d�L�a3���B��e�b)���Ҍ�8�!f�-<a0�x�l�Q�.`&�@y�q��771�]�S��-#>&/�~#9��	����1���#Q�d�[3����Q�˺<A���=������hJ���d$+���5� x�>�N�șu(r4�@��~x�A,e�)p���8�W������_B&�@�'d �ޑ�w���U�cP[K���s���f�ͳ�Y�`�*����IC3ߧ��~����G�i��.*�1Q�e��qT��[6B��O�������RG�wt!�aLO�sl�-��B��T�2�w�����#d�6��|�[�^s�k��&ڝ���;�R�;bZK�=Ua��Cd=6���A�~�6c��<б����«c�� ��O"1% £�"��!����d���� �0�q�i�}�8�o4ټ�@m�E�6��xs�#f�B<���������a�I&F�!�ȟ��Z���"<f�x��](��l��J1�7S�i��j`r*�J�I���nv���0�1xx\���Aq<�~g��"d	-xyh�\)��ǃxo�2���4(\Kz��a-�j��v�����q�4O�j��s<"�������L�{ќ��!�L��h�G(�&��.r �ӀӲ��2���?q��ܡ/ ����⊪�w��=t���q;�̮>����/#�<�0of��*�5��f���c��	\l
�R�>j;���q��C�R��X���a2�96��H�/e5_B�D���X`�YŦ�
^1�)�����4�2��D��d7����&�8I��+��=�0�g�UM���5�6��F#�O��:Ƃ:6�X�~���@�f\|�tc���w�������������嵚�Q����8��E��v����۽�N��hbM$��@�A�&�z1�H�
*�
*��n��dF���ۆb��OG/��-wR�>*�..�Cl�͑i}��w���|	�������O���i(	�� r�G�vϵЧ��j�4cBYѵ��ѯ�dG�N
�B���"�f��G��q͝�M|�/<FPH�i{�p_5?���8O"3)^4��1�T���H|&�R4nnB������%���"m�xu��xz͸��($��Ҩ�����~v���(��]�F��)��Tǩ�&d0?��=�B����j�����-H>�H2���X~�`
<����`�P{A��~+L�^��}]�Wƃ��qS����7�.�9���PD�)�����ߝ�\Vz5=�,�䎸k�� g���
�ގ�jAf0 ��.�V�2�Ѳ��w{�����D+�w�ia����h�����p��~�ĭ^�m��
*gnE�C@x���8�M�����I��w�*6{8@<LzW"Gx�&{	(uA���c�i�����w �|�)�|]<�L�Cb,m��ܲ��2�_��}�_�l�#���o�^���nan�D7x�h�C��]p$�>v
R�P͠\d�D
�kj*���"���hF]�;;�@!h_ٿC0u��W�8͝�#�#�`Gez�tF��!e��*�jd WVS"��Ћ�,��&�
:�xC���6hrT�w��]���@�)z�!c�0ٛ@���ز�Q�7�F\��К�t���-�[!��>��݆� ^����N\C_�fU�">��_&I�A*�N�@&��%�R�FVΑ(ە?M�t�����y�6ˣ)�n���i�@ �TJ�Tٔ����ۚ��U�dwAz�qu@�qY	y}�=�� >�NmA90+eBj�E����VB7ʩ���^�+1Ԃi�B(��!�r5�7D��˴�����8W;��7����"�M�X���+�q��e��>���5@��9]�ѣ��C��=����p>jF�g�4�o��\����?ķ�����e)�SKd��#�5< %rtã���"� ���g.s��0/F���1^͇]�`�dC@l�dVVh�7��q��LE��"D��G��S ��f:����V��hl�?�+_�T�I�ƵLh�yR7�8���.��Ϋ�@�����7���;0_�5����2ݸBtC�Z3L�i5KZ��E�wˋ������C��� 5g� ��p?������x?�^oZ�Y�g��5������g"�R��)$#�<�"�VK���l��<��<(��#1>���ǘ$�L]*e��9W�7،��ˣY>�������걗Ó��ӨY���BK��h��<$�TS/�D"b>��=�HJ\�!)��_JÆ!�}9�h�g�I\I����AŶ ��C�n'򸯣���,vಗ{�^y$֠P�J	W.�����s;�_������e���dѓ�֍M"�.f�s5( q4�#�M���K��
g@aP@V�C��EF���a�Vu���zvR0�!�w�[6b4�3�`8a�n��l��Vl}���]PN��p�O��7�뉾&d'v�z{�
��='��VC�S���W���#�v�R�A�D���n.���<�_��q�s��Vf�jd�2-O�kf;[)��J?�;N����P#�����!��9������s���WibL@��^�&_D���A�?oAd<�wS�m]�aO�(djv��.��~��>�����H��d(���8�NF����]���P)�;/��+�y"5��SO��~��u`�����MACv�Cq]*��+<~�
e6š97��1�Z����$�@&�O����cW�|{Qb;�:� �ː�6t�����Py�@ j.2��i�'���tǑD���ʳ�g�nGa��S��(�kj�S�e�_������Ð��<��D��=z�8�&�r�Kw�Ӏy�Gu?��"-��(r����v�~u!��ܳ��-9y�� ū(�N!�CPH��P܏ђ�ў���n�M�F���oF�3!��A_���`(^�s�*�!���u%�9D޹0hi����m%�ϡ�F�̣*6c����f`�x�2.�(p�Ԫ�%f�#��h@z/3�keb~p���^Dq!b�5E;EEs)1b�'���k<�ݘ5� W!n��T��+�y��b���!�:!�2F S2���1 ޣDTT������V݃�X{	x	�L'�@�:��"C.J�4�,�gy��L��L�6�ׁlM�e��O�́���$�M��)�>�bًȟ
�ǣ!����}.�xO����[E]��0q�W��M�_���@�f��lͻQ�� ����S���U�	_�_�h��Y<�F��Iw��>4�!2'�.����'T�D�� ��&\,�Lr��DV��Td�ǣ8/�c��-�eO��L�l��&̃D\u+bY�"6���sX��{-���è�������x*tZ�,��w�����D��M�k�ɊSQ�Q����3�#aU"	���v߰I�m�&�Osh�d�^C����-�a#��ưn�`J��1��1�c�d�&�� �B�T�?��mx����?uc?�G�L�t	�0��Q<���o/����� ���B�-vM-cK
\Ne�/E�~j�pu��0�24_�s+9�6AsФ'�׵��q¯������w�w� �90[ЏW8�:�=��
�㤯+�n�����G\�f�~�ơ�)"��؇I�`�nv�L���>�X��H��?��  �IDAT���B������ �e��~%�G��;������&�����MH��$��/U�A����,� S���LBY�@�����3�Q\��	�#)����ĥR��ݼ˖h�L������Ri��QdV�!��c+�5��|ڰ�^vNF��h�x/v���
�XC9���G�#����P��=��$h؉�O��p�ݠ8^�>���E��a����M�Y�BNW鯮�_F��W�"Z��&����xl���\Ԋw��Od������_��� E�[maǇl<\�z���t�oS�0���E�9s$�K��I}�`Z��+����n�%�@��D$�0��>�����	J��v,	@aoL����|�v�yg\d�$�;%&�Ï�'h��U����A�E��6��K�x
���i�,�$�j�1��E4%ۍ���s4�n.L�b�������<�P[�e�OmF�����H�Ƿ��R���M *l:�hq(�U��8d|��r��"���ǽE���*B��Њ���@Z��	�=� hN��q�B��ϐ�Cfa�Y�b!����j4��J��`�۱��=Nd�H2w��8�Uh2?�X Lv�f��K�a�{x���B����FH�������tW GY��	W񍧙����ew�q���3%���}:��y2�F��_C��i�ǟ
�ښ������@q�����D� �f�j�'�rȜ�Ë�d�h�9,���.��u�Cz�{���&�	�m�:2���@�0���h�� "�A!��$���#��'|X|0���4��wc�ON���������ޢ�=���I�M��Jt� L��EziBD��DHo�j��h�!9
[v��7���f���!��>��[�{��g�4Z����J�b�X~���PU�K����Ó����0�����C���l�����
ǃ�a�{��g�B�B���d�aK�a�nWֵu<0�;���0a'�`�W���|>�bJ^^K�]���T5�ω5=Dp�w����I�8��]Ȉ�ad�3g��P$��q�$>&�֋��|��et�&;Ԝ� )Vm�?��&�#�0a����`ħy-�ݤk%��G��F�����H��ؾ�j��P~UŮ�����W������,'�j�jndb:.�Yp��O��f���%�Po	�\۰������44F�h��r�s@8.�M���y�uJ<��"�<P����l��L_C���td=h]�-h\!�b�(�y�v|ҭ��+���`���9����ޜ��ci\��$<�FE-�5�	(M��屯k�S���Kh��� �i=H�W�5��+����K,i�L����j��C�i��]��W1�����&ٺ��8C�6m�����#Y������K��\��B���ޕ�4-���U�8
G(r��3��x�;R�F���]�(�6i���1ϟz����@����s�f� �D2�@�?��;�r��Z�3�!|5�ak�B$�6�7����Fs�S��8	�߀��dM�&�[�z21-Q��p9�\�&�
�x8�	�}RBzC�R�	��w��3��a4	�`5�Ƃ��Α(^�]:�}�+�l���@
>0g������"e�����b��_pn+R���])��qT#bZ�è��M�>
	�[��|�G�&�\?���)�F�2:6�MSd��� �O�d�=�ձ:e��!�G"2}iZ25�53J�d=&4���#mwD"���d$|9Y#�
�ktj�a��+�)	q={�F%ҍ����l�O���xD0ȥTP%��[m��an���*4��aq$=�(ك~�S��1lʂQt���l_y1E��ۄ�����%c'�uH��6���QŘ�.DM�V$�5>$�ɿ��7�8�d��3(�I��6(�!�ex~z���P�����?9
T!����"O3��� C6�c��?0�c���ʚ�at�iE1���i�W.�u,OD�!�N�5��B�(��|��)�U+2X���T֖X	����������y0+Rl��!��/�]{c���*�Hb�ݔ��^�"Z��� �s�دc�!�v�2�h�����c��_wJ?pb�Q@<#/��:���0ԡ����'�)�����{g�Ȅ�*^+*�%=��^2C��!f9��c�,)x���D25�}�b��{�2�?�I(\	jbg��I(^��)go��� �!�M�0��}�!��C���kf�n<N���:��C��q%��\�	����8$AmMA��j��n%��y���@Ef�W��";b�. v�����<�G����(ݵ[�OT��H0=���_�,�!�y�vf0Y��頮I��àp������ܜq �>b�D�.]��O�9
ͭ��=мESb+�O�_޺���E���d���kR�����C��L�c�*{�-�@��h�����Cs$��	Ȯ��ڂ�k<��@��a��� 4}����P4{��
�Vi6 V�X�f� �](�
��j�VA/�WTƱȜ��|��Y�;bք�������74USqx5�;��s���r��Զr\�'�en 
?�ҋ���CP;��-�^)^g���^�j��aq��Y��?��c���b���Q�"�w��^����lC�@R�\���`�0qQt&�H�+��65�_'ݕ�ؒ���nP�׍E4v[�����p��|�����9<���ecSf�KDs��ݗB>��A�՞^+����b'�B�f"c�jGq#2������<��Wp'�d�{%n5�WȰ�|���72!;��t_D��{��4�@�`{P�n���Exs�ha��Zx�n��~��� �:��������7P/K�;�E�,�%Ė�&�4
�'g�~:.����2lU{m0��e �s���(�JO��˕�����>�"2!}����Ho��_,C��T�f���פ7���>Nd̛�����Y�^�����P	+���`Ӑ'�������z�5W!e���o���w/pk�`L��	�`]MP�����/��;ln����z�?.�##�@N��8)T��|,�^�������~��������,N�+F{"o�j�9|䄯������r��H�ha�Wʟ����C�y%��;i*t� ���.-��#'|�8���{�b��߉���.�1�䁾ݱQ34}��{g*�c��Y�d��:�j��>�?���/^�zF�h��o�l�p�	�
��Z�X�7�V֡�@�-�+R��A޻W�ܤIEBi�%VW`���|�P-���@I������i�))a � Q�t�[�rUA��)�{�T���bVx�����?tob�io�֌"B�F�m6<G������CN�T$�bSߴ�<0Wd|$j`��)�����OCN�ڰw�6�,���� �A���y\L�{<(�D�~��T�q樄��UCJC�G�����n�|�'TؿMU���d9ƽ]�-2��B�/o]�8��f�D,�hw(���`5�ڔryL3���t�}�q���XviN��/�#읊����Ov,�O����P|�WF��+|��!$�5�|g�����P.{�m�H"��|G��I��x��T/x3�ۨ�`Ņ�>L��~���D
�~�Bv���ؙ���*���2.���#�C�H9����1���!�������v�#���5���n�Ɲ��P���<q#L���P<4}�]'A_�2A�#���P����ª�0h[��j�)�a�@��K�O��L�yP8QH��?����z3OP ��ӑZ���*�� �0��۫B7����ŠƂjC$�+���;a��	��&r§#'|�86��xQF����*ޘ=�ӈ���Anl��?��SXÑ��5#Bv����g�����m�K!�J��'ʼ6"���#�/*7+�=�RY�����=��A�Fw=	A^�$�稄��5�qDj2��L���H�I�lN�зC��P�6�a3m#�_5~��܆�&�/�	�\ ��AjW�k����������Xx�Gi��Lj�䄯��V�Cj$x�����{e��/=?�~!"m}��+b���q�ך/�#'|C���qĕaï����}��+��H���� '|è0�ZF���	,��,|#�gS/��9��	�*�\j�����?�.k�*��_`#��7�M�,��#�-�)�|�#G�9r�ȑ#G�9r�ȑ#G�9r�ȑ#G�9r�ȑ#G�9rd��L�����    IEND�B`�[remap]

importer="texture"
type="StreamTexture"
path="res://.import/NeuralNet.svg-564a8818049a9c4ceea904a6bc94ae13.stex"
metadata={
"vram_texture": false
}

[deps]

source_file="res://icons/NeuralNet.svg"
dest_files=[ "res://.import/NeuralNet.svg-564a8818049a9c4ceea904a6bc94ae13.stex" ]

[params]

compress/mode=0
compress/lossy_quality=0.7
compress/hdr_mode=0
compress/bptc_ldr=0
compress/normal_map=0
flags/repeat=0
flags/filter=true
flags/mipmaps=false
flags/anisotropic=false
flags/srgb=2
process/fix_alpha_border=true
process/premult_alpha=false
process/HDR_as_SRGB=false
process/invert_color=false
stream=false
size_limit=0
detect_3d=true
svg/scale=1.0
           GDSC            F      ������ڶ   ����Ӷ��   �����϶�   ����������¶   ����Ӷ��   ���¶���   ���������������Ҷ���   �������������Ҷ�                                
                                 	   &   
   '      -      1      5      6      <      @      D      3YY;�  V�  YY0�  PQV�  �  PQYY0�  PQV�  W�  T�  �7  P�  QYY0�  PQV�  �  �  �  �  PQYY0�  PQV�  �  �  �  �  PQY`     GDSC   _      �   >     ������ڶ   ��������   ��������   �����¶�   �������Ӷ���   ����Ŷ��   ����������Ӷ   �����¶�   ����¶��   ����������������Ҷ��   ����������¶   ������Ӷ   ��������Ӷ��   ������ն   �����������������Ҷ�   ���������Ӷ�   ��������������Ӷ   ����Ӷ��   ����������������Ķ��   �������¶���   ���ӄ�   ��������   �����������Ӷ���   ζ��   ��Ķ   ���������Ŷ�   �������Ŷ���   ������������Ŷ��   ����   ��ض   ߶��   ���Ӷ���   �����Ŷ�   ���ƶ���   ��������Ҷ��   �������Ӷ���   ��������¶��   �����Ҷ�   ����������۶   �����Ӷ�   ������������Ӷ��   ������������ض��   �������Ӷ���   ��Ѷ   ����Ӷ��   ����������Ӷ   �������׶���   �����϶�   ����Ŷ��   ����������޶   ����Ŷ��   �����������¶���   ���¶���   �������¶���   ���������Ӷ�   ����Ķ��   �����������Ŷ���   ���Ӷ���   ϶��   ��ζ   �����������Ӷ���   ����������������Ӷ��   �������������ض�   ׶��   Զ��   ���Ӈ���   ���ӄ���   ������   ������   �����������������������ض���   �����������Ķ���   ��ζ   ��ض    �����������������������������Ҷ�    ����������������������������Ҷ��   �����������������Ҷ�   ���������¶�   ���������Ҷ�   ��������¶��   ����������������Ҷ��   ����������¶   ��������Ӷ��   �������Ŷ���   ���������¶�   �������¶���   �������Ӷ���   ���������Ӷ�   ��������Ӷ��   ������������Ӷ��   ����¶��   ��Ҷ   ����۶��   ��������   ��������   ��ж             
Catches input events
        draw                
Ends drawing line.
  �������?                   lineComparison     
   idle_frame         
Compares two line's magnitudes
   !   
Calculates the line's magnitude
                
Calculats the line's direction
      
Add vector to line on delay
     {�G�zt?      timeout    K   
Determines drawn image width based on 
farthest and closest vector point.
    @B    K   
Determines drawn image height based on
farthest and closest vector point.
    4   
Submits the data to the visualizer as 
input data.
   	   set_input      1   
Clears the current drawn image and output
data.
         clear_input    D   
Point Analysis class that determines lines based
on vector points.
      ?   m   
	Runs comparison based on comparison tolerance, creating 
	new line based on relative difference of slope.
	                
	Calculates point slope.
	    '   
	Relative difference between slopes.
	                    
                              !   	   "   
   #      $      +      9      @      D      H      L      `      d      h      i      r      v      w      }      �      �      �      �      �       �   "   �   #   �   $   �   %   �   &   �   '   �   (   �   )   �   *   �   +     ,     -     .   !  /   "  0   +  1   2  2   3  3   8  4   A  5   J  6   Q  7   X  8   [  9   \  :   t  ;   �  <   �  =   �  >   �  ?   �  @   �  A   �  B   �  C   �  D   �  E   �  F   �  G   �  H   �  I   �  J     K     L     M     N   %  O   0  P   1  Q   J  R   S  S   Z  T   a  U   b  V   c  X   d  Y   m  Z   }  [   �  \   �  ]   �  ^   �  `   �  a   �  b   �  c   �  d   �  f   �  g   �  h   �  i   �  j   �  l   �  m   �  n   �  o   �  p     q     r     s     v     w     x   #  y   (  z   5  {   B  |   K  }   X  ~   a     l  �   m  �   n  �   o  �   x  �   ~  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �     �     �     �     �     �     �   !  �   ,  �   4  �   5  �   6  �   7  �   ;  �   @  �   A  �   B  �   C  �   L  �   R  �   Z  �   b  �   g  �   h  �   w  �   }  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �     �     �     �     �     �   !  �   1  �   <  �   3YY;�  V�  Y;�  V�  Y;�  V�  Y;�  Y;�  V�  Y;�  YY�  Y0�  P�  QV�  &�  T�	  P�  Q�  �  V�  W�
  T�  �  �  �  �  �  �  PQ�  �  PQ�  &P�  �  QP�  T�  P�  Q�  QV�  �  PQ�  �  YY0�  P�  V�  QV�  �  �  YY0�  PQV�  W�  �  �  T�  PQYY0�  P�  QV�  �  �  YY�  Y0�  PQV�  ;�  W�  �  �  T�  PQ�  ;�  �  T�  PQ�  ;�  �  T�  P�  Q�  )�  �D  P�  T�  PQQV�  ;�   �  L�  M�  ;�!  V�  �  T�  PQ�  W�  �  T�"  P�!  Q�  �!  T�#  �  P�"  P�  R�  QR�"  P�  R�  QR�"  P�  R�  QQ�  �!  T�$  P�   L�  MQ�  �!  T�$  P�   L�  MQ�  T�  T�%  P�!  Q�  �  �  T�&  PR�	  Q�  �  T�'  P�  Q�  �  �  LM�  )�  �D  P�  QV�  &�  L�  M�  V�  �  T�%  P�  Q�  �  T�%  P�  Q�  (V�  �  �  T�%  P�(  P�  L�  ML�  MR�  L�  ML�  MQQ�  �  T�%  P�)  P�  L�  ML�  MR�  L�  ML�  MQQ�  �  AP�*  PQR�
  Q�  AP�*  PQR�
  Q�  ;�+  V�,  W�  �  T�-  PQT�.  PQ�  �  �+  T�/  PQ�  �  ;�0  �1  P�  Q�  ;�2  �3  P�  Q�  ;�4  �  P�0  L�  MR�2  L�  MR�0  L�  MR�2  L�  MQ�  �+  �+  T�5  P�4  Q�  �  �+  T�'  P�  R�  Q�  �  )�  �D  PT�  T�  PQQV�  T�  L�  MT�6  PQ�  T�  T�7  PQ�  �  W�  �  �  T�8  PQ�  W�  �  �  T�9  PQ�  �  �+  T�'  PW�  �  T�  T�  RW�  �  T�  T�:  R�  Q�  ;�;  �<  T�  PQ�  �;  T�=  P�+  Q�  W�
  T�  �;  YY�  Y0�>  P�?  R�@  QV�  ;�A  �(  P�?  L�  MR�?  L�  MQ�  ;�B  �(  P�@  L�  MR�@  L�  MQ�  .�A  �B  YY�  Y0�(  P�C  V�  R�D  V�  QV�  .�
  P�  P�C  T�  �D  T�  R�  Q�  P�C  T�:  �D  T�:  R�  QQYY�  Y0�)  P�C  V�  R�D  V�  QV�  .�	  P�D  T�:  �C  T�:  R�D  T�  �C  T�  QYY�  Y0�  PQV�  &�  V�  W�  �  �  T�$  P�E  PQQ�  AP�*  PQT�F  P�  QR�  Q�  �  PQYY�  Y0�1  P�  V�  QV�  ;�G  �  �  ;�H  �  �  )�  �D  P�  T�  PQQV�  &P�  L�  MT�  	�H  QV�  �H  �  L�  MT�  �  &P�  L�  MT�  �G  QV�  �G  �  L�  MT�  �  .L�H  R�G  �H  R�G  MYY�  Y0�3  P�  V�  QV�  ;�G  �  �  ;�H  �  �  )�  �D  P�  T�  PQQV�  &P�  L�  MT�:  	�H  QV�  �H  �  L�  MT�:  �  &P�  L�  MT�:  �G  QV�  �G  �  L�  MT�:  �  .L�H  R�G  �H  R�G  MYY0�I  PQV�  �  �  YY0�J  PQV�  �  YY�  Y0�K  PQV�  &�  T�  PQ�  �L  PQT�L  PQT�M  P�  QV�  �  T�'  P�  Q�  �L  PQT�L  PQT�N  P�  QYY�  Y0�O  PQV�  W�
  T�  �  �  &�L  PQT�M  P�  QV�  �L  PQT�P  PQYY�  Y1�  V�  ;�Q  �  �  �  �  �  0�  P�   V�  QV�  ;�R  LM�  ;�S  �   L�  M�  ;�T  �   L�  M�  ;�U  �  �  �  )�  �D  P�   T�  PQ�  QV�  &�  �  V�  �S  �   L�  M�  �T  �   L�  M�  �U  �V  P�T  R�   L�  �  MQ�  ,�  �  �T  �   L�  M�  ;�W  �V  P�T  R�   L�  �  MQ�  �  &�X  P�U  R�W  Q�Q  V�  �  �R  T�%  PL�S  R�   L�  �  MMQ�  �S  �   L�  M�  �U  �W  �  �  .�R  �  �  �  �  0�V  P�Y  R�Z  QV�  ;�[  P�Z  T�  �Y  T�  Q�  .�  P�Z  T�:  �Y  T�:  Q�[  &�[  �  (�  �  �  �  �  0�X  P�\  R�]  QV�  ;�^  �  P�\  �]  Q�  ;�[  �-  P�  P�\  QR�  P�]  QQ�  .�  &�[  �  (�^  �[  Y`   GDSC   {   3   �   �     ������ڶ   �����ض�   ������������Ӷ��   �������Ӷ���   �������������϶�   ������������϶��   �����������ض���   ����������������   ������ݶ   ���������������϶���   ���������׶�   ���������Ŷ�   �����Ӷ�   ����������ζ   ������������������Ӷ   �������Ѷ���   �����϶�   �������������Ҷ�   �����ƶ�   ��������϶��   �������Ҷ���   �����������Ӷ���   ��������������Ӷ   �����������������ݶ�   ���������Ӷ�   ������������������Ķ   �������Ӷ���   �������������Ŷ�   ��������Ķ��   ������������Ķ��   ����   ��������¶��   �����������Ķ���   �����������䶶��   ��������Ҷ��   ζ��   ���������ض�   �������Ӷ���   �������Ŷ���   ������������Ŷ��   ��������������¶   ���Ӷ���   �����������������ض�   �����������ض���   ����ζ��   ��������������Ŷ   ������������������ض   �����ζ�   ��������������Ķ   �������������������Ӷ���   �������������������Ķ���   ߶��   ���Ӷ���   ���������Ŷ�   ܶ��   �����¶�   ����������������¶��   ������������Ӷ��   ��������¶��   ���¶���   ����������Ķ   ����������¶   ���������ݶ�   ����¶��   �����¶�   ������޶   ض��   �������ⶶ��   ����¶��   ���������ض�   �����������ݶ���   ���������Ķ�   �����������Ķ���   �����ζ�   �������Ӷ���   ����ض��   ��Ҷ   �����Ҷ�   ��Ķ   ۶��   ���Ӷ���   ����������������������Ҷ   �������¶���   �����Ŷ�   �������������������������Ҷ�   ������������   ��������������Ӷ   ������������������������Ҷ��   ����������������Ҷ��   ����ƶ��   ��������   ���Ӷ���   �����������������������Ҷ���   �����������������Ҷ�   ��������   �����������Ŷ���   �������޶���   ���׶���   ������������������϶   ���Ӷ���   ���Ӷ���   ���ض���   ����󶶶   ���������Ӷ�   ��������   ����Ӷ��   �����������Ŷ���   ��������޶��   ����������Ŷ   ���򶶶�   ����Ӷ��   �������Ӷ���   ���������¶�   �������Ŷ���   ���������������������϶�   �������϶���   ������Ӷ    ���������������������������Ҷ���    ���������������������������Ҷ���   ��������������Ҷ   �����������ݶ���   ����Ҷ��   �����������Ҷ���      res://assets/Neuron.tscn      Interface/MouseCapture        Canvas/Output         Interface/InputSet        Interface/Train       Network_Display/Container         CPP_Network              Setup                
Visualizer set up
             creating dense network     
   idle_frame     7   
Creates the neuron layer based on passed
layer object
             _         
Visualizes the weights
   <   
Connects the respective neuron layers together
accordingly
      
Sets the input data value
       Input Data Set        
Clears the input data
       Input Data Not Set     t   
Begins running the network with the passed
input array and desired output array for the
inputted number of epochs.
      Current Prediction %: 
       
Run :    {�G�z�?      timeout       Train      .   
Creates encoded array for passed class index
     5   
Gets the index of the max value of the passed array
      3   
Updates the num_epochs value on text change event
    6   
Updates the learning rate value on text change event
     4   
Updates the class index value on text change event
      
Begins training on press
              Stop Training      )   
Toggles value for weight change display
          
Opens the save window on click
   
   SaveDialog     B   
Saves the current neural network weights to the passed
file path
        Error opening file     :   
Loads passed weights stored within the passed
file path.
        Invalid file path!        Interface/LearnRate              
   LoadDialog     -   
Loads the weights from a selected file path
      ;   
Saves the current network weights to the passed file path
    '   
Creates a new neural network on click
                                            (      1   	   :   
   D      E      J      O      T      Y      ^      e      l      m      s      �      �      �      �      �      �      �      �      �      �       �   !   �   "   �   #   �   $   �   %   �   &   �   '   �   (   �   )   �   *   �   +   �   ,   �   -   �   .   �   /   �   2   �   3   �   4   �   5   �   6     7     8     9     :   ,  ;   F  <   M  =   N  >   O  @   P  A   V  B   a  C   o  D   v  E   w  F   x  I   y  J   �  K   �  L   �  M   �  N   �  O   �  P   �  Q   �  R   �  S   �  T     U     V     X     Y     Z     [     \      ]   ,  ^   8  _   E  `   F  a   G  c   H  d   N  e   S  f   Y  g   Z  h   [  l   \  m   g  n   p  o   q  p   |  q   �  r   �  s   �  t   �  u   �  v   �  w   �  x   �  y   �  z   �  {   �  |   �  }   �  ~   �     �  �     �     �     �     �     �     �     �     �   #  �   $  �   %  �   &  �   1  �   9  �   F  �   M  �   T  �   W  �   X  �   Y  �   Z  �   c  �   l  �   y  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �     �     �     �   !  �   *  �   -  �   4  �   B  �   I  �   J  �   K  �   L  �   R  �   W  �   X  �   Y  �   Z  �   `  �   l  �   m  �   n  �   o  �   x  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �     �     �     �   )  �   *  �   1  �   =  �   >  �   K  �   R  �   [  �   d  �   e  �   f    g    p    u    v    w    x    �  	  �  
  �    �    �    �    �    �    �    �    3YY5;�  ?PQYY5;�  �  P�  QY5;�  �  P�  QY5;�  �  P�  QY5;�  �  P�  QY5;�  �  P�  QY5;�  �  P�  QSYY;�	  V�  Y;�
  V�  Y;�  V�  Y;�  V�  Y;�  V�  Y;�  V�  �  Y;�  V�  �  YY0�  PQV�  �  P�  QT�  P�  P�	  R�	  QQYY�
  Y0�  P�  V�  R�  V�  QV�  �	  �  �  �  �  �  T�  P�  P�
  P�  QQQ�  �  T�  P�  L�	  MQ�  �  &�  V�  �8  P�  Q�  �  T�  P�	  Q�  �  �  )�  �	  V�  �  P�  Q�  �  �  AP�  PQR�  QY�  �  �  PQYY�  Y0�  P�  V�  QV�  ;�  V�  �  T�  PQ�  �  T�  �   T�!  �  �  T�"  P�  Q�  �  )�#  �D  P�  QV�  ;�$  �  T�%  PQ�  �$  T�&  P�  T�'  P�  T�(  PQ�  R�#  QQ�  �$  T�)  �7  P�  T�(  PQ�  Q�  �7  P�  T�(  PQQ�  �  T�"  P�$  QYY�  Y0�  PQV�  ;�*  V�  �  T�+  PQ�  )�,  �D  P�  R�Q  P�	  QQV�  �-  P�*  R�,  QYY�  Y0�-  P�.  V�  R�/  V�  QV�  ;�0  V�  �.  L�/  MT�+  PQ�  ;�1  V�  �Q  P�0  QS�  ;�2  V�  �.  L�/  �  MT�+  PQ�  )�3  �D  P�1  QV�  ;�4  �0  L�3  M�  &P�/  �Q  P�.  Q�  QV�  �4  T�5  P�  T�'  P�/  R�3  QQ�  )�6  �D  P�Q  P�2  QQV�  ;�7  �  T�8  P�/  R�3  R�6  Q�  �4  T�9  P�2  L�6  MR�7  QYY�  Y0�:  P�#  QV�  �
  �#  �  �  T�;  �  �  ;�*  V�  �  T�+  PQ�  ;�<  �*  L�	  MT�+  PQ�  )�3  �D  P�Q  P�<  QQV�  �<  L�3  MT�5  P�
  L�3  MQYY�  Y0�=  PQV�  �
  LM�  �  T�;  �  YY�  Y0�>  P�?  R�@  R�A  QV�  )�B  �D  P�A  QV�  �  &P�4  P�@  Q�C  QV�  �@  �D  P�@  Q�  ;�E  �  T�F  P�?  R�@  Q�  �  T�;  �  �7  P�E  Q�  �7  P�B  �  Q�  �  ;�G  V�  �  T�+  PQL�  MT�+  PQ�  )�,  �D  P�Q  P�G  QQV�  �G  L�,  MT�5  P�E  L�,  MQ�  �  &�  V�  +�  AP�  PQT�H  P�  QR�  Q�  �  &P�I  P�E  Q�  QV�  �  T�J  �  T�K  �  (V�  �  T�J  �  T�L  Y�  &P�  QV�  �  PQ�  �  �  PQ�  �=  PQ�  �  �  �  �  T�;  �  YY�  Y0�D  P�  V�  QX�  V�  ;�@  V�  LM�  )�3  �D  P�	  L�  MQV�  �@  T�M  P�	  Q�  �@  L�  M�  �  .�@  YY�  Y0�I  P�N  V�  QV�  ;�O  �N  T�-  PQ�  )�3  �D  P�N  T�P  PQQV�  &P�N  L�3  M�O  QV�  .�3  �  .�  YY�  Y0�Q  P�R  QV�  ;�S  �  P�R  Q�  &P�4  P�S  Q�C  QV�  �  �S  YY�   Y0�T  P�R  QV�  �  �  P�R  Q�  &P�4  P�  Q�U  QV�  �  T�V  P�  QYY�!  Y0�W  P�R  QV�  ;�,  �  P�R  Q�  &P�4  P�,  Q�C  QV�  �  �,  YY�"  Y0�X  PQV�  &�  V�  �  �  �  '�
  T�P  PQ�	  �  �	  �  �	  �  �	  V�  �  �#  �  �  T�;  �$  �  �>  P�
  R�  R�  Q�  (V�  W�Y  T�Z  PQ�  AP�  PQT�H  P�  QR�  Q�  W�Y  T�[  PQYY�%  Y0�\  PQV�  �  �  YY�&  Y0�]  PQV�  �  P�'  QT�  P�  T�^  QYY�(  Y0�_  P�`  V�  QV�  ;�a  V�  �  T�b  PQ�  �  �  ;�c  �d  T�  PQ�  &�c  T�e  P�`  R�d  T�f  Q�	  V�  �8  P�)  Q�  .�  �  �  �c  T�g  P�h  T�8  P�a  QQ�  �c  T�i  PQYY�*  Y0�j  P�k  V�  QV�  ;�c  V�d  �d  T�  PQ�  &�c  T�l  P�k  QV�  �8  P�+  Q�  .Y�  �  &�c  T�e  P�k  R�d  T�m  Q�	  V�  �8  P�)  Q�  .�  �  �  ;�a  V�  NO�  �a  �h  T�n  P�c  T�o  PQQT�p  PQ�  ;�q  V�  �  T�r  P�a  Q�  ;�s  V�  �q  L�	  MY�  �  �q  L�  M�  �  P�,  QT�;  �7  P�  QY�  &�s  �-  �Q  P�s  Q�.  V�  �  P�s  R�#  Q�  �  P�/  QT�t  �  �  �  P�  QT�t  �  YY�0  Y0�u  P�`  V�  QV�  �j  P�`  QYY�1  Y0�v  P�`  V�  QV�  �_  P�`  QYY�2  Y0�w  PQV�  �  T�x  PQ�  )�y  �  T�+  PQV�  �  T�z  P�y  Q�  �  P�  QT�  P�  T�^  QY`               GDSC   &   
   /        ����ڶ��   ����Ŷ��   �����϶�   �������Ŷ���   ����Ŷ��   ���ӄ�   ���Ŷ���   ���¶���   ����������Ӷ   �����¶�   ������ڶ   ���ӄ�   ����Ӷ��   ����   ������ζ   ��������Ҷ��   ���������Ŷ�   �������Ŷ���   ���������Ӷ�   ���������¶�   ����޶��   ������Ҷ   ������������Ķ��   ��Ҷ   ���Ӷ���   ������������Ӷ��   �����¶�   ��Ŷ   ���Ӷ���   ����Ӷ��   �����Ӷ�   ��������Ҷ��   ��������¶��   ��������Ӷ��   �������ڶ���   �������������������۶���   ���������ض�   ζ��      
Sets the label bias
         %0.2f      /   
Creates the line for visual neuron connection
             
Updates the display bias
     >   
Updates the neuron's connection display based on
new weight.
            '   
Connects this neuron to passed target
            �?                         	                           
                .      /      0      1      <      E      L      Q      X      [      \      ]      ^      g      l      m      n   !   o   "   |   #   �   $   �   %   �   &   �   (   �   )   �   *   �   +   �   ,   �   -   �   .   �   /   �   0   �   1   �   2   �   3   �   4     5   3YY;�  V�  YY0�  PQV�  �  NOYYY0�  P�  V�  QV�  W�  �  T�  �7  P�  �  QYY�  Y0�  P�	  V�
  QX�  V�  ;�  �  T�  PQ�  �  T�  �  �  �  P�  Q�  �  L�	  M�  �  .�  YY�  Y0�  P�  V�  QV�  �  P�  QYY�  Y0�  P�	  V�
  R�  V�  QV�  �  L�	  MT�  �  P�  Q�  �  �  L�	  MT�  �  T�  &�  	�  (�  T�  YY�  Y0�  P�	  V�
  R�  V�  QV�  &�  T�  P�	  QV�  �  P�	  R�  Q�  .�  �  ;�  �  P�	  Q�  ;�  V�  �  P�  Q�  �  T�   P�!  �  Q�  �  T�   P�  T�"  P�	  T�#  PQT�$  PQ�	  T�!  QQ�  �  P�	  R�  QYY0�  P�%  QV�  .�	  P�	  �  P�%  QQ`  GDSC            �      ��������������Ķ   ������Ѷ   ����������¶   ����������������¶��   ����������������Ŷ��   ��������������������Ŷ��   �����¶�   ����¶��   ��������������������ض��   ������Ҷ   �����������ڶ���   �����������ڶ���   �������ض���   �嶶   �������������ն�   ��������������������ض��   ����׶��   �����������ڶ���   ζ��   �����������ڶ���   ϶��   �����Ҷ�          C   
Gives scroll container the ability to click and drag to
navigate.
                                                           	               $      *      0      4      A      G      Q      W      Z      ^      f      o      z      �      �      3YY;�  Y;�  Y;�  Y;�  LMY;�  LMYY�  Y0�  P�  QV�  &�  4�  V�  &�  T�	  V�  �  �  �  �  �  P�
  PQR�  PQQ�  �  �  T�  �  �  L�  T�  PQM�  �  L�  M�  (V�  �  �  '�  �  4�  V�  ;�  �  T�  �  �  �  P�  T�  �  T�  Q�  �  P�  T�  �  T�  Q�  �  T�  P�  T�  PQQ�  �  T�  P�  T�  Q`             GDSC   "      F   G     ������ڶ   ����ڶ��   ����������������������Ķ   �������Ӷ���   ����Ŷ��   �����϶�   �������ڶ���   ����������ڶ   ��������Ӷ��   �����������ض���   ��������¶��   �����������Ҷ���   ���������Ӷ�   �������Ҷ���   ���ڶ���   �������Ӷ���   ��������Ҷ��   �����������������Ҷ�   �������϶���   �����������϶���   ��������۶��   ������Ӷ   ���������¶�   �����ƶ�   ��������϶��   �����Ҷ�   ����Ӷ��   ����Ҷ��   �����������������Ҷ�   ����������Ѷ   �������������Ҷ�   ��������   ����������������Ҷ��   ���������������Ҷ���      res://assets/Dial.tscn     +   Hidden_Layers/ScrollContainer/HBoxContainer       Hidden_Layers/Minus    B   
Removes dial from hidden layer.
Disables minus when appropriate.
                  -   
Adds dials into the hidden layer container.
             8   
Creates a neural network based on current
dial values.
         /   
Gets the topology based on the current
dials.
    
   Input_Dial        Output_Dial    8   
Opens load window for importing a neural
network file.
   
   LoadDialog     #   
Decrease number of hidden layers.
    #   
Increase number of hidden layers.
                                            %      )   	   *   
   +      ,      2      ?      F      L      M      \      b      c      d      e      k      t      {      |      �      �      �       �   #   �   $   �   %   �   &   �   '   �   (   �   )   �   *   �   +   �   ,   �   -   �   .   �   /   �   0   �   1   �   2   �   3   �   6   �   7   �   8   �   9   �   :   �   ;   �   <     =     >     ?     B     C     D   #  E   ,  F   -  G   .  I   /  J   5  K   9  L   :  M   ;  O   <  P   B  Q   3YY5;�  ?PQY5;�  �  P�  QY5;�  �  P�  QYY0�  PQV�  �  PQYY�  Y0�  PQV�  ;�  �  T�	  PQT�
  PQ�  �  T�  P�  Q�  �  T�  PQ�  �  &P�Q  P�  T�	  PQQ�  QV�  �  T�  �  YY�  Y0�  PQV�  ;�  �  T�  PQ�  �  T�  P�  Q�  �  &P�  T�  QV�  �  T�  �  YY�  Y0�  PQV�  ;�  V�  �  PQ�  �  �  &P�Q  P�  Q	�	  QV�  .�  �  )�  �  V�  &�  
�  V�  .�  �  �  T�  �  �  �  PQT�  P�  R�  QYY�
  Y0�  PQX�  V�  ;�  V�  LM�  �  T�  P�  P�  QT�  Q�  )�  �  T�	  PQV�  �  T�  P�  T�  Q�  �  T�  P�  P�  QT�  Q�  .�  YY�  Y0�  PQV�  ;�  �  PQT�  P�  Q�  �  T�  P�  T�  QYY�  Y0�   PQV�  �  PQYY�  Y0�!  PQV�  �  PQ`     [remap]

path="res://scripts/Dial.gdc"
         [remap]

path="res://scripts/MouseCapture.gdc"
 [remap]

path="res://scripts/NetworkVisualizer.gdc"
            [remap]

path="res://scripts/Neuron.gdc"
       [remap]

path="res://scripts/ScrollContainer.gdc"
              [remap]

path="res://scripts/Setup.gdc"
        �PNG

   IHDR  O  O   e�0n   sBIT|d�   	pHYs  .#  .#x�?v   tEXtSoftware www.inkscape.org��<    IDATx���w�T����ϙ]����7�b�D���G��F1F��-F�5c��b��1A�	�ML��^���!6@aٝ���,?a�wwf�{��|߯�/�ޙ�gٙgNy�������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������%�tD��d��qsh���9ص �lg0��v��&0_A�K0����g�[`�B�\�~��I��HeQ�$"���������:D�@���1�3`��㋈�Dɓ�D����``O��	v���`>�(0�W~D�\)y�d;��C��` ����
r��`&C��!�(��HiS�$"E3�I��@פ{��������O�3"R��<�H;\��.��tO���	0��&�)J�D$O�,;���NI�&B����SaZSҝ��S�$"y�/p�7��c��,��Ѥ{""��IDZ1z}0W�=!���< ����'�I'%O" [��ݒ�MWC�8�6$�I%O"�¨^���I�$y�h�!Lx#鞈Hzd��ɘ#�<��i9; 2/c���F�DW�r�`�J�'�e�@�� �u�=�d)y�x#�s�m��I��W����V�=��(y�h���6H�'��|M����$Ck�D*օ{ O�ĩ@v=��j�}%"HɓHEs��'ݓ��F�tGD$~UIw@D�6�D`*P�tOJ\5pz%�ΈH|��I���9�}�~�Fأa�}IwDD��I�b���t�s�=)C߀��?�tGD�?%O"�m�i�F�=)@#�5B�9T������I�읿T?C�v��W���y�<�����iL���#ׄ\g�^l/���`v �����!��a�IwDD�Q�$Rֲ��0�-� ��(t��ϊ���v��^`6,>f���� ۘtGD�%O"em�x�#��"�>`2�̀l�_S�4���G���.3ƎI�}�IɓH�y d�N2�Z�n���a*T�4�z��>9�J�m�LɓHY�!ؗ��cn��c��$�.��� �w���h�[�m�O�i��x��T$S�,�lc��
��+��1�݊'��'aН��@���ΐ���_�"�<���QG��c���1�,�6��½ w�I|m�#`��kOD|S�$RV��C�l`Ә�Ԝ�̺����;T�S��CM�tLc�H4m'RVvCC���a�90�ڋГK��{a�B`o�/��MU��t��HL4�$R6.������Z
�x��vb0�P0� �yn�2���s<�#"1HSe^)J��O�� �oy$N ��� �K��@n��6D$&y)lU/��w�+W�{��HȘ�\�s�zl�B�vp�<�!"1�ȓHY���ĩrC�3q�<p��c#Gy�/"1�ȓHɻ�7�f���P�1�3Uך�G���k���p���H4�$R��(��.��+#q�G�
�TA.ɳE$y)i�����Bq;���5���F�jhx䩁e`7���=��4�$R�̱��a7�~TY�@���u]ੁ�9�Sl���'��6�c�Wc?~�/�=�_l�M�v"%�Zh���q�;���Je`ԣ`����?\V�'�����'���t�����Tv�`��`������ߔ<��&x��5w��W��.5�Ճ��OlM݉�*%O"%����7���C�f�>�o��WD<S�$R�����ޣ�-]�&0�O윧������I�4��'���O�R���S`%O"%H��DJΐ*�z>�fā�`�6�,#�^���Ԭ�\�qE�#�<���^;}��;1ˈ��!hX����"⑒'��cw��	��C�2b�<�eFSD|R�$Rz�x�����C�22�C0�x��CL�HɓH���a;�C�2d�SɓH�Q�$Rz�<%���d�<��%O"%������A͓�,S����ؚ������<��������{�]q�2u�B������qL�HɓHi��C̹b��עY�I�1E�%O"%�v�S�Sa�E���UD|Q�$RRL1߉>fY{;��J�DJ��'����}L�U�1˙��KɓH	Q�$RR|�<e�X� fa�13J�DJ��'���cz'�!(k^/Mۉ�%O"%�TG�6F���y��CL�DɓHI1��|L��*�D�)!J�DJ�U�8/SlJ�DJ��'���c��vz������*"�(y))^w��!f9��ziѾH)Q�$RR����!f9��!��'���I�44l�{�� ��!�U��)"�(yI�M�_���q���z/�K6f���6x8/OD|��H�m� N:�؏����V����,SK�:E�+���c��GJ�D�k#\������Q�==�,C��������*"%BɓH:m <l�µܿ|L�)yʏ�<}\}L�IɓH���K�Z.L�����H�v�a:"�Ugv;(���m
�}\�EɓH��L'x;��������]�>�C�2��`����pB�qE�%O"���8�-�6����O�,���{ZF��'�0��/G!"i��I$� č.���R���Ca�Z>��l����~��y��#��"⃒'��uv��)���G���\�~
t�>�;+�ax��E�%O"����S�Ӽ$O ��9��
^�������^��x8n������'�dl ��७�� �M>>��i�i%��G��`�Հ^{���"�Y�>0W����p'|��y�>����b���:C�\���<�sˋ��Mѷ'"Q�ȓ�_U�>�d�S������	��Y���1,�)v��x1^'���cv6�Ӟ�DEɓ��n>����m��	Xm���ypa_?�KŨ~`��|>�yg�=-I9%O"�[������I�K`���r7A�B�#�07�����C�M����O�"�
}c��c` ���6r���]��.�?�� ���o'�=M��m�H��`\ğ*�*���O-0ƼvOm4���?�)~
���#��������1@������%~� "��ȓ�?M�r�y���Wn��� �`~�=l�O�1��o���y�|����� "�P�$�On�]�0��W����{j�n��`��H���} ��*�@ǿ���e��B���")��Iď*�N�8�mV�l̕����Cvu��$�ՠ�~`{�]�~^+���N���cE�J�D��~��s;���E�5woyn�{�lZ�%P�u�����������;:�0���vR�$�k�cj�y�.�\�9{4<��ۊè��f:p����0-�r�Đ����?��n;��U���vō��������O4�癭���0��	��]�9��η��O�ø�	���x�U���'W"� �<�D�x
���
������>��� ,T��[�왩�
�:�[>�:r�O��Mg�8�|﶐{gD�%)����"�,�+��6�s�������s֧0��]"n#H'0C``7�g&�,��l�5��5`��Z�u-L�����g�깇k�q��H
h�I$�6�{����t1�v�0?��:�_Lm�Ө�a���ķd�c��4��.��}
��")�5O"�ep��y�����
�uc�g���`��ʠ	�!ث��ܰ{(�/䘞�@P5�\��O#陈E�v"�]�r/Ga	N���v�myo��\�&���r̶�9����O|c�-��^�+B�D��_��|�{���붸^��z2���Hq4�T�� ����@g,]hb��t��9�,��ϩa�y!����'��OA,.y
�sp������878�p�fk��	�U8o�2`*����'i}�tT��d�X�Cmy�[��6�)�������)y*2���`腥���Q`�e�b���0��2���j�ૈ�^��F�jB�O%��f_�4�Y�g|p�
6��7��j��[@�
�{���L�����W�4��f�a[0G�=�,����9�v�	���]��Xp�5E$&J�R�n����>X����d�2nWϓ,�1%SE�<��77�p^�����|���p�2���
�ʘ#�N#=���`f/Cn.��_�O������l���O���.�0�/m?�U��S��Zdl)RZ�L+�B��7���y��R���4��ǼFn���:���$�q`ǀ�Sq���[�S�sz �ޥQ烹��������_��|�L�5�.�ѳ^��"Q��`�5��x%�z7u�Nm�H;)yJ�������H��a�0��2�nL3O�M�J���p�Ń��[Ľ7���V]'����ߵ�W���҆�`���{��j���D$NJ�`�����J����0�Aט9|�tgR�V�!��y�8��>[�
jF���ہ{�]��T�8��������RD�"�1����Z�2�z,�)��	��s����e��:5U�F�85��'�z?����Ђ�����''N����uZ�A�m�H�<��nF'[��p�����ſ��8����;lm������V����{��B7=��u,<��J�w?,9�>F����/���H�4m癭e/\͖>I�ųeX&bc�Y�tgb�#n\��7�J,�n�%���x܂qφTA��`�FʤM�N�p
d=6��:�_t����y���f[�d��?q��,`��娤;����8�w���&����h�ՖiM0�T��x�+;W�ؓ<'N�F��7�s�"B�S���l~A5���O6��Z�j��qҝ��0e�a���c���a�^#�},���U�:?M`΄q#��b:�Đ�'����GJ�"d{�����J��v(��mm��� wۅ�_�+f�e����N�FЯvwd�Rt�o�O�al�����F���㎠��)y���e/�yW�X��_l?����G������B�-�%No��W�0Ȍ��N���=�&ׇ4��B�v0.l
ͧ&��G<=Ύ����H��tYܺ���N��PO���IC�98'���|:�~ب�%��Na�χ��M�-������U�$؏�@����ߍ�/"O��`{ӕ܇e���R">5�<�tG��;���^L�%��\�T���+؉�cB*��`O���$ݑf�\�pR�]�lyj'[K:����	L/��x}�?�8����)[�f�SP3 �����^����(q�������E$&yjۇͨ�@��R�����[��H��MŅUT
W�`I+1z��r+�L���C����{�y c�O�'!�ڀ�?���/"K#O������Q�T�*`R�Z�R�wTFX���'uj�4Oe���3W��=f7{H�'pk�����[���/[l_�&��
��Q��	Iw��N)�f�W�@�=��5 ����K�z�����s��K�K��w�2�lҝ����B�W�WD<��S��vlH��Q��q�?''݉V�!<qjy�okn�y���wj�������L�I�G�����6�4�J��.	���Db���<�tg	�^Q�ӄ�hS�}Iw��#q��þd��;�0� ���ۏ�#8J�9������=�t�F倧�L��@���� �p�،x�ԋT$%Om�X�%<�t_B,� �����B,�@7��~g�l�p���`��1�;�w�����v���l���p;�ԻԺ`�:� ~ P�`g����i*\�^�}��s�N׳�B�<������u*Ih^f`�̡/�i�4�}X�*��
��	����cG3'�(�8m�[��a��⒃B�����l�ҟ�:}r�����_*�0�t��0d���^R~Bp��M	.�)"Q��
۟�XnN���2��ӼL�vU���8�����_0ÿ��`�Bbo�]��	�����:�.L0W窥,3R����A�7�޸�}��q;��$�f.��Af.4�Ä�=t<�V�-��t���x��)��e{\]�N	u�=,�c�j��X;�,�@ܶ���h3�#\mf�Z��>t�3��+�Б�� a��㒵
w~W�����2�pS~���
��A�E�]�l_S�׸���ƭ�O�<��t��)�@�oc��E�b�i�n�7���sq���^ l�nf�ט��0W�iO���{�c�7r��1E��]m�>nq�]�b�H�P�� �Iğ8}�a8����q]�����S�P,�!��[o�c���.t͑'�8����I��%]A�@���������s_D*���l-�c�Q�MS����cR!���`f�����[���hܝ��~��V�_����pE4�}��"켻��STD��i����Dg�-cjr>0���Ϙ�+��ϺX� ���쁞_�>���5B��8���@��4�L�Z(�|T�
�=�koD*�F�VԙQĕ8fQ���8�:�K=�c��m���n�m��Z���'NO�>�ڛ8A�yv_/W\���C�⸈'J���Z�Ώ�������ҫl��:�#�A�s�|ےչ�C��^`���o�|�&)l��c��)Z)Y�\sl`ǘ�"R�<}���S�����3�̂
,����tr�	��c��}�,҈p��)�+���v:~��;IT> �r��8;"R)�<����n�pSϥ�ۉ��ËvÍ��ԉjFF�"��m��"hg �	y�,)�p�x����H�<9c��`���kuGWI2u��a_�\����J����l�;�7l��D���,��tAK�C���E*B�'O��~���6bofs��6Ԝ@���BG9��;w���Z��8��������oB�@;�E"U��p!>_�T�b�J������}.�f�e�v>wS��k�<�	�rO�ć�!��n_`P�})k�<5�;�cs�p�)n�{�0u<��2�M�N����y]���ܯ��r�2�L�a�Z�$>��rOeD"T����_�-�5���S�t��eD�fhU���T��*`*�m��O��pu�����#�yd">�-?��/"R��M���?�c�z^�?��[�3��S��߂N��8$��7���{E�jUa��Ж�rO/\���""��䉾�l�)��ԗ�κ|�z>�x\��;�=83�^������]�=�w�n�~�F�E*J�&O��:?����J�MU�-�3��nн���O������K+ٓ��-%O��T���os_D�RE&Ov[:�� D��S��^��ǳ�X�K9���}�{ tm�d`|��V�z��p������+���D�"�'�8 ��!�2,�<�-I��g1<�%�娐;=q;���}�O�j���Hn"��ˁ��1�E��Tf���V�S�e�q�j�V�`��J)�N�_��ok����Ӕ��e6�d����<��=%O�i"��43��'i����bS�
˻�<�+ܮ������	��<�s�"+
[�w
�1Ύ����K�l_�'|d���ט�!n��v������ˀ#q�0}[�4W�I$.��\5�"��'��);�/q����?��-OT� �򸳁����N��#[�h�H*1y
(�B2��Cܲ`�a	��F�{�Wc7�w�O�]C�%��	�E�=�w�$L$�ءA�61�E�lT^��{�y��R�{<������;�����O��0+�~�,��{����rRQɓu��<�kJ�t-�I�q(�ڢ�!�^�!��~�6��
� �~��(�<y�    IDATl�u(��l)PE%Ol��x��iJ�-��OE�wM�叁Cq���<i��$�g�
oWD�Ce%O����m�F��D�d�^u����~ߏ����lrOɵ$)�+�dD�)��<ɓ��c��ȏ��r�dq'��:S�5a������H��\v;�{1�E��UV�d=�w2̍<f��ѿV�V���������{��CZE�4�K�=�-)P�%O==�T�zއhw%v�@GW��w�X�w�rOSv�aǏ֎�#"�������À����˸i�ף�۳��Ӣ�[�m	��Q�$i�Pp�#pb�})i��<Y/�r��!f9:.�(�T�.~�mK�z�<�0)­!�O��>D�Pi�,>����×��Ql�	u���wz
�:Ύ���w�nIx�WiA�S�<&��[��PX7��y��uDSv�6	�T���"y���s��L=�,w�'�]���u�[�����!�TS�覐� ����RUi�Ӳ��U��i%XpM�"4����À���߆��N_㡮�H^!�Z�N����Ғ��G=2lz(��|�tan�?^lu�[�44*)�V��@�8;"R�*-y�|�M:X�@���.��OUQ���-��;I���_���q�B�H+�<����,g�nQ�\�[��n��Q�`���)y�4��3�������'�ag\�E��Y��:�WM��/�u[-��w�x�s�"źwhpK{�b�HI����z8c��a�e�nKg`�Hc�m�j�7�բl����N3���N$=�$|G�O��H����	ދ<�U�Fz����ASp���ݭ�rOSvR*����F$Le%O��<D���\E>0���lg E�.n]Uǐ{��$��o��� ���/"%���'�\Qw����C��c�=ꐯ�|�]��~�q�aSv�|��H5�����q���<uc�$�щ.�q�r����殜<�:�.��R���L��8�������v��/"%���'�4�<D]:
!�l-���T�i�������ٮ�cY�h�������C�gGDJEE%O��x�y������k�b�
�W�&��Ii�=p���h��������k���RP��ӓb�`�aq��	Q|o����M�1!O�L����a#��-��G���~wEJ]%&O~Fr��K�2`��5�ݨ�>�8�� ���?C�68�ȦÒ��E�I�n	�7�.���K�:�,�p��5e8o�3�����J�|���i�Bص�mú�Θ"ip;�u��>��jiVqɓy�e�Bo�����5W?�G왋wu�%F�B�ځ�Wߓ��%ORʾ ���S��T\���ׇ�([��i�&��킋�����|ȽA���pw�c����nؔݛ���'�&a�Ǖ�*��>Ã�"��Gz�]r��X8�S������� <�98��v��;i����g��W?��/"�U�ɓ��^��q�8��K����p���n|���K�n���z�W�g����є���I!ׇQ�NU��P�� �)�"oEg~�)vɰ���HO��:�Ǖ���V*�8n^�ɿ�xب�Eɓ������os_DR�r��w��u�a�݆-<�.�_��������y̠���# �Ra�f���|�R�5pW�=-���'S���=�_���!�9�mk9����9SW�s>� ��%/	y�M����z'�	>�q`��"�:�<5���C�mOfs����d�����2����
�7y7JuQ��۪>�)�yX�B����9����WCM��Ut�X;�,aќw$�� S�m�+U�ft�3����H�����	?��z�k܈c�(�y�5�On���/��fXXg���.@W�݁�+��+0���`���̃O߂[�j}I4��	�� ��)l���`c����X:c茥+�C�E�WX�"�b`�wY�<nga����<�ZF�w�d>M4����gvU���T��E��ɬ:����B'�&��n�SЁ�K��Yu7�]��z�Z�`�f`v������̊�ʿxx��23a��mE�����N |]TyFw�L�1�����Ŀs�yX�`x��̴�J���#y��>��k�馲b�q���6b8��q_��;�����G�����%D+~�� 7���ՠݕ��.��`���'�/��?� 3xƖ��]���?�+	R���:U�a �݀�).�/�"�߸�2�3�	^�&	���	���\W{n�?d���'ppI���%���ffS�6&�4�)�xت���~r��@_\B�`Xi���Z�q��I�7+x��pd$ݙ�	nd7h-���K�vǳ!԰c
�$�% ,`�J�)���Xb��	��t�O��!C�����#���Ă��	��14v����.���M��B_�5p�s�����v҅M�6���|�������0��a�	��H��٠CpeU	�>�r)]p:��87R�N��i�l`2����ߤ;S��<5����CS�R���U��Ж7�k�&�Λ��Φ�5 �*��t��V��7��;O?�n�
�_�S�.�Ug���Y;�޴�|�Z��	�����@�GWᾜ���%�'�a4>7���������Ԧ���f���3ĳ�w��Ju�݁�i`*ndƷ����6w/���o�l��?�w7���_Nx�2��	g g���,{4���T�4��<>��Ä�i��X,#qS�.�Kn�L<�P<P�۟��<C<��,�X�����vkۗ�d�����>L4����qg|�+��m<�#�+钏�����5��B�4\)�r��Mc�/��L
] L�>���/�%ç���B���I��'�(n	]�)Q�Ԃ��$,��֠aM�b�R�(U�k8��������O��'�E�;||�.�6���@�{��R���7y>����|�*����)hwӊz���1w�dz�`{�f,�K�+1X�a"K�P5��Q�Ԃ����n���Wҍ����}�Z�n �c~l�3��V�
Uv���o�Af^h�L���F♞M�Ǡ�t�L;��5w�Q���c�K~\m�K��N����#����IK�S ۗ��� �>oa�%=����vhq�l�ŒO��{M=C
|�?qgo�4���3�#�y<�b಼{VR�u����J5�U�&��\	.�l⿏)0���p�?��;m8��1���tWe���­|�tW�I��m�����hz,S���?g���B���e{۟?by�d�7�Ԯ]|��s$��Q��4a��u�1���K6+9q�{!4<��kXI� �p���b��;����:��' �AT�
�90鮔�<��{P�|f��V�ρi�S�>�۰&9��*u'�������ݎ��ހ[��o]����t�b�Gs�22�P0w���de_�=�ߛtGV��]`KH�H��ٔ� �$ڏt�n���t6)����V�Z6��]�xf~�`�M<J�Y��E�����mq�v���K~��p����v>�#naxЈ�'Va��Ո
�Ldk`�`�F�	����`��
>��+n���;+8��1����r�Y�qL�ä�R��F�[��
��'�j�%u����ax��j҉ElD�эF�a��G�^Xz}��HGR����������#g��2�k��G,.ٜQp�R����^�$ݓ�4WV�N����-�s_����Z�������ȫIw�T�/Zl-�S�1�e)�7XZd���Ȁ���<����V���>^�gf{@�P۷��؃a���;��mq��Z��Fd߉�/��\ɷ�PJ�>�p(7�]R�����s7����<pX��/ߕ��S�������
��2c6u��8��w�̂��E,w��&j)C<�59Y��oQ��^kb���H�#�H�S�L=7b�:�~��71|��5\+x��E�](���|j8�@����쓸����@Փpa9�Q��#YN&�á�a5���i�_i����	Iw��(y*�l�~�t7��{T�����d�/	�N��,X�n�����qS"���?��x�r����͒�H̦\�i=�^[BK�Ja�����<�\B(y*�K=?�pI�})�14�����{�۹��5p	u	�����)�d�$l ��!�I[Jx9���5�ͭ�D�ϕ�*,S9�"Oh%O2`MYg�HJ�a��:�����)�$'l�S���}�3򈛠����7�NK��Vn�}�K���$�����a�ù�O�Ė2��i�tGJA	|[N/۟��N�����t�慼V��zzA`n���i��q�����,i~Nؔa��u�� �_�=	� ��5�o��v������c�@�'�-q?�ޤ��nJt��T*�H��z��p~\i�h|��0���	��c��5d����3��q{���A����L䭤;�fJ��d�����S�%��r�pnL���lp���m�<�;��CDOō>��ܗp��SVy|���s��E���<
̀e3�ʏ����z/�H�@Z�P��U��À�\���nq�a5ކ��/ґh|��12� ã�H��M7�.Xaك{aفt`�
߰�cI�I+%O�۲M�M�kj*�WX~jf��ۼ���8��[yޡ�_C�m��B-�γ�1}0n�,���π{�L��k�\TM' ?���N�,��al�ߣrR�	lp��/*� �/ ��r�֥/뷿�E[
<Lfe��/H�X�j�&�P;zi#���u�ZIS�;�*�p��HǷ��x�Mo���I��l���~p�}�;�Q�W	~3���r�'c6�"ɝU�,�	��~��1��lXX�0�p����?0;�X_����"�Ҁ�/C����������7_;x`�G���<D~�C|��5ngb�qOgG�8��$5e�!7�D�N9%Ok�ƻ��~�2��Lcm����	�.VWA��;q#��[��;�6��|:�ǰ�s���0�=�@�-��C2��OA���&��Xx�ൟ;Ϯ��mp	��jE��#��u�Đ���x�,�ħ�Nek����__����1��z!����!:���$}�xr^"�n��sJ��M����r}-�Ƚ���=Dx���I��x�K�?q� ��G:'�q��q���b?uWX�˘�L�'�{�(u���}��Z���B��%#��H{ڶ�.�}����'�[x��9�*��ؗn����uX���'�l-�7S9G`|�!K_n4�R�8��M���>��9A6�mqmu��a���i%�G�B�K��-�	����FU)ރ�B�%�Y�7� v;�ZL�%eO�р������CܑG�0:Pŋ�����,ù%t�:�
\M���,g3��ck�h��#S���3�K+�*�iӄ���m�.%�^,3���묚8�ۢ|���n"�E��n ���~
��`�/ҝ8\���2�^�"j5`n����� ��W�߇��k"���%N�.a=�Ky�`�ȝX�'�Q(�e����W�<yf g깕�l�K��i�2`
UԚ:~b^<�!Iao.��
[��z���B��~G���O��C��2��0ᡘڋ��3�h�����0�1���u���@�>�L6..�Cy�|�aOn&K���O�C�g�iq��9�������)&���zn�[�ʷU�1��)��z��WS���*nʠ%�����O��\���X`VȽ��{\�
�݀	�e�`\\#8?^?�%�t���8�����v˵׷�S#����[�7�|�G�h�f�X���fY~�i��zL%O13/���3�~�b8�_H]Q�P�������	�(Tx*�^˩��Cg��y�s�0��5�:�b%KI,ӄ�R����E6�	Ɲ�r�m���:E����W��U�����TdL�^r{ G+3ib �b�D�I������D�pY���)1fM��?�:��'b�N{+����p� SOSϕ��O��T�]�6�UO~���~r��8gMq�i��`N�9s�-�dc/"�g!;F��H���e�����D{��#���|Á�ʗ1���L'�^����?���m�%O)`^�S�dSǾ��� ��%.qk��ME��ڬk�9�ԗ��vx�4 �[n��䩵�N-��roG��:�3���� \c������6��.�9�s>�W|~����tvƄ� G�:sL*JD���������.�m�� �,d��v�^v��cG9��n��Ke��������)hZa/�z�-h���[�^�&HO���AERs�$-l}T��p�'C�f�Aû��-�-n��܍�8�c_B�f�Z��V�����B�ù���6 �i`0�M�Ki|�s(�'�NG_b"z��z>_\)���y	w��5 v��/�SEo�96���+�5p�]pI�"��pt.��_,��0�%�3o$Y;Kq���K��A6Q�1+p��d�/'����o}��:a�⒮���L�&N�!�w���i��9�u?����;,=���G�1���=��MNe;�`��[����O� &�7Nc$��=�2*;y�ȓT��q��-M����5��<�v�9	W�"�o�S�x������!����������e�>���!��2�b=�y�-���~
�6�����M5nM��D���'������(X�&�wO���0��R�ȶZT�;#�5OR	��=}WLr�����wj�\`^Ƚ���B+��/�M� �	~x�C�8�*+q��0���x%�t;�Ol��,��K��Og>�M��a���'p	㉸���1v�)y�J�$���Z9���N�YL���'����p�rG��7���ɞ�����
�=�/~�]>���ۡ�b{�7໸R��.k�EkUu�b���>��L�#��g9�s���ROɓT�E���r��t�`\Ƚ��W�C���<> y�6p@�}�;���5]��Ä�l|g�E�u`g���l�vE�f��ɭ�_`��M<�[��C7��0O�SOɓT����]B�?C4E�.�m��?0
7=x��G�<e�m�9E����:߮\��)x,;�S�,~����]�����̤6ܖ?�9�������w�)y�J�<�-
.f�ӊqur����oS@��m��1V�����G C��r��d`7�s�]or�ٸ�5��▞���iw�e?Fx+a�jJ��R�"�z{�w1�Zz�EȽ|?8Z$O��ǭ�� �q%�$����F��)v�^3�sA���Ӕ��6n.���j�&�s����!n�)y�J1���o--!|���n�/��-�'���g���b��	���؀��C\_Vo����s�F��NŎ@/G-��+qK��,�p���a�3�5%ORI¦�Zz������M�;+WI��eJ�xc��~��R:��b�]k�4�؇�΄� 0^����&����-�z�.ਕ����(y�J�o���B�/��c�zN>>t��0���㖃%��UJ��p���S�V;��>|w��;�1� ,G��3LAGM�kC���C�TS�$�$��� x"ysi���``��`�­����U�F+�mk����>u~�;ñ�
���@
��<���<�%r9�1�K\[y�OJ�����{Co�"�Y߲9.	�@t97'OO�9?o�e����q^\�����/�1��5P��~��N@/}�B����q��G+zJC�R�$���m��
�����������E��#��Lx�C�22�9���f|�<��
nSDX����j�|%N��O�"��i4a=L�Y/�I���I*M[Sw�L٭��&~'Х�8�����Qյ*w^'�+�����ᒣḪ�-���rkx�ç��l/��I&�5� [���C��R�$���䩽�{���b-y
;¥>�Pˑ�����3)=���:��Z���(��(��k�a���(������    IDAT��v�(J����^�`1n��=�.ŭ��e��,�Ѝ3��Y����iȖ۷�>����f[�rw�Dޢ�Q,y�����E�T֎;%ORi,���G�S�/�7�M�V���)��6��%џg� ��~iY�>�Y�Ak�����T�>n*�:��
ףm3���S��Jo�*%O"�lu\�� Q���m�M;ʺI��2�x�����yx�r[E35j���׀a�A5>FO�>A@��"Z�M���I*�`��l�;�=*O��@�m�n��r����D�FZ�|$�~���>pO�,D���S~��`���O� R����E��Z��*�v����+_p	��0V>j%_�@�[��yroD�����?�9_O'���w�$Jv�$���w���#�i#��zJ��ҴU	w�'O˽	���m�>
�����t��E��i�����d0nT��E�b�ѨS!�{ؗXQɓ����t�6�����ŝ�%p0?��6���c
eF���x��*B_�$O��~�����;l�<���=_��.��I�p9�1p"��u��m?�PV<Q�Tɓ����=�^6m��I*I>kJVv�ݑ���Z�G�Zk*�aQ�	+� ������3�J>�u���4�p�;QZ=�x���I*I�'�1u��7�x�تY��'�b�3��C���KH�{N���p!�Q*#���KEzi��T�u��@�A�U������ n�ܩ��pD]O1SQ��ų^�M&��Y��)�1EGi(�#|�%Y����z�ʚ:U�$�bO��_>7*��O��8
���Q�F�s��)��}��^��.Ϩ�A��t���b�H_ 
������ug���JQH�5�~�:҆N��a\1�Q������������}�\��X���V���G7��p!���^��wx%y�J�<}N���`�!�q�8	7-�ʷB/kc��h2��������:�8�9��"�X{/�,�w���ńX��cbcWNz�������QQ��Ɗ���^	b������q� ˽�S�w���z���;��㖙g�9�y��HNY��xW��e �J=�a�;��*���W���'�y�J��+���!�Ǳi|�R\��I�=%�N�r���zΆ�. �M�1�)�Ǟ,K� z�w8"J=9Sw�r�߼��C)y�Jpp�����k���*�|>F�S�y/���<������ڋ,F�~���m�4���G�	��$mY�JɓT��%���GC�mL���z#p�Ɵ��kʣ�����ݺP�q�O�|3��G��A�1�`�pI��_��Gs�<�,Wߋ<b&�"��CɓT���i�!�P�B�>u�5~
�4w9�~a!3_��S;�؛���lk���]HG���7��	�{Z��1kz�C1���� ��-�K1#�ELɓ��m��C�Mh���C��<�f� �����Dw�}:~>�+yʎ��g\�� v'��hE���x�˲x.�H*W�{_�a�1���')waw
Z��Z�=���p����q���gcF�?Q�qG�yH�L).w4��[�Hvu�7`�߆����q�^"����;�-_*��')wa/o�|�uX��W�<[�����|� ��!�PwO�S�;��s�2q�Ĉ�x\��K�o����p%� \Kl?~�-r9Hp��ȥ�;\0%OR��� �Y�ߧsC�~��������kr52:~Z>� ?q�E���'�\��)
� G�*���/�Ӹ<�\̋�����p=9�0�6�gyS�$�n� Ȋ	�^y^�Kw��S������t���'�Yy� �gus������R�+�G���l\��qP�S/I�֜����� U<�%nS�$�,lO�B�Ǣ�4pp�fZ|���ϴ�<�������-=�����ϱ(���0�����<�l8�S���N��!��ZYe
@ɓ����N� ��<�<�y�~w
)�=J����\31��m�z���\�)p9&O�O�M������T�=a[�,�{�\qw�@ɓ���d@��7�7.�WŲM����kO�p1�Yᔝ}����W�!n	|$�����n�������-�|��E'2��C��u!ݱ\�%������I�U?�Gȵ�?�f��!�/�&�B�)�+(,ᙾ�_�}�{�� u���%ɀ���x���;	G��÷��+��v��g��act�j�B����&���)y�r�d�����,l��O����J�h[�����~�)v��?��Ols�������hՍ��Nw��A�B�<�-=g�!��<E���> dMɓ�����\$,y�X���|�$��O�m�'�W��w̍p��%$���7�&Ȍ�;N��?7����`	S"��2�r���Rō��f�2�K���I�Q5����[���d�3�K�����*�g�퉖����-s�T]�'v�h�-��������JYܡ���}ގb2�73�o��y�kB�÷ta���EOɓ���	������pǃ��lr�?Y��7�6��O~A]�n� �|�K~�n�5\3�¼B#��ۑ�CO�y�]��d,wxa,׳�c����I�Qح���|m����'|	q�$�)��PH?�����6�Uʽ�_Vh�L�m�~�IK�@�R�d{8��d�H�}G/ܝY?,��<*�~��3co�^8�]�b���' ���Љ�lE�1�Ʋ9��a-���@��7��F[���YXf҉Yf����
KV��v���<���4�旧A��ॸx��3W� l�($y���n _�����x���Ɵ�jh��+������Wͻ�p��L,w������c�]����J�Ts/̹\��H�#<��L���)y*bv UL�0 �>��,aK���h{F%�3+˞�l-spo�oϰϙ׳n�Y�V#�tT6�H^��/j{WvM�/�V���ڽe�sV���
���C�?$y���.��Qd��`�?����8U,,�L�c|ͣ=���3p;��o
���5�28�C|�u��O�d?��ؖ��ϱd�u�9~	P%�"cw`;,�a��O�V�Sh�-mM��4��h^��nM19����;�Ď��K�ں�%���W�i��wg�\��~���pw�B�����Lf���I�
��� i�k�cD�'nY-�&ٹ�>��%&��ZT^�+r�*��9���8��gy��ٕ�� �!�y*vֶ��eky�ӱ� K������cy�%̱����z}�Rؒ�Wdר�}O������s���F�L\��bc���O_&s��#d���������_Ɛ8-��M���a/��W��v^���qv�;�'�����n�����Tx�J�c�Smk����2�~��WSl�����L�}l�/�ʽa���r\6
)�y��\>�n�2� `nc����>|p[qu��&�q�bZ�8]�X���(E���צ�qU�g{�����C�Y���⬼{V��8�3�߇3[S��G<�Q�l3[Kp"P�Jx:�j �o��fG��S��k �fgc���k� �\��t]�%��+ז*[ ���|���4��Vt����;|f�Xێ�}8��|����R�O��]p"N�5.�d\K%8���Q��a�Χ1���9�ý�%�,��]����R��#]��3,� �$=��,��0�L�hY)G ��և��V�>�G�π�#��"��Oq��=�?���-mM Ni��Wb����1`�i�`؃1������ �8�?�d��᧙�� ���E�'N�л��+M5��J,W�*�n��)	Z���B��r-�ߺ��\'p/���L���mkI�Pؒݲ���e�����'N�K�0�^���� �-�;,��"R7 �&Ǘ8��e�8�Kz�?�����m�@<KÆM�	�G�t	�^�&�a����}|>-\�8%�t~YJ��C?�2	�\�3��x��}�mk���O�FXq̧�}�c��������-]��8v��.�[ϻU���׻e�bv��0�F��$�}{���J�E���~�������z޵U��j�0���.�1�e8����d��~l��;(�;��(y����Z�7bx˞I�'F]���5ۗ�c{-`��k��ч%O۰|O��d������ʧ�b0d:�8;�W����oUwb��f�@��P5�ω�uo����J^
W�2�ո���]^$M3�sɭ(m�P�[��0�b�����|�syÝ���qy�/�=��J��<E�nϑ��3�Bq)�`��&.3��:]V��p%�l��c<|A���Ǹ~uW���w�{j��F$]M�B�I,�$���J`�|�+	/����Ps`io���A�;zm}���ш�pu<�.���Z������ȹ\	1��k���j�1��m��w���/l�-�3����Y��<E����/cݼWo`9�4x9޼�qU������;=�{�o�f�����-p.�JP��`�$�O�+z�p�7�j��3W5	_Bs?vj���������wR�4��qzwgw�4)��ppT͉a�ki���+��ͮ��W�$���@F�D�.rJ�"`��)��(�:M�`p�i�~�c�U���d>�J,+v(I��G�y�d�ֿƂC'���U��2��{�uĂ=��#�9ĩ+�9�F��=�êᵸ�Q�p�[V��}�a�L|�Ԃ4ObKg�z�u��GXNgY����mٔH�LJ�
d�p�{�u��K	MO.2#��n�-��y����]��r��Gpw��-���k�_%=�V�yƝl~��;3��� �@��F6˂�Q0��N&��ij��7�'�ո��U�R�Á�$چ��Z <��`�1�t��?e:�?ns �=�a����؛�c�rQ��<��e�;ɿ�I�1�g)Ǜ�,�0��}!�6����U��}�2�I{~��yrV'����'=� _3�̀�{�\H-�Y�+�2݀�`���ݡ(�,�A�q��i�'�C�
�(�qQk��b����2���,�&C�n�²�-qIe/\�_l�ßaه�� ��b���ۗ��\����x��|���ˈ����*�3���O�� v�k\O@�{��^��"���h:�)Ϧ��6��+l{G��j��h�2��KT�o1�g4���H�K��`��`l-#[?)q���t�9[�f���E��l*��N�i����1�Ve]�7j����	�T��Ҟ%��	�6�q�A$zK���8eG�S,�r'pY�s)�/�ت�8�����rQ�`�&y�x<��<>���)��|
�HH�{�8���O�Y��l���W�,��d���(y�E-�`9#�i��M��d�m]��:Y`bq�y��7��\�81�	�'����HC?Lz"	�Gp��%�Ln���j���%)�k�r��P�)%J��dk��e��(C[O؝X+ϯ�g7��g�e:��<�9pv�c$`�th���IϤ�M��}���"
y�a��#_�b��h�:N��8�۽�),[J��`�r*0,�y��Y��vV��k��<�~��ډ��w���Q�A����IϤD�� Y�K�ո�CA�y#N��h~�%�VD��k,�r�%=�R��hm����o�R��a��d�E�H1�F�ӃE�D#;�&����Xza�;��+�֬�� �8�d_f[\�\��|��sϙ�;.g���]�j�G$=��a�C��0*�r�������W=<�R���s�>��ù�}Γ~��a�gsR�	���\0L��0�*^0S(�TO����X�ǯ�b���i���llX��K�K{��\u�⹻ o�9N�8��;�,�ׄp��S}��q
r?�Q[בTA��8���^�[ *�DZ8�;�<鉔2�P����f.I5V]�+�o���>�;�K�1����$������y)�熽�����⪊o�����W�.��W̾~ô��q��V��<�~�Q���DA;��K�[�wˋ��<���$�8���,bc��پ' 3�oLw��Ž �!��Nd���vX=��~��p�ݳM� ��s�"4�Qh�	�����W �����1��SI&N w��?0o=K�\,GpW(q���� �/Ga9Y78�>lk�r���%	���[��Ӱ|�����G��ݯZ`��k��w��$������Q�@� 0Cp�P+Y#�k��F|��d�H�)���D�4����q�gIO��ð#��D�)'e���=�bn�L\#XĨ����>��V\?���4SC�"����&�&�\��]�.���qX]�ބo\/aWn���Ò�I��D�|>-���q��۾wdZ�W��K��w\�e0ű�3N�b�ct`�f)��<�U��M�&���i�7Ř8�i<��/q����lg�.�����8�����ݐ���݊���s�qҳ�����0|��@����)�-q��E�&�LJx6qY��&�)q�G��
l-���(cI1o�^Lc��4�d�-�>zV[X_�
8 �kr]�����R�Z�Cx��2M��:j��,Lz6�, �Cso�7�p��<^�Eoc
���I؇�R��Uv߁�\���[-
Z�keR�4^�m���?�I�!�B��k]�C<��2X�6���
��
����粌�x	�r�I�H�o��)�MI��UJ� ���t.��_G��K�|�6�U���r��EUP*�u�s�!��$<�(d�b�_n�ͤ'S)�<��}�96�#���afFi�iM6Ggy��۳�Y~J�2�i���ý�7f �\��+w�b����rnJ1.[x�����Y/������u�<�]�b4pn��wA	��L��~ �a�9���	�a����d*��'��f�� ������p3�<����ںӷ�M���x�S�b��HW�\[����p�3\-��N�%c&]K���%���	̓`���p��'TBj�O	�zѴCJ��Ԓa��`CO��i��,�.�Ȝ�'S��<�6�NF��Љ�d�3`k�
���af�@�q����xN��Wt,no@О��d]Xk������ ���XzXz"�A��v���b������hi.o�<�)�]y�:�NTs8N�p��oӇ��C�{��IOF���-QvV�+�|7!*�a�r����r=�ڶOM�^�qOXioT��Z��=�����v�>�D�۸�������}!��]b�=0�p�'���Oc�\=��P��p܇��s1]hd,��N��O|��^��㹍7P�Ϣ�䩖��=�`�P�k�R��W�ۥDerUg�����]�+��6�u�6!�~H�/R��egqK���~S��T_��pI��@��6�6�� f�m��DZ)e�n�:�����x�����5�a'`{���"��X�>fa��a�ـ�H�)|��KE'Ov:��wp��}�`9��NR�k���"O,��)S�|x�gpL;_Z�����Sp�x:�U���v4���%P�����m!��&d���
���`�Y,� �2���~_zcq>pK��^��r\�F4�CO���C7\R�&�F`!���_���C�1�k
k�.��mS�0�@�R' �K쎜D��� D�̵�;&x�XG��F�8}��ӑ͋X��AІ��P�t3�N�?R��ڱwm����g�Lũ�"��y�lOC�{gʒ��LL�q�H�ו���=��9B{��
��R8�B�eJږ�K���1�E�hTl�d{�n��Iq��2׬�T�!��ZAR�)k���<܆� ��߷���V�sW��͋���	���O�1y�\�<щSp�W�gb���¤X]@�=�r��������GOh� 7�1�W!�a'�DʇN��%;�h��<YN���p���%�4�0�G�^5��ʥ��;u�-+l���<��Hx{-�I�8 �2��o��b��HQ�����eG���lfg���u�Z��O]��[�~�[puY�|�+�YH�i�{�r�Q�~
��)y�<a�5��A{�]r�K,�p��؇-?��9�Rq�<�{�-~L�}�����S�R&uMp    IDATV��<�%;�x��<� /Q�*u�x���/�ۻ3l�Nݵ-�p};_z�O.^$�%E7`��$��2{��/R�*.y���	��OY��<�-if
�h��[�tV���g]X���?F4�|\a� Z��R�^m'�	��Wq�K�?�1�mR����n���q�z!O�4�)hߓ����C��=1�E�(U^�z���^/qˀi`6�Æ�9;�����#�w��,�WA�)�)��<Ut�#)i��oo��)[��<��!�����"�`��d�Q5ն�̀ˀ�B����un���<��k|+R�ԎE���<��<�Q?�?�G����g ��Wa9�/����|�a	j�"�_�<�]X�8���æ��L�c`v�qg7�W�<��mhߓ����<>n�-R�**yb	����y	���S��kޛ��k�r�	�����I�E���o-ى����'CoQ_6�9��lhCݼ����L�D��0E-�is\��Rq�w��{�l��VYɓ��<5x�Y�L�߫ޫVtZ�;Y��{�W+�7�9�D!l��_q��E�Ue%O>�fE�\U��{ڤT-/
��u���Y���Tt��{�%;�6*+y2t��m_5	�Zm�Оr+��.O��!�����$��G���Ӫ"%���'롲x3D���u����߁�Q��RX����DD��dww��)��<��G�b~�1�ۂ���bp��ܚ
|��OE�(m�x�9A$���BUE���ȿ_u=,�:n2�K!״t'��t�k�M >�y."%�Ғ����2LỈc��ȓ�w/����$���ǵQ\$D�%O�"��l�[**UQ7�X�`A��]�>i��n��,m-��#� ��<E��Sck�⍻��H$z���� �.d5�G�s�V�F���.�H�����ǋ��*�&�r�(�횀WC�i�N�Q5��kZ�iG�%O�ߡ�x؄^ޢ�~���rX�%OR���x�3<��)'��<��C�b��c����G�*�e;���'ű/KdEaKv�O_H��UY�S�Û��W�1˔uu���~-(�e;p�
�_��\Dڳ6ptȵ{✈H)����𥇘�E�\ղ).���B3�ƈc�k!09䚖� tx�M���"Rr*-y�����w�1˕��U�5fV�')aKv�(.���J�,3=��1�l��^���&,yڗJ�{�b�5�W����}1�E�$U֋y��7���l�!n9u��4�i�1�<��S���y."A�����E�s)I��<��g�����>)(7vV#��nA���]��[�/!4I�ҝ$� ��\Ӓ�H�**yjm������,7]��u���p?��M��}OR���-۵5�\DJVE%O����h�cQ��(>)N�:���9���[��_���i�Xg!������^HDT^�d��!j72��Cܲкd������K�Z�\�y
K�6��q"+Z�c�D%&O����иa��du�֊:�U�<_�0�q���q�5-�IR��?$<�� �<��|���`����2��#l@��UG���x9z!�q%O���xw���H����1STs���%���^@���.������K���G��hӸ���a!��s""�2�'��^�ZN�۳��إ*�U>¦R<�Ԇ�A�<�6m~.���$;/�L'����"�2�������)�=�-Iv{������>�H`N�S�%�^zٚ�x������I�XD"T�ɓy�<�?��Q�)R��)�b��w�}��������H��&�\�ҝ�i{`��Ǜ��1�E�,Td����'��[���*��Z�v��a�:߶�����!�=���ytD����������"���M��|�)�.L�s¬����q����c���<�b�"�	�<���D�b��SB�i�N$O�<�`��2���e'�񋒅U܍��N��Г���\���]�gJ�� xI��3�He:�4��p�*D$�<��h�{5�~[K7O�S-W{��g��L]����c��j@Eޜ�K��\�ҝ�!l��}@c�)'�<���<�q����;aU�l-�Wzb����) ?#�@e\�m��X;��I��B�Fi�N� �<`d<�p"����(ؾ�k.��w���M�v�%�q�׮Y�xk@�%O{ �c��T��	�[��J�s)+�<��L����\j�r��1c��5�G�	��j����p���j@m�#�P/ᎄ�Յ���"Q	[��j�"R��O�Z�>�e��[~'�l-�`��w �l�����}�(�k@�܅�����!״t'�l��,ޖ��\Dʎ�'�L�5ܝ��`m���<Nl��+���c��׽�H����˒��=I�N%�8쳸"R %O�.����r���m�?�������P2L ��C��t>��k.h��%��y��VX�/�T?��wj���(.%O��T����F�qs�dwd�XƋ�ck��?����l\!�v��v��@���(<O�5�Z��Je��!��%�kK%RQ�<��;F�cmwZx����1�W0[��2A\wL�"�#x	�o)\}(_5����\�ҝD-l����߶F"R %O+0��n������Q[�8�#��8nNZ�6��M�&T_?h��dD�2�7�C�/��MD㵥}O�j�>� Z�����6Z߬�~�9�޶�\Pl{�l_v���7���_����\�
���Tψǅ��icI�:� ��9�S1�E�l)y
RŹ�/���p3sy���"���<�Jl-;پ<��`����43��C�e5��r}��^����B��*Ɽr�-���m�D$G�6$�/;b�7ѿ�f�7��{=%���Ԑ�HR�ǘ!F�.�<�n�D�e��-DY}�`�����W��k�Ү��
v�^2U@3d�C�|hZf�Ծ�`݁/pK�m�#�ޘ��H�S;l-g�Ox-�x����f�n�S�\�N�%�D?/ғ�i���q�h�6����p�1!�ד����z�Rہ�l	9-W7	��e�Y�T��G������p�w�����Z��K,�A�<�+v7��|�K�a�ѓF�v'ŁX����n��a�g��q�c^��}��%���Y�&�e&������`��[�ƃ����W��+&����R����"R֔<u��E��$��a1j���>b�X,Ų&Ѝ݀���Nr����!fj�i8�n .
���x$�q���\��<�1v��2Rǀ݉d��� o � 3F|��\|�W9��kz�rjm$"P��k����NIϥL�`h��pB�p{����p�r��ܞ������?!��	Uǂ� ��użv�{�py�*�7�?�\D�^���ۛ�����[��~f����x��B�s��)�����q��D�ݯ"u����2\ҙ���-�w0�`ؔ�'�/���;i'"R�[�6���Iϥ�\n��$Z��+��m����>����K�ڀ��ĵ�(W��`�G��k���`h�w����_�Ja,�w:"�Ou�r`�M�C ��e���(q���k@m�й�1e��uWĮ�u�!�n���'h����=	�K��{Xm����Iċ2x�ݞ�I�eyB*-���!�Xu1��@x���ɯT5��]PS�#��ӳ�6�N#ܞ�rg���a�GI�$K5�s=��;�+"ӝ�<��|����R�s)A��	E�8��+\�� �3v3�ϋ��G��.����8أ�jԥ!]��l�p��ӧ��"⁒�<�����V��0hx(�da,�[�|0��D����Mo��8�%+g]����U��;��t ,�����x�e�فT1���\I�5m�ݫ�8Ѽ�{IO$G7�
eɷTܲ`[���Kr���U�핸���}��i�<�Q���:�%��=x}q��E�%O���	�0���f�����LR���?��0 �ԗ�.�7��*m�;љ��z�t�C���M N�a�T��|��߿�۷'"�h�."��	���ڠ��<G�i\T���}O���mu�	����a��\Kh�n 4MV�j ��PwX�YAؒݘXg!R��<E�4�}8�� ��$�a�����cIO$��c/��!�Ork}SD���	�GwM;`�s���-�w���c��H�Q�13���ud�Me~|��7o�*�~Z�j@��އ�j@�%O��^#�N��k�kA���`��$�������	̉y."G{�<k�u�͵�-�ci�A����w�2����P��8k����eg\�q�VA������q���������1�k�w�-��p�D�ҡO���&��θ['=��?REo��e�8��ct�5�N$�Ak[_~���݅�a��8�4X�!��K��@p��50.湈T$%O10�i4��jl���>3�9E���6���1Sx?�	�h,P���+�s�������:��J,Hᎆ.�\B������.E�3%O12���4p7}�ŝ�z-�9�a�;`���fjY�M��H���/s3pt1�N���n�D� ��gH��zڅ�ķ�X�$B{�f��=p���_���܍�.;�*�!�ؐ�pK,a��6>	��x��٭��:��hc�
n�ax�$����;���^;����"a!E��,�����%�$,c��A�6_'<�b�:�4�g���[��5�}�*��3�?<����.^����|v�5�&05����㪯��Mp��������9L��8���@�S��;��p0�	n��`20��Y���;*=��mB�OÕ �&��]�}pA���܀��`�x��瀝���dh�	#rH�����^���?���5���3���߸>�Ne��c�-.�ˋ���B����'Co�0l��7�fw:�]`0�,`�)���O��lL"��18�U7��	�1�������ʭ!�:�f�
2�0�aD��d���h�u����dv�Q�q�pM��/PTM�Eʟ��g��;Ut#EW��N�5�nX:c�X,Ĳ�N|�v�7c�mݓ�q5��N^݇;(�b�Ћ�ӗ�������}�]�Q�&�0w��c�@�>`����
��W�f_HG�?p2����p递'�h����v��׸�)+���=n'PP���7��9H#�0C`h��1/�R��3ܞ���`xT�k���/���-	��'q����j��\�k��b����@?x<�i�� �6��͛s7,=
F��Ϗy�6&}/���3n��������O�磨�v�w��� ���@w�D��8?��R�(��E��{�)���ݡi:�Q�_��Y�:���q����V�o|c�/�y;�mAR�Ӛ\;x���"����"���k�p�ǖ%Fa�2w �i��-���+�i�%N�|ٷ�'���A�.`�#�zHvH�r���\��c�Ht�IğՁ	��!�?�-�}���t"�(����W(q��5u;���qv��ð�:|fѩ?�]�������&������u#���g%"yӝ'��>�������!�Yv���I���U��N�N]ő8Mӯ4'����쌫��[5n7����`�v,"	Q�$�����Z����7�(�~��E/�
m.�<5���<s1	2�'{�.
C?�����bl|Z_7�@�5w�QD��IĿi�;Pa�^�p7ܛ{�6�����n�U���G0׎<5��V�����������X�*H�zWpP��w:ɟ�<���D\����fr[r������=�';:�2lۓ������y�A0,���|ۖ�� [ %~�O�t�ΓH|�����ޕhM�n߹O�m����L��U��C0oz��uoZ6N'��3(qI��'�xn�6�vx��14
�2�b�����ӏ�lK��sOy\�E��I$~��O�O�_3��w_��x��I�%T_�x�n��m-�v:"�+%O"�i���h`�hBn�M�@�>Ʀ�Eb��`�����+v��Ia��,�xB"��8���T���#p��zF~��C.�4����W��t0�$��4�Ր��p��h�N��Γ�_��'�S�}�a�&׫.�6&�&���ل��(�9ɝ�t$��/��6��H<���p^��lt�>�3����4y��S��xxȵ�%�{�O��(y���'��<�d�!,�+xi�|�'�	J��&�`W���D�3%O"���-�u�um�����{
^b���<?
.n{��\�� ��4ɑ�'���L�&Ԧ�O,�����!p	���Ga�V�.{�y,l�N�E���'�x����M�ͣ	��̵���Vz	���6���&�T|3�:��	%O"����]�-��\�g�K_�?���]�� ��>TH�R�Ҡ�Iį}��t�Rg`���-�>�G�җ�
x�C�� ��4��-ى%O"~5��:Ex�`3�oP��M�]�nA��q�]ɠ{>��H�<���o\/�������9���r��k����J��.����G�F��A)2J�D�^ .b�Djn�zD9�e��;��\>�M`��>nK``�E�)BJ�D����DjC���^�գ}$Of��������qw`���G?��JɓH�Z���ɸ;R�u�%k{�FJ�SV���r5��\����-(Rܔ<��9����������wP�u��T3��� UWC���F;��DEɓHq�
�|��~�Y����^�Jg�w������%��3�/%O"��S�`�U/��a8�7��̊>d�w�(.R̔<�����R֠�����d��yH6WZ�m(�"��I�x���]�Pg�Xw��x�~��s}P�w�"��I���|X��o���Z�<|�V��j�N��)y)~��c�%~�'��C�2f|&O�z�H��<��������%#%O�i�<�4F_D���I�t��}XS}�r��k���"�d'R�<����<�yJ�}̲�����~�+����)y))>�ج�����~5��c)	J�DJ���ʦ{�1˙��W��}\�AɓHI�q�)�y�1�ږчL� �%��%O"%%5?���w�1˙�����t�1E�%O"%��#A�<��G������_�<����,��T���E�L���D��ړ#�)")y))� �G4K��8f�j�0��YqL�HɓH�}Ȫ��,K�O���SD|R�$Rz<��Z%OY1z���I��(y)9f���;�ey�[F.��1��fF�1E�'%O"%�e���UPu���e��T��f���)">)y)9�N���'b�s����f���"⑒'��3�x6�����}�rP�+��C����x�+")y)If����K��-u�R?q�3~⊈OJ�DJR�S��	0����%�m����Q�$R��<����N>��
R�y�[ª��<� �뤝H	R�$R�,p��Ч������\�X����OD|S�$R��ݞWC�-D߆��h������'�����I�d��uO�����y�]"���S�k�N�t)y)m��>����_�bV�)�����u)aJ�DJZ�>`���낽Ҟ���U���w��� M��~O�E$J�DJ���b�K�1~Z:��� wC�K��E�3%O"%/3h��^�'��_L�N�W��x\�8(y)y�g����R`�@�a�(W�	����0b���"%O"e���~�0���[�b7�<t�8H��{�/"1Q�$R�M�yd����������z
��y������(y)v0�N�-��=����A�q`M�5����"��D$/̅����������~����=�勁������a��0�A��H*���H�I�M�18�0|^L�E`p0�ӀAS�fQL㉈gZ�)+����*�������P0��/q�J�Dʋ�<��������C�`�g1������^zY�  �IDAT�8����cbSD<S�$R��6;�z1<�ph�F-�y� ���t!p9�F�c�/����x�ߔ<�����S$s0d>pԌ��W�?����k�?>�p�Tc��gJ�D�Z��>�	,��x�i뱍��*��H�V�7VG�o`��ɍ/">)y)k�jhz�?� ���}:O��9nz]h��@\´a�1e'���&�"�$%O"e�~c�/�$=�d���-�3!5R���[0`�7�v3�X�P�&4�z����@q��}��>Oz""�O1�興7W�B�s�:I�$͸���b���!ӓ�����:O"���J��P)%N����I�2(y�������#�Y
�G0|R��x���HEy���C���)
-�0���'""�ў'��T,���.IϤ�5�C�&=���'��5� 0� k&=���ca�sIODD��I�����l��LJ���0lr�3�dhσHE6��G�@vހ���8�T6m�x�};�5����l�����k"��."�L�v"������V�3)"�a��IODD���'i�m u�k�3)�@�$�^���=O"�ƈ�P����o��MB�~���ID�ҝ'iǥ@�(��R9�c�a؜�'""ũR^E� u����I���d.���&=)nJ�D$[�{%�{ғ��y�uPs�3I�FD���'�C��`��AIϤ ������6�ɈH�P�$"�������I�&��r�'%=)MJ�D$��`�!`�����
Z�NpE.��k%=!)mJ�D$b�n �G�98�(�I|
��� ��䜈DIɓ�x6x;` ����z� 5�v60x���!3#�/"�%O"��U�{h����n�t��Y�@5�f�o���"�k0�Y0�ے���������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������H��]�g��pJ    IEND�B`�        ECFG
      _global_script_classes             _global_script_class_icons             application/config/name$         Neural Network Visualizer      application/run/main_scene$         res://NetworkVisualizer.tscn   application/config/icon$         res://icons/NeuralNet.png      display/window/dpi/allow_hidpi            display/window/stretch/mode         2d     display/window/stretch/aspect         expand  
   input/draw�              deadzone      ?      events              InputEventMouseButton         resource_local_to_scene           resource_name             device            alt           shift             control           meta          command           button_mask           position              global_position               factor       �?   button_index         pressed           doubleclick           script         )   rendering/environment/default_environment          res://default_env.tres                 GDPC