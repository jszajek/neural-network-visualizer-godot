#include "neuralnetworkbase.hpp"
#include <time.h>

using namespace neural_network_base;

#pragma region Axon

/// <summary>
/// Initializes a new instance of Axon
/// </summary>
Axon::Axon()
{
	delta = 0.0;
	bias = randomFloat(-0.5, 0.5);
}

/// <summary>
/// Axon destructor
/// </summary>
Axon::~Axon() { }

#pragma endregion


#pragma region Dendrite

/// <summary>
/// Initializes a new instance of Dendrite
/// </summary>
Dendrite::Dendrite()
{
	weight = randomFloat(-0.5, 0.5);
}

/// <summary>
/// Dendrite destructor
/// </summary>
Dendrite::~Dendrite() { }

#pragma endregion

#pragma region Layer

/// <summary>
/// Initializes a new instance of Layer
/// </summary>
Layer::Layer()
{
	// Assure randomization within each layer creation.
	srand(static_cast <unsigned> (time(0)));
}

/// <summary>
/// Layer destructor
/// </summary>
Layer::~Layer() { }

#pragma endregion

