#ifndef DENSENETWORK_HPP
#define DENSENETWORK_HPP

#include "neuralnetwork.hpp"
#include <string>
#include <vector>

namespace neural_network
{
	class DenseNetwork : public NeuralNetwork {
	private:
		vector<int> topology;
		vector<Layer> layers = vector<Layer>();
		int output_size;
		int input_size;
	public:
		DenseNetwork(vector<int> topology);
		~DenseNetwork();

		int sizeOfLayers() { return (int)layers.size(); }
		Layer* getLayerAt(int index) { return &layers.at(index); }
		Layer* getLastLayer() { return &layers.at(layers.size() - 1); }
		void addToLayers(Layer layer) { layers.push_back(layer); }

		vector<int> getTopology() { return topology; }

		bool run();
		const vector<float> output();
		const vector<float> train();

		const string toString();
	};
}

#endif // !DENSENETWORK_HPP



