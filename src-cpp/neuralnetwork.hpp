#ifndef NEURALNETWORK_HPP
#define NEURALNETWORK_HPP

#include "neuralnetworkbase.hpp"
#include <stdlib.h>
#include <math.h>
#include <vector>
#include <string>

using namespace neural_network_base;

namespace neural_network
{
	class NeuralNetwork
	{
	private:
		vector<float> _input = vector<float>();
		vector<float> _output = vector<float>();
		float _learning_rate;
	public:
		int sizeOfInput() { return (int)_input.size(); }
		void addToInput(float value) { _input.push_back(value); }
		float getInputAt(int index) { return _input.at(index); }

		int sizeOfOutput() { return (int)_output.size(); }
		void addToOutput(float value) { _output.push_back(value); }
		float getOutputAt(int index) { return _output.at(index); }

		void setLearningRate(float new_rate) { _learning_rate = new_rate; }
		float getLearningRate() { return _learning_rate; }

		void clearInput() { _input.clear(); }
		void clearOutput() { _output.clear(); }

		virtual vector<int> getTopology() { return {}; }

		virtual int sizeOfLayers() { return 0; }
		virtual Layer* getLayerAt(int index) { return NULL; }

		virtual bool run() { return false; }
		virtual const vector<float> output() { return {}; }
		virtual const vector<float> train() { return {}; }

		virtual const string toString() { return "A base Neural Network."; }

		inline float sigmoid(float x)
		{
			return (float)((1.0) / (1.0 + exp(-x)));
		}
	};
}

#endif // !NEURALNETWORK_HPP