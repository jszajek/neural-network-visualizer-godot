#include "densenetwork.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace neural_network;

/// <summary>
/// Initializes a new instance of DenseNetwork
/// </summary>
/// <param topology>The network topology</param>
DenseNetwork::DenseNetwork(vector<int> topology)
{
	if (topology.size() < 2)
	{
		return;
	}
	this->topology = topology;
	this->input_size = topology.front();
	this->output_size = topology.back();

	for (int i = 0; i < topology.size(); i++)
	{
		int lay_val = topology.at(i);
		Layer layer = Layer();
		for (int j = 0; j < lay_val; j++)
		{
			Axon axon = Axon();
			if (i == 0)
			{
				axon.setBias(0);
			}
			else
			{
				for (int k = 0; k < topology.at(i-1); k++)
				{
					axon.addToDendrites(Dendrite());
				}
			}
			layer.addToAxons(axon);
		}
		addToLayers(layer);
	}
}

/// <summary>
/// DenseNetwork destructor
/// </summary>
DenseNetwork::~DenseNetwork() { }

/// <summary>
/// Runs the DenseNetwork based on set configs
/// </summary>
bool DenseNetwork::run()
{
	if (sizeOfInput() != (*getLayerAt(0)).sizeOfAxons())
	{
		return false;
	}

	for (int i = 0; i < sizeOfLayers(); i++)
	{
		Layer *layer = getLayerAt(i);
		for (int j = 0; j < layer->sizeOfAxons(); j++)
		{
			Axon *axon = layer->getAxonAt(j);
			if (i == 0)
			{
				axon->setValue(getInputAt(j));
			}
			else
			{
				axon->setValue(0);
				for (int k = 0; k < (*getLayerAt(i - 1)).sizeOfAxons(); k++)
				{
					axon->setValue(axon->getValue() + getLayerAt(i - 1)->getAxonAt(k)->getValue()
						* axon->getDendriteAt(k)->getWeight());
				}
				axon->setValue(sigmoid(axon->getValue() + axon->getBias()));
			}
		}
	}
	return true;
}

/// <summary>
/// Gets the output of the DenseNetwork
/// </summary>
const vector<float> DenseNetwork::output()
{
	vector<float> output = vector<float>();
	
	Layer *last = getLastLayer();
	for (int i = 0; i < last->sizeOfAxons(); i++)
	{
		output.push_back(last->getAxonAt(i)->getValue());
	}
	return output;
}

/// <summary>
/// Trains the DenseNetwork
/// </summary>
const vector<float> DenseNetwork::train()
{
	if (sizeOfInput() != getLayerAt(0)->sizeOfAxons() ||
		sizeOfOutput() != getLayerAt(sizeOfLayers() - 1)->sizeOfAxons())
	{
		cout << "Inconsistent Input or Output" << endl;
		return {};
	}

	if (!run()) { return {}; }

	// Loop over all output axons
	for (int i = 0; i < getLastLayer()->sizeOfAxons(); i++)
	{
		Axon *axon = getLastLayer()->getAxonAt(i);
		axon->setDelta(axon->getValue() * (1 - axon->getValue()) * (getOutputAt(i) - axon->getValue()));
	}

	// Loop over all layers - reverse order
	for (int i = sizeOfLayers() - 1; i > 0; i--)
	{
		Layer* actualLayer = getLayerAt(i);
		Layer* nextLayer = getLayerAt(i - 1);

		for (int j = 0; j < actualLayer->sizeOfAxons(); j++)
		{
			Axon *axon = actualLayer->getAxonAt(j);
			for (int k = 0; k < axon->sizeOfDendrites(); k++)
			{
				Axon *toChangeDelta = nextLayer->getAxonAt(k);
				toChangeDelta->setDelta(toChangeDelta->getDelta() + (toChangeDelta->getValue() 
										* (1 - toChangeDelta->getValue()) * axon->getDendriteAt(k)->getWeight() 
										* axon->getDelta()));
			}
		}
	}

	for (int i = sizeOfLayers() - 1; i > 0; i--)
	{
		Layer *layer = getLayerAt(i);
		for (int j = 0; j < layer->sizeOfAxons(); j++)
		{
			Axon *axon = layer->getAxonAt(j);
			axon->setBias(axon->getBias() + (this->getLearningRate() * axon->getDelta()));
			for (int k = 0; k < axon->sizeOfDendrites(); k++)
			{
				Dendrite *dendrite = axon->getDendriteAt(k);
				dendrite->setWeight(dendrite->getWeight() + (this->getLearningRate() * getLayerAt(i - 1)->getAxonAt(k)->getValue()* axon->getDelta()));
			}
		}
	}
	return output();
}

/// <summary>
/// Gets to string representation of PathNode
/// </summary>
const string DenseNetwork::toString()
{
	string returnString = "Dense Network:\n";
	for (int i = 0; i < sizeOfLayers(); i++)
	{
		Layer l = (*getLayerAt(i));
		returnString += "Layer: " + to_string(i) + "\n";
		returnString += "Axon count: " + to_string(l.sizeOfAxons()) + "\n";
		for (int j = 0; j < l.sizeOfAxons(); j++)
		{
			Axon axon = (*l.getAxonAt(j));
			returnString += "Axon: " + to_string(j) + "\n";
			returnString += "Bias " + to_string(axon.getBias()); 
			returnString += " Value " + to_string(axon.getValue());
			returnString += " Delta " + to_string(axon.getDelta()) + "\n";
			for (int k = 0; k < axon.sizeOfDendrites(); k++)
			{
				Dendrite dendrite = (*axon.getDendriteAt(k));
				returnString += "Dendrite: " + to_string(k) + " Weight " + to_string(dendrite.getWeight()) + "\n";
			}
		}
		returnString += "\n";
	}
	return returnString;
}