#ifndef GDNETWORK_HPP
#define GDNETWORK_HPP

#include <Godot.hpp>
#include <Node.hpp>
#include <vector>
#include "neuralnetwork.hpp"

using namespace neural_network;

namespace godot
{
	class GDNetwork : public Node
	{
		GODOT_CLASS(GDNetwork, Node)
	private:
		String test;
		NeuralNetwork* network;
		const std::vector<int> convertPoolIntArray(Array _array);
		const std::vector<float> convertPoolRealArray(Array _array);
		PoolRealArray convertFloatVector(const std::vector<float> vector);
		PoolIntArray convertIntVector(const std::vector<int> vector);
	public:
		static void _register_methods();
		GDNetwork();
		~GDNetwork();
		void _init();

		NeuralNetwork* getNetwork() { return network; }

		bool createDenseNetwork(PoolIntArray topology);
		void setLearningRate(float l_rate);
		PoolRealArray trainNetwork(PoolRealArray input, PoolRealArray output);
		float getNeuronBias(int layer_index, int  neuron_index);
		float getDendriteWeight(int layer_index, int neuron_index, int dendrite_index);

		void clearNetwork();
		const Dictionary getExportDictionary();
		const Array importWeightDictionary(Dictionary dictionary);
	};
}

#endif // !GDNETWORK_HPP
