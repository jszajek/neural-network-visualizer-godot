#ifndef NEURALNETWORKBASE_HPP
#define NEURALNETWORKBASE_HPP

#include <stdlib.h>
#include <math.h>
#include <vector>

using namespace std;

namespace neural_network_base
{
	class Dendrite {
	private:
		float weight;
	public:
		Dendrite();
		~Dendrite();

		void setWeight(float new_value) { weight = new_value; }
		float getWeight() const { return weight; }
	};

	class Axon {
	private:
		vector<Dendrite> dendrites;
		float bias;
		float delta;
		float value;
	public:
		Axon();
		~Axon();

		int sizeOfDendrites() { return (int)dendrites.size(); }
		void addToDendrites(Dendrite x) { dendrites.push_back(x); }
		Dendrite* getDendriteAt(int index) { return &dendrites.at(index); }

		void setBias(float new_value) { bias = new_value; }
		void setDelta(float new_value) { delta = new_value; }
		void setValue(float new_value) { value = new_value; }
		float getBias() const { return bias; }
		float getDelta() const { return delta; }
		float getValue() const { return value; }
	};

	class Layer {
	private:
		vector<Axon> axons;
	public:
		Layer();
		~Layer();

		int sizeOfAxons() { return (int)axons.size(); }
		void addToAxons(Axon x) { axons.push_back(x); }
		Axon* getAxonAt(int index) { return &axons.at(index); }
	};

	/// <summary>
	/// Gets random float within bounds.
	/// </summary>
	inline float randomFloat(float a, float b)
	{
		return a + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (b - a)));
	}
}
#endif // !NEURALNETWORKBASE_HPP
