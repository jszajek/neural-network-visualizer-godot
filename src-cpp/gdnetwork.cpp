#include "gdnetwork.hpp"
#include "densenetwork.hpp"
#include <iostream>
#include <vector>

using namespace godot;
using namespace neural_network;

/// <summary>
/// Registers methods for use in gdscript
/// </summary>
void GDNetwork::_register_methods()
{
	register_method("createDenseNetwork", &GDNetwork::createDenseNetwork);
	register_method("setLearningRate", &GDNetwork::setLearningRate);
	register_method("trainNetwork", &GDNetwork::trainNetwork);
	register_method("getNeuronBias", &GDNetwork::getNeuronBias);
	register_method("getDendriteWeight", &GDNetwork::getDendriteWeight);

	register_method("clearNetwork", &GDNetwork::clearNetwork);
	register_method("getExportDictionary", &GDNetwork::getExportDictionary);
	register_method("importWeightDictionary", &GDNetwork::importWeightDictionary);
}

/// <summary>
/// Initializes a new default instance of GDNetwork
/// </summary>
GDNetwork::GDNetwork() { }

/// <summary>
/// GDNetwork destructor
/// </summary>
GDNetwork::~GDNetwork() { }

/// <summary>
/// GDNetwork Initializer
/// </summary>
void GDNetwork::_init() { }

/// <summary>
/// Creates a DenseNetwork
/// </summary>
/// <param topology>The network topology</param>
bool GDNetwork::createDenseNetwork(PoolIntArray topology)
{
	vector<int> top = convertPoolIntArray(topology);

	network = new DenseNetwork(top);

	return getNetwork() != NULL ? true : false;
}

/// <summary>
/// Sets the current network's learning rate
/// </summary>
/// <param l_rate>The desired learning rate</param>
void GDNetwork::setLearningRate(float l_rate)
{
	getNetwork()->setLearningRate(l_rate);
}

/// <summary>
/// Trains the network given an input and desired output
/// </summary>
/// <param input>The network input</param>
/// <param output>The desired network output</param>
PoolRealArray GDNetwork::trainNetwork(PoolRealArray input, PoolRealArray output)
{
	NeuralNetwork* net = getNetwork();
	net->clearInput();
	net->clearOutput();

	for (int i = 0; i < input.size(); i++)
	{
		net->addToInput((float)input[i]);
	}

	for (int i = 0; i < output.size(); i++)
	{
		net->addToOutput((float)output[i]);
	}
	
	const vector<float> results = net->train();
	cout << net->toString();
	return convertFloatVector(results);
}

/// <summary>
/// Gets the neuron bias at the given indices
/// </summary>
/// <param layer_index>The index of the layer</param>
/// <param neuron_index>The index of the neuron</param>
float GDNetwork::getNeuronBias(int layer_index, int neuron_index)
{
	return network->getLayerAt(layer_index)->getAxonAt(neuron_index)->getBias();
}

/// <summary>
/// Gets the dendrite weight at the given indices
/// </summary>
/// <param layer_index>The index of the layer</param>
/// <param neuron_index>The index of the neuron</param>
/// <param neuron_index>The index of the dendrite</param>
float GDNetwork::getDendriteWeight(int layer_index, int neuron_index, int dendrite_index)
{
	return network->getLayerAt(layer_index)->getAxonAt(neuron_index)->getDendriteAt(dendrite_index)->getWeight();
}

/// <summary>
/// Clears the network
/// </summary>
void GDNetwork::clearNetwork()
{
	delete network;
}

/// <summary>
/// Gets a gdscript formatted export dictionary of the network
/// </summary>
const Dictionary GDNetwork::getExportDictionary()
{
	NeuralNetwork* net = getNetwork();
	Dictionary exportDictionary = Dictionary();
	for (int i = 0; i < net->sizeOfLayers(); i++)
	{
		Dictionary layerDictionary = Dictionary();
		for (int j = 0; j < net->getLayerAt(i)->sizeOfAxons(); j++)
		{
			Dictionary neuronDictionary = Dictionary();
			Axon* axon = net->getLayerAt(i)->getAxonAt(j);
			neuronDictionary["bias"] = axon->getBias();
			neuronDictionary["delta"] = axon->getDelta();
			neuronDictionary["value"] = axon->getValue();
			Array* weights = new Array();
			for (int k = 0; k < axon->sizeOfDendrites(); k++)
			{
				weights->append(axon->getDendriteAt(k)->getWeight());
			}
			neuronDictionary["weights"] = weights;
			layerDictionary["neuron " + j] = neuronDictionary;
		}
		exportDictionary["layer " + i] = layerDictionary;
	}
	Array settings = Array();
	settings.append(convertIntVector(net->getTopology())); 
	settings.append(net->getLearningRate());

	exportDictionary["settings"] = settings;
	return exportDictionary;
}

/// <summary>
/// Imports the weights of a gdscript dictionary into a new network
/// </summary>
const Array GDNetwork::importWeightDictionary(Dictionary dictionary)
{
	Array settings = dictionary["settings"];
	network = new DenseNetwork(convertPoolIntArray((PoolIntArray)settings[0]));

	network->setLearningRate((float)settings[1]);

	for (int i = 0; i < network->sizeOfLayers(); i++)
	{
		if (dictionary.has("layer " + i))
		{
			Dictionary layerDictionary = dictionary["layer " + i];
			for (int j = 0; j < layerDictionary.size(); j++)
			{
				if (layerDictionary.has("neuron " + j))
				{
					Dictionary neuronDictionary = layerDictionary["neuron " + j];
					Axon* axon = network->getLayerAt(i)->getAxonAt(j);
					axon->setBias((float)neuronDictionary["bias"]);
					axon->setDelta((float)neuronDictionary["delta"]);
					axon->setValue((float)neuronDictionary["value"]);
					Array weights = neuronDictionary["weights"];
					for (int k = 0; k < weights.size(); k++)
					{
						cout << (float)weights[k];
						axon->getDendriteAt(k)->setWeight((float)weights[k]);
					}
				}
			}
		}
	}
	return settings;
}

/// <summary>
/// Converts gdscript Array to C++ vector of ints
/// </summary>
/// <param _array>The array to be converted</param>
const vector<int> GDNetwork::convertPoolIntArray(Array _array)
{
	vector<int> converted = vector<int>();
	for (int i = 0; i < _array.size(); i++)
	{
		converted.push_back((int)_array[i]);
	}
	return converted;
}

/// <summary>
/// Converts gdscript Array to C++ vector of floats
/// </summary>
/// <param _array>The array to be converted</param>
const vector<float> GDNetwork::convertPoolRealArray(Array _array)
{
	vector<float> converted = vector<float>();
	for (int i = 0; i < _array.size(); i++)
	{
		converted.push_back((float)_array[i]);
	}
	return converted;
}

/// <summary>
/// Converts C++ vector of float to gdscript Real Array
/// </summary>
/// <param vector>The vector to be converted</param>
PoolRealArray GDNetwork::convertFloatVector(const vector<float> vector)
{
	PoolRealArray array = PoolRealArray();
	for (int i = 0; i < vector.size(); i++)
	{
		array.append(vector.at(i));
	}
	return array;
}

/// <summary>
/// Converts C++ vector of int to gdscript Int Array
/// </summary>
/// <param vector>The vector to be converted</param>
PoolIntArray GDNetwork::convertIntVector(const vector<int> vector)
{
	PoolIntArray array = PoolIntArray();
	for (int i = 0; i < vector.size(); i++)
	{
		array.append(vector.at(i));
	}
	return array;
}